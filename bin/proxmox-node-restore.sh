#!/bin/bash

if ! command -v rsync >/dev/null; then
  echo 'rsync is required but it is not installed.'
  exit 1
fi

ISCSI_PORTAL="iscsi_portal"
ISCSI_TARGET="iscsi_target"
ISCSI_ADDRESS="${ISCSI_PORTAL}:${ISCSI_TARGET}"

BACKUP_DIR="/mnt/isci/${ISCSI_ADDRESS}/Backups/Proxmox/"

RSYNC_OPTIONS=" --archive --compress --verbose "

# check if ISCSI_PORTAL is not set or empty
if [ -z "${ISCSI_PORTAL}" ]; then
  echo 'ISCSI_PORTAL must be set'
  exit 1
fi

# check if ISCSI_TARGET is not set or empty
if [ -z "${ISCSI_TARGET}" ]; then
  echo 'ISCSI_TARGET must be set'
  exit 1
fi

# check if backup directory is accessible (and likely mounted)
if ! [ -e "${BACKUP_DIR}" ]; then
  echo "Backup directory ${BACKUP_DIR} does not exist or is not accessible"
  exit 1
fi

# Generate an array of backups and present to user with select
mapfile -t options < <(find "${BACKUP_DIR}" -mindepth 1 -maxdepth 1 -type d -print0 | xargs -0)

echo "Available backups:"
select opt in "${options[@]}"; do
  BACKUP_FOLDER=$opt
  break
done

# Check if backup folder was selected
if [[ -z ${BACKUP_FOLDER} ]]; then
  echo "No backup folder selected, exiting..."
  exit 1
fi

# Check if backup exists
if [[ ! -d "${BACKUP_FOLDER}" ]]; then
  echo "Backup ${BACKUP_FOLDER} does not exist"
  exit 1
fi

# Perform a dry run to check which files would be overwritten
echo "Performing dry run..."
DRY_RUN_OUTPUT=$(
  rsync --dry-run ${RSYNC_OPTIONS} "${BACKUP_FOLDER}/pve" /etc/
  rsync --dry-run ${RSYNC_OPTIONS} "${BACKUP_FOLDER}/vzdump.conf" /etc/
)

# If Dry run output is not empty, existing files might be overwritten
if [[ -n "${DRY_RUN_OUTPUT}" ]]; then
  echo "The following files will be overwritten:"
  echo "${DRY_RUN_OUTPUT}"

  # Ask for confirmation before replacing existing files
  read -r -p 'Do you want to continue and overwrite them? [y/N] ' response
  if [[ "$response" != "y" ]]; then
    echo "Operation cancelled."
    exit 1
  fi
fi

rsync ${RSYNC_OPTIONS} "${BACKUP_FOLDER}/pve" /etc/ || {
  echo 'Failed to restore PVE configuration'
  exit 1
}
rsync ${RSYNC_OPTIONS} "${BACKUP_FOLDER}/vzdump.conf" /etc/ || {
  echo 'Failed to restore vzdump configuration'
  exit 1
}

echo "Proxmox server configuration files restored from ${BACKUP_FOLDER}"
