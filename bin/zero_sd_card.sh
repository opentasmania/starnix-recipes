#!/bin/bash
# Attempt to clear the crud on uSD and SD cards so that images compress better
# Copyright (C) Peter Lawler
# Snail mail: PO Box 195
#             Lindisfarne, Tasmania
#             AUSTRALIA 7015
# email:      relwalretep@gmail.com
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
##########################################################################

# TODO: Location picker with sensible defaults for $zero_file
# TODO: Backup existing before zeroing
#        eg 'dd status=progress if=/dev/sdX | xz --compress --threads=8 - > sdcard,img.xz'
# TODO: Currently only (tacitly) designed to work when running on sdcard booted system,
#        would be good to have it work on any linux

check_sudo () {
    if [ "${UID}" != "0" ]; then
        /bin/echo "Not root user"
        if ! hash sudo 2>/dev/null; then
            echo "su -c \"apt --assume-yes install sudo\""
        fi
        HAVE_SUDO=$(command -v sudo 2>/dev/null)
        /bin/echo "sudo(1) found at ${HAVE_SUDO}"
    else
        /bin/echo "Running as root, sudo(1) not needed."
        HAVE_SUDO=""
    fi
}

check_sudo

zero_path=$HOME
zero_file=$zero_path/zero_file

block_size=$(stat "$zero_path" | grep "IO Block" | awk '{print $7}')
echo "Block: $block_size"
free_space=$(df -kh "$zero_path" | tail -1 | awk '{print $4}' )
echo "Need to fill: $free_space"

if [ -f "${zero_path}"/zero_file ]; then
    ${HAVE_SUDO} rm --force --verbose "${zero_path}"/zero_file
    echo "pre- sync(1): $(date)"
    ${HAVE_SUDO} sync
fi

echo "1/5 sync(1): $(date)"
${HAVE_SUDO} sync

echo "Start dd(1): $(date)"
echo "dd status=progress if=/dev/zero of=${zero_file} bs=${block_size}"
${HAVE_SUDO} dd status=progress if=/dev/zero of="$zero_file" bs="$block_size"
echo "Done dd(1): $(date)"

echo "2/5 sync(1): $(date)"
${HAVE_SUDO} sync
echo "3/5 sync(1): $(date)"
${HAVE_SUDO} sync

ls -l --human-readable "$zero_file"
${HAVE_SUDO} rm --force --verbose "$zero_file"

echo "4/5 sync(1)ing: $(date)"
${HAVE_SUDO} sync
echo "5/5 sync(1)ing: $(date)"
${HAVE_SUDO} sync

echo "-------Finish zeroing at $(date)-------"
