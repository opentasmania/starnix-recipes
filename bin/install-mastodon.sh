#!/bin/bash

echo "Don't touch that button, touch this instead"
echo "https://docs.joinmastodon.org/admin/install/"
exit 1

apt update
apt install -y curl wget gnupg apt-transport-https lsb-release ca-certificates
apt install -y \
  imagemagick ffmpeg libpq-dev libxml2-dev libxslt1-dev file git-core \
  g++ libprotobuf-dev protobuf-compiler pkg-config nodejs gcc autoconf \
  bison build-essential libssl-dev libyaml-dev libreadline6-dev \
  zlib1g-dev libncurses5-dev libffi-dev libgdbm-dev \
  nginx redis-server redis-tools postgresql postgresql-contrib \
  certbot python3-certbot-nginx libidn11-dev libicu-dev libjemalloc-dev


if nodejsversion < 16

# TODO: LTS Node.js (18?)
# YEEEK
curl -sL https://deb.nodesource.com/setup_16.x | bash -


Yarn
corepack enable
yarn set version classic

adduser --disabled-login mastodon

su - mastodon

git clone https://github.com/rbenv/rbenv.git ~/.rbenv
cd ~/.rbenv && src/configure && make -C src
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(rbenv init -)"' >> ~/.bashrc
exec bash
git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
RUBY_CONFIGURE_OPTS=--with-jemalloc rbenv install 3.0.4
rbenv global 3.0.4
gem install bundler --no-document

Return to the root user:
# https://pgtune.leopard.in.ua/#/

Creating a user

sudo -u postgres psql
CREATE USER mastodon CREATEDB;
\q

su - mastodon

git clone https://github.com/mastodon/mastodon.git live && cd live
git checkout $(git tag -l | grep -v 'rc[0-9]*$' | sort -V | tail -n 1)

bundle config deployment 'true'
bundle config without 'development test'
bundle install -j$(getconf _NPROCESSORS_ONLN)
yarn install --pure-lockfile

RAILS_ENV=production bundle exec rake mastodon:setup

# back to root
exit

cp /home/mastodon/live/dist/nginx.conf /etc/nginx/sites-available/mastodon
ln -s /etc/nginx/sites-available/mastodon /etc/nginx/sites-enabled/mastodon

systemctl reload nginx

certbot --nginx -d example.com

cp /home/mastodon/live/dist/mastodon-*.service /etc/systemd/system/

systemctl daemon-reload
systemctl enable --now mastodon-web mastodon-sidekiq mastodon-streaming