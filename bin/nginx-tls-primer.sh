#!/bin/sh
# Prime TLS on all enabled nginx sites after successful startup of nginx
# Copyright (C) Peter Lawler <relwalretep@gmail.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# nginx doesn't fully initialise TLSv1.2+ on startup, but requires a client to first connect
# well I think I read that somewhere and it seems to be true
# Note:
# nasty hack, assumes ALL server_name have TLS enabled

urls=$(/bin/grep server_name /etc/nginx/sites-enabled/* | grep -v return | /usr/bin/awk '{$1=$1;print}' | /usr/bin/cut -f 3 -d " " | /bin/sed s/\;//  | /usr/bin/sort | /usr/bin/uniq)

for u in $urls; do
    echo \"priming https://$u\";
    /usr/bin/wget -O /dev/null -o /dev/null https://$u;
    /usr/bin/wget -O /dev/null -o /dev/null https://$u;
done
