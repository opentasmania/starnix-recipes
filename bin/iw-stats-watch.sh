#!/bin/bash
# iw-stats-watch.sh - A program to log linux wireless stats
# Copyright (C) Peter Lawler <relwalretep@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

LOGFILE=/var/log/iw-stats.log
SLEEP=1

# print column titles
head -2 /proc/net/wireless  > ${LOGFILE}

if [ -w ${LOGFILE} ]; then
    while true; do
        tail +3 /proc/net/wireless >> ${LOGFILE}
        sleep ${SLEEP}
    done
else
    echo "Can't write to ${LOGFILE}"
    exit 1
fi

exit

# Some notes:
for w in $(tail +3 /proc/net/wireless | cut -d \: -f 1 ); do
	iw dev ${w} station dump
done

