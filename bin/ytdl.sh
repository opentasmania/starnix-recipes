#!/bin/bash

# ytdl.sh - A script to download from YouTube
# Copyright (C) Peter Lawler
# Snail mail: PO Box 195
#             Lindisfarne, Tasmania
#             AUSTRALIA 7015
# email:      relwalretep@gmail.com

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
URL=$1

if ! hash youtube-dl 2>/dev/null; then
    if ! hash ~/.local/bin/youtube-dl 2>/dev/null; then
        echo "Missing youtube-dl"
        exit
    else
        YTDL=~/.local/bin/youtube-dl
    fi
else
    YTDL="$(which youtube-dl )"
fi

if [ "$URL" = "" ]; then
    echo "Error:"
    echo "youtube-dl URL"
    exit 1
fi
$YTDL --format best --continue --ignore-errors --no-overwrites \
    --output "%(title)s.%(ext)s" --verbose \
    $1

