#!/usr/bin/env bash
# Recurse a directory and update repositories.
# Summarize successes and failures.

# Copyright (C) <2025> Peter Lawler
# Licensed under the GNU Affero GPL, version 3 or later.

# Default values
target_dir=""
max_depth=2
show_successful=false
show_failed=true

# Define repository types, their detection criteria, and respective update commands
declare -A REPO_TYPES=(
  [git]='.git'
  [hg]='.hg'
  [svn]='.svn'
)

declare -A REPO_UPDATE_COMMANDS=(
  [git]='git -C __DIR__ pull --quiet'
  [hg]='hg -R __DIR__ pull --update --quiet'
  [svn]='svn update __DIR__ --quiet'
)

# Print usage and exit
print_usage_and_exit() {
  echo "Usage: $0 <directory> [--depth|-m <max_depth>] [--successful|-s] [--no-successful|--hide-successful] [--failed|-f] [--no-failed|--hide-failed] [--help|-h]"
  exit "$1"
}

# Parse command-line arguments
while [[ "$#" -gt 0 ]]; do
  case $1 in
    --depth|-m)
      if [[ "$2" =~ ^[0-9]+$ ]]; then
        max_depth="$2"
        shift 2
      else
        echo "Error: '--depth' requires a numeric argument." >&2
        print_usage_and_exit 1
      fi
      ;;
    --successful|-s)
      show_successful=true
      shift
      ;;
    --failed|-f)
      show_failed=true
      shift
      ;;
    --no-successful|--hide-successful)
      show_successful=false
      shift
      ;;
    --no-failed|--hide-failed)
      show_failed=false
      shift
      ;;
    --help|-h)
      print_usage_and_exit 0
      ;;
    -*)
      echo "Error: Invalid argument '$1'" >&2
      print_usage_and_exit 1
      ;;
    *)
      # Assign the first non-option argument as the target directory
      if [ -z "$target_dir" ]; then
        target_dir="$1"
        shift
      else
        echo "Error: Extra argument '$1'" >&2
        print_usage_and_exit 1
      fi
      ;;
  esac
done

# Ensure a target directory was provided
if [[ -z "$target_dir" ]]; then
  echo "Error: Missing required argument '<directory>'." >&2
  print_usage_and_exit 1
fi

# Check if the target directory exists
if [ ! -d "$target_dir" ]; then
  echo "Error: Directory '$target_dir' does not exist." >&2
  exit 1
fi

# Initialize arrays to track successes and failures
failed_dirs=()
successful_dirs=()

# Function to update repositories using their associated commands
update_repo_helper() {
  local repo_type="$1"
  local repo_dir="$2"

  # Replace placeholder in the update command with the actual repo directory
  local command="${REPO_UPDATE_COMMANDS[$repo_type]//__DIR__/$repo_dir}"

  echo "Updating $repo_type repository in $repo_dir"

  # Execute the command and record success or failure
  if eval "$command" 2>/dev/null; then
    echo "$repo_dir: $repo_type update successful"
    successful_dirs+=("$repo_dir")
  else
    echo "$repo_dir: $repo_type update failed"
    failed_dirs+=("$repo_dir")
  fi
}

# Iterate through the directory and filter for valid repo types
find "$target_dir" -mindepth 1 -maxdepth "$max_depth" -type d | while IFS= read -r subdir; do
  repo_found=false
  for repo_type in "${!REPO_TYPES[@]}"; do
    if [ -d "$subdir/${REPO_TYPES[$repo_type]}" ]; then
      repo_found=true
      update_repo_helper "$repo_type" "$subdir"
      break # Stop checking other repo types once a match is found
    fi
  done
  # If no repo type matched, record the directory as failed
  if [ "$repo_found" = false ]; then
    failed_dirs+=("$subdir")
  fi
done

# Optionally print successful updates
if [ "$show_successful" = true ]; then
  echo ""
  echo "Successful updates summary:"
  if [ "${#successful_dirs[@]}" -gt 0 ]; then
    for dir in "${successful_dirs[@]}"; do
      echo "$dir"
    done
  else
    echo "No successful updates."
  fi
fi

# Optionally print failed updates
if [ "$show_failed" = true ]; then
  echo ""
  echo "Failed updates summary:"
  if [ "${#failed_dirs[@]}" -gt 0 ]; then
    for dir in "${failed_dirs[@]}"; do
      echo "$dir"
    done
  else
    echo "No failed updates."
  fi
fi