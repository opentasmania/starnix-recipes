#!/bin/bash
# Script to empty server-side Samba share recycle bin
# Copyright (C) Peter Lawler <relwalretep@gmail.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

RECYCLE_DIR_NAME=".recycle"
DEFAULT_EXPIRY_DAYS=90
SMB_CONF="/etc/samba/smb.conf"

find_samba_shares() {
  if [ ! -f "$SMB_CONF" ]; then
    echo "Samba configuration file not found at $SMB_CONF"
    exit 1
  fi
  grep -E '^\s*path\s*=' "$SMB_CONF" | sed -e 's/^\s*path\s*=\s*//'
}

clean_recycle_bin() {
    local share_path="$1"
    local expiry_days="$2"
    local recycle_dir="$share_path/$RECYCLE_DIR_NAME"
    if [ -d "$recycle_dir" ]; then
        find "$recycle_dir" -atime +"$expiry_days" -exec rm -rf '{}' \;
        find "$recycle_dir" -type d -empty -exec rmdir '{}' \;
    else
        echo "Recycle bin directory $recycle_dir does not exist."
    fi
}

DEFAULT_SHARES_PATH=$(find_samba_shares)
while IFS= read -r share_path; do
    if [ -d "$share_path" ]; then
        clean_recycle_bin "$share_path" "$DEFAULT_EXPIRY_DAYS"
    else
        echo "Share path $share_path does not exist."
    fi
done <<< "$DEFAULT_SHARES_PATH"
