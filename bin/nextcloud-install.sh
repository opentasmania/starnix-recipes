#!/bin/bash -e

# A shell script to install nextcloud on a debian 12 (bookworm) system

# Copyright (C) 2022 Peter Lawler <relwalretep@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# https://www.how2shout.com/linux/how-to-install-nextcloud-on-debain-11-bullseye-linux/
# https://cloudcone.com/docs/article/how-to-install-nextcloud-on-debian-10/
# TODO: Work out php version number for fcgi socket

function init () {
  FQDN=$(host -TtA $(hostname -s) | grep "has address" | awk '{print $1}')
  if [[ "${FQDN}" == "" ]]; then FQDN=$(hostname -s); fi
  ISO_3166_1="AU"
  NEXTCLOUD_DBNAME="nextclouddb"
  NEXTCLOUD_DB_USER="nextclouddbuser"
  NEXTCLOUD_DB_PASSWORD="nextclouddbpassword"
  NEXTCLOUD_ADMIN_USER="admin"
  NEXTCLOUD_ADMIN_PASSWORD="temppwd"
  REMOTE_NETWORK="192.168.128.0/22"
  echo "Fetching Nextcloud version"
  NEXTCLOUD_VERSION=$(wget -S -O - https://github.com/nextcloud/server/releases/latest 2>&1 | grep -i location | head -1 | rev | cut -d \/ -f 1 | rev | sed s/v//)
  NCAPPS=()
  NCAPPS+=("announcementcenter")
  NCAPPS+=("appointments")
  NCAPPS+=("apporder")
  NCAPPS+=("audioplayer")
  NCAPPS+=("backup")
  NCAPPS+=("bookmarks")
  NCAPPS+=("breezedark")
  NCAPPS+=("bruteforcesettings")
  NCAPPS+=("calendar")
  NCAPPS+=("camerarawpreviews")
  NCAPPS+=("cfg_share_links")
  NCAPPS+=("checksum")
  NCAPPS+=("contacts")
  NCAPPS+=("cookbook")
  NCAPPS+=("data_request")
  NCAPPS+=("deck")
  NCAPPS+=("drawio")
  NCAPPS+=("drop_account")
  NCAPPS+=("duplicatefinder")
  NCAPPS+=("electronicsignatures")
  NCAPPS+=("end_to_end_encryption")
  NCAPPS+=("event_update_notification")
  NCAPPS+=("external")
  NCAPPS+=("extract")
#  NCAPPS+=("facerecognition") # TODO: Error: App "Face Recognition" cannot be installed because the following dependencies are not fulfilled: The library pdlib is not available.
  NCAPPS+=("files_accesscontrol")
  NCAPPS+=("files_antivirus")
  NCAPPS+=("files_automatedtagging")
  NCAPPS+=("files_markdown")
  NCAPPS+=("files_mindmap")
  NCAPPS+=("files_photospheres")
  NCAPPS+=("files_reader")
  NCAPPS+=("files_scripts")
  NCAPPS+=("fileslibreofficeedit")
  NCAPPS+=("forms")
  NCAPPS+=("files_fulltextsearch")
  NCAPPS+=("fulltextsearch_elasticsearch")
#  NCAPPS+=("fulltextsearch_fulltextsearch")
  NCAPPS+=("files_fulltextsearch_tesseract")
  NCAPPS+=("fulltextsearch")
  NCAPPS+=("geoblocker")
  NCAPPS+=("gpoddersync")
  NCAPPS+=("group_default_quota")
  NCAPPS+=("groupfolders")
  NCAPPS+=("holiday_calendars")
#  NCAPPS+=("imageconverter")
  NCAPPS+=("impersonate")
  NCAPPS+=("integration_github")
  NCAPPS+=("integration_gitlab")
  NCAPPS+=("integration_google")
  NCAPPS+=("keeweb")
  NCAPPS+=("mail")
  NCAPPS+=("maps")
  NCAPPS+=("mediadc")
  NCAPPS+=("money")
  NCAPPS+=("music")
  NCAPPS+=("news")
  NCAPPS+=("notes")
#  NCAPPS+=("ocr")
  NCAPPS+=("ownpad")
  NCAPPS+=("polls")
  NCAPPS+=("quota_warning")
  NCAPPS+=("ransomware_protection")
  NCAPPS+=("richdocuments")
  NCAPPS+=("richdocumentscode")
  NCAPPS+=("sociallogin")
  NCAPPS+=("spreed") # aka Talk
  NCAPPS+=("suspicious_login")
  NCAPPS+=("tasks")
  NCAPPS+=("terms_of_service")
  NCAPPS+=("twofactor_totp")
  NCAPPS+=("twofactor_webauthn")
  NCAPPS+=("user_saml")
  NCAPPS+=("user_usage_report")
  NCAPPS+=("video_converter")
  NCAPPS+=("webhooks")
  NCAPPS+=("workflow_media_converter")
  NCAPPS+=("workflow_ocr")
  NCAPPS+=("workflow_pdf_converter")
  NCAPPS+=("workflow_script")
  NCAPPS+=("zipper")
}

function parse_options () {
  unset DOMAINNAME
  local o
  while getopts "d:" o; do
    case "${o}" in
    d)
      DOMAINNAME=${OPTARG}
      ;;
    *)
      usage
      ;;
    esac
  done
  if [ -z ${DOMAINNAME} ]; then
    echo "./$(basename $0): must specify domain name -- -d"
    usage || exit 1
  fi
}

function usage() {
  echo "Usage: $0 -d <domainnname>" 1>&2
  exit 1
}

function install_packages () {
  sudo apt update --yes
  sudo apt install --yes apache2 apache2-mod-fgcid bzip2 libapache2-mod-php mariadb-server \
    php php-bcmath php-cgi php-cli php-curl php-fpm php-gd \
    php-gmp php-imagick php-intl php-mbstring php-mysql php-redis php-xml php-zip \
    python3-distutils python3-dev php-memcached \
    libdlib-dev \
    libmagickcore-6.q16-6-extra libmagickcore-6.q16hdri-6-extra \
    apt-utils bash-completion curl htop iotop iputils-ping lshw \
    net-tools ufw whiptail \
    clamav clamav-daemon clamav-freshclam libclamunrar libclamunrar9 \
    php-apcu fail2ban \
    redis-server redis-tools redis
  sudo apt install --yes --non-interactive phpmyadmin
  echo "Post-install delay (because network...)..."
  sudo systemctl --show-transaction stop apache2
  sudo systemctl --show-transaction stop mariadb
  sleep 5 # Look, I know...
}

function install_nextcloud () {
  PHP_VERSION=$(php -v | grep ^PHP | cut -d \  -f 2 | cut -d \. -f 1,2)
  echo "Downloading nextcloud-${NEXTCLOUD_VERSION}"
  #TODO: Check for existing files
  wget https://download.nextcloud.com/server/releases/nextcloud-${NEXTCLOUD_VERSION}.tar.bz2
  wget https://download.nextcloud.com/server/releases/nextcloud-${NEXTCLOUD_VERSION}.tar.bz2.sha512
  sha512sum --check nextcloud-${NEXTCLOUD_VERSION}.tar.bz2.sha512
  sudo mkdir --parents /srv/nextcloud/{www,data}
  sudo tar --extract --verbose --file ./nextcloud-${NEXTCLOUD_VERSION}.tar.bz2 --directory=/srv/nextcloud/www/. --strip-components=1
}

function configure_website () {
  sudo chown --recursive www-data:www-data /srv/nextcloud/{www,data}
  sudo chmod --recursive 755 /srv/nextcloud/{www,data}
  sudo touch /etc/apache2/sites-available/nextcloud.conf
  sudo cat <<NEXTCLOUDVHOSTCONFIG | sudo tee /etc/apache2/sites-available/nextcloud.conf
<VirtualHost *:80>
   ServerName nextcloud.${DOMAINNAME}
   Redirect permanent / https://nextcloud.${DOMAINNAME}/
</VirtualHost>
<VirtualHost *:443>
  ServerAdmin admin@${DOMAINNAME}
  DocumentRoot /srv/nextcloud/www/
  ServerName ${DOMAINNAME}
  ServerAlias nextcloud.${DOMAINNAME}
    <Directory /srv/nextcloud/www/>
    Options FollowSymlinks
    AllowOverride All
    Require all granted
  </Directory>
  <IfModule mod_headers.c>
    Header always set Strict-Transport-Security "max-age=15552000; includeSubDomains"
  </IfModule>
  SSLEngine on
  SSLCertificateFile /etc/ssl/certs/apache-nextcloud-selfsigned.crt
  SSLCertificateKeyFile /etc/ssl/private/apache-nextcloud-selfsigned.key
ErrorLog /srv/nextcloud/data/error.log
CustomLog /srv/nextcloud/data/access.log combined
<FilesMatch \.php$>
   SetHandler "proxy:unix:/run/php/php8.2-fpm.sock|fcgi://localhost"
</FilesMatch>
<Directory /srv/nextcloud/www/>
  RewriteEngine on
  RewriteBase /
  RewriteCond %{REQUEST_FILENAME} !-f
  RewriteRule ^(.*) index.php [PT,L]
</Directory>
</VirtualHost>
NEXTCLOUDVHOSTCONFIG

sudo cat << PHPMYADMIN | sudo tee /etc/apache2/conf-available/phpmyadmin-restrictions.conf
<Directory /usr/share/phpmyadmin>
order deny,allow
allow from ${REMOTE_NETWORK}
deny from all
</Directory>
PHPMYADMIN
  echo "Creating certificate"
  sudo openssl req -x509 -nodes -days 90 -newkey rsa:4096 \
    -keyout /etc/ssl/private/apache-nextcloud-selfsigned.key \
    -out /etc/ssl/certs/apache-nextcloud-selfsigned.crt \
    -subj "/C=NA/ST=NA/L=NA/O=NA/OU=NA/CN=${FQDN}"

  echo "Configuring Apache"
  sudo a2dissite 000-default.conf default-ssl
  sudo a2enmod dir env headers mime proxy_fcgi rewrite setenvif ssl
  sudo a2enconf php${PHP_VERSION}-fpm phpmyadmin-restrictions
  sudo a2ensite nextcloud.conf
}

function config_php () {
  echo "Configuring PHP"
  echo "memory_limit = 512M" | sudo tee -a /etc/php/${PHP_VERSION}/fpm/php.ini
  echo "apc.enable_cli = 1" | sudo tee -a /etc/php/${PHP_VERSION}/mods-available/apcu.ini
  # PHP configuration option output_buffering must be disabled
  sudo sed --in-place s/^output_buffering/\;output_buffering/g /etc/php/${PHP_VERSION}/fpm/php.ini
  sudo sed --in-place '/^\;opcache\.interned_strings_buffer/{n;s/.*/opcache\.interned_strings_buffer\=16/}' /etc/php/${PHP_VERSION}/fpm/php.ini
}

function config_mariadb () {
  echo "Configuring MariaDB"
  # Since MariaDB 10.4, the use of the mysql_secure_installation script is no longer needed
  # See https://mariadb.com/kb/en/authentication-from-mariadb-104/
  sudo systemctl --show-transaction start mariadb
  sudo mariadb -u root -p <<MARIADB_SCRIPT
CREATE DATABASE ${NEXTCLOUD_DBNAME};
CREATE USER '${NEXTCLOUD_DB_USER}'@'localhost' IDENTIFIED BY '${NEXTCLOUD_DB_PASSWORD}';
GRANT ALL ON ${NEXTCLOUD_DBNAME}.* TO '${NEXTCLOUD_DB_USER}'@'localhost';
FLUSH PRIVILEGES;
EXIT
MARIADB_SCRIPT
  sudo systemctl --show-transaction stop mariadb
}

function setup_nextcloud () {
  sudo systemctl --show-transaction start mariadb
  echo "Setting up Nextcloud"
  sudo -u www-data php /srv/nextcloud/www/occ maintenance:install --no-interaction \
    --database "mysql" --database-name "${NEXTCLOUD_DBNAME}" \
    --database-user "${NEXTCLOUD_DB_USER}" --database-pass "${NEXTCLOUD_DB_PASSWORD}" \
    --admin-user "${NEXTCLOUD_ADMIN_USER}" --admin-pass "${NEXTCLOUD_ADMIN_PASSWORD}"
  sudo systemctl --show-transaction stop mariadb
}

function install_nextcloud_apps () {
  sudo systemctl --show-transaction start mariadb
  for a in "${NCAPPS[@]}"; do
    echo "Installing ${a}"
    sudo -u www-data php /srv/nextcloud/www/occ app:install ${a}
  done
  sudo systemctl --show-transaction stop mariadb
}

function configure_nextcloud () {
  sudo systemctl --show-transaction start mariadb
  # TODO: Need to work out
  echo "Setting logfile location"
  sudo touch /var/log/nextcloud.log
  sudo chown www-data:www-data /var/log/nextcloud.log
  sudo -u www-data php /srv/nextcloud/www/occ log:file --file /var/log/nextcloud.log
  echo "Setting memcache for APCu"
  sudo -u www-data php /srv/nextcloud/www/occ config:system:set memcache.local --value="\OC\Memcache\APCu"
  echo "Setting datadirectory"
  sudo -u www-data php /srv/nextcloud/www/occ config:system:set datadirectory --value="/srv/nextcloud/data"
  echo "Setting trusted_domains"
  sudo -u www-data php /srv/nextcloud/www/occ config:system:set trusted_domains 2 --value="${FQDN}"
  echo "Setting logtimezone"
  sudo -u www-data php /srv/nextcloud/www/occ config:system:set logtimezone --value="Australia/Hobart"
  echo "Setting ISO_3166_1"
  sudo -u www-data php /srv/nextcloud/www/occ config:system:set default_phone_region --value="${ISO_3166_1}"
  echo "Setting memory cache"
  sudo -u www-data php /srv/nextcloud/www/occ config:system:set memcache.local --value="\OC\Memcache\APCu"
  sudo -u www-data php /srv/nextcloud/www/occ config:system:set memcache.locking --value="\OC\Memcache\Redis"
  sudo -u www-data php /srv/nextcloud/www/occ config:system:set redis host --value="/var/run/redis/redis.sock"
  sudo -u www-data php /srv/nextcloud/www/occ config:system:set redis port --value="6379"
  sudo -u www-data php /srv/nextcloud/www/occ config:system:set redis host --value="localhost"
  echo "unixsocket /run/redis/redis.sock" | sudo tee -a /etc/redis/redis.conf
  echo "unixsocketperm 775" | sudo tee -a /etc/redis/redis.conf
  sudo systemctl --show-transaction stop mariadb
}

function configure_firewall () {
  sudo ufw allow from any to any app WWW
  sudo ufw allow from any to any app "WWW Secure"
}

function configure_fail2ban () {
cat << EOF |sudo tee /etc/fail2ban/jail.d/nextcloud.local
[nextcloud]
backend = auto
enabled = true
port = 80,443
protocol = tcp
filter = nextcloud
maxretry = 3
bantime = 86400
findtime = 43200
logpath = /srv/nextcloud/data/nextcloud.log
EOF

cat << EOF |sudo tee /etc/fail2ban//filter.d/nextcloud.conf
[Definition]
_groupsre = (?:(?:,?\s*"\w+":(?:"[^"]+"|\w+))*)
failregex = ^\{%(_groupsre)s,?\s*"remoteAddr":"<HOST>"%(_groupsre)s,?\s*"message":"Login failed:
            ^\{%(_groupsre)s,?\s*"remoteAddr":"<HOST>"%(_groupsre)s,?\s*"message":"Trusted domain error.
datepattern = ,?\s*"time"\s*:\s*"%%Y-%%m-%%d[T ]%%H:%%M:%%S(%%z)?"
EOF
sudo systemctl --show-transaction reload fail2ban
fail2ban-client status nextcloud
}

function configure_redis () {
  echo "vm.overcommit_memory=1" /etc/sysctl.d/redis.conf
  sudo sysctl vm.overcommit_memory=1
  cat << EOF |sudo tee /etc/systemd/system/disable-transparent-huge-pages.service
[Unit]
Description=Disable Transparent Huge Pages (THP)
DefaultDependencies=no
After=sysinit.target local-fs.target
Before=redis-server.service
[Service]
Type=oneshot
ExecStart=/bin/sh -c 'echo never | tee /sys/kernel/mm/transparent_hugepage/enabled > /dev/null'
[Install]
WantedBy=basic.target
EOF
  sudo systemctl --show-transaction daemon-reload
  sudo systemctl --show-transaction enable disable-transparent-huge-pages
  sudo systemctl --show-transaction start disable-transparent-huge-pages
  ## TODO: automate this bit
  echo "Don't forget to enable transactional locking!!"
  echo "https://docs.nextcloud.com/server/27/admin_manual/configuration_files/files_locking_transactional.html"
}

function main() {
  install_packages
  install_nextcloud
  configure_website
  config_php
  config_mariadb
  setup_nextcloud
  configure_nextcloud
  install_nextcloud_apps
  configure_fail2ban
  configure_firewall
  configure_redis
  sudo systemctl --show-transaction start apache2
}

init "$@"
parse_options "$@"
main
exit 0
