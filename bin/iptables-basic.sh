#!/bin/bash

# iptables-basic.sh - establish very basic iptables rules
# Copyright (C) Peter Lawler
# Snail mail: PO Box 195
#             Lindisfarne, Tasmania
#             AUSTRALIA 7015
# email:      relwalretep@gmail.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


IPTABLES="/sbin/iptables"
SERVER_IP=$(hostname -I|awk '{print $1}')

function check_ip() {
    local ip=$1
    local status=1
    if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
        OIFS=$IFS
        IFS='.'
        ip=($ip)
        IFS=$OIFS
        [[ ${ip[0]} -le 255 && ${ip[1]} -le 255 \
            && ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
        status=$?
        echo "Status ${status}"
    fi
    return $status
}

function sudo_check() {
    echo "Checking for sudo"
    if [ "$(id | grep root )" ]; then
        echo "Probably shouldn't be run as root… but proceeding anyway"
    elif [ "$(sudo -v)" ]; then
        echo "sudo privileges needed, exiting"
        exit 1
    fi
}

sudo_check
while read -e -p "If you wish to remote manage, please input an IP address: " REMOTE_MANAGE_IP; do
    echo "Remote ${REMOTE_MANAGE_IP}"
    if [[ $(check_ip ${REMOTE_MANAGE_IP}) ]]; then
        REMOTE_IP=${REMOTE_MANAGE_IP}
        break
    else
        read -r -p "Are you sure? [Y/n] " response
        RESPONSE=${response,,}
        if [[ "${RESPONSE}" =~ ^(Yes|y)$ ]]; then
            break
        fi
    fi
done

${IPTABLES} -F
${IPTABLES} -X
${IPTABLES} -P INPUT DROP
${IPTABLES} -P OUTPUT DROP
${IPTABLES} -P FORWARD DROP
${IPTABLES} -A INPUT -i lo -j ACCEPT
${IPTABLES} -A OUTPUT -o lo -j ACCEPT
if [ ${REMOTE_IP} ]; then
    ${IPTABLES} -A INPUT -p tcp -s 0/0 -d $SERVER_IP -s $REMOTE_IP --sport 513:65535 --dport 22 -m state --state NEW,ESTABLISHED -j ACCEPT
fi
# ${IPTABLES} -A OUTPUT -p tcp -s $SERVER_IP -d 0/0 --sport 22 --dport 513:65535 -m state --state ESTABLISHED -j ACCEPT
${IPTABLES} -A INPUT -j DROP
${IPTABLES} -A OUTPUT -j DROP
