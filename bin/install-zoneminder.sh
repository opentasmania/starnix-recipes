#!/bin/bash

echo "Don't touch that button, touch this instead"
echo "Be warned, though, there are monsters inside"
echo "https://wiki.zoneminder.com/Debian_12_Bookworm_with_Zoneminder_1.36.33"
exit 1

# Based on https://wiki.zoneminder.com/Debian_12_Bookworm_with_Zoneminder_1.36.33

# Uncomment for local video
# adduser www-data video

# find the date section
sudo timedatectl set-timezone Australia/Hobart

sudo apt install --yes apache2 mariadb-server php libapache2-mod-php php-mysql lsb-release gnupg2
sudo systemctl stop apache2

sudo mysql_secure_installation

# find the date section in /etc/php/8.2/apache2/php.ini
date.timezone = Australia/Hobart

sudo apt install --yes zoneminder

sudo mysql -uroot -p < /usr/share/zoneminder/db/zm_create.sql
sudo mysql -uroot -p -e "grant all on zm.* to 'zmuser'@localhost identified by 'zmpass';"
sudo mysqladmin -uroot -p reload

sudo cp /etc/apache2/conf-available/zoneminder.conf /etc/apache2/conf-available/zoneminder.conf.sav
sudo rm /etc/apache2/conf-available/zoneminder.conf

sudo cat << ZONEMINDERCONFIG | sudo tee /etc/apache2/conf-available/zoneminder.conf
# Remember to enable cgi mod (i.e. "a2enmod cgi").
ScriptAlias /zm/cgi-bin "/usr/lib/zoneminder/cgi-bin"
<Directory "/usr/lib/zoneminder/cgi-bin">
    Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch
    AllowOverride All
    Require all granted
</Directory>


# Order matters. This alias must come first.
Alias /zm/cache /var/cache/zoneminder
<Directory /var/cache/zoneminder>
    Options -Indexes +FollowSymLinks
    AllowOverride None
    <IfModule mod_authz_core.c>
        # Apache 2.4
        Require all granted
    </IfModule>
</Directory>

Alias /zm /usr/share/zoneminder/www
<Directory /usr/share/zoneminder/www>
  Options -Indexes +FollowSymLinks
  <IfModule mod_dir.c>
    DirectoryIndex index.php
  </IfModule>
</Directory>

# For better visibility, the following directives have been migrated from the
# default .htaccess files included with the CakePHP project.
# Parameters not set here are inherited from the parent directive above.
<Directory "/usr/share/zoneminder/www/api">
   RewriteEngine on
   RewriteRule ^$ app/webroot/ [L]
   RewriteRule (.*) app/webroot/$1 [L]
   RewriteBase /zm/api
</Directory>

<Directory "/usr/share/zoneminder/www/api/app">
   RewriteEngine on
   RewriteRule ^$ webroot/ [L]
   RewriteRule (.*) webroot/$1 [L]
   RewriteBase /zm/api
</Directory>

<Directory "/usr/share/zoneminder/www/api/app/webroot">
    RewriteEngine On
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteRule ^ index.php [L]
    RewriteBase /zm/api
</Directory>
ZONEMINDERCONFIG

sudo a2enmod rewrite
sudo a2enmod headers
sudo a2enmod expires
sudo a2enmod cgi
sudo a2enconf zoneminder

sudo chmod 640 /etc/zm/zm.conf
sudo chown root:www-data /etc/zm/zm.conf
sudo mkdir --parents /var/cache/zoneminder/cache
sudo chown --recursive www-data:www-data /var/cache/zoneminder/cache
sudo chmod 755 /var/cache/zoneminder/


sudo systemctl enable zoneminder.service
sudo systemctl start zoneminder.service

sudo systemctl start apache2