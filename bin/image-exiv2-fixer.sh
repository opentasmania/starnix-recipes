#!/bin/bash
##########################################################################
# image-mtime-fixer.sh - A script to adjust file mtime to exif data
# Copyright (C) Peter Lawler
# Snail mail: PO Box 195
#             Lindisfarne, Tasmania
#             AUSTRALIA 7015
# email:      relwalretep@gmail.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
##########################################################################

# Exit on error
# set -e

# Options (only on bash 4) to help with find(1) handling spaces
shopt -s globstar nullglob

VERBOSE=""
DRYRUN=""
while getopts "dv" opt; do
    case $opt in
        d)
            echo "Setting dryrun"
            DRYRUN="true"
            ;;
        v)
            echo "Setting verbose"
            VERBOSE="true"
            ;;
        \?)
            echo "$0 -d -v"
            exit 1
    esac
done


DCRAW_BINARY="$(which dcraw)"
if [ -z "${DCRAW_BINARY}" ]; then
    echo "No dcraw found, exiting"
    exit 1
else
    echo "dcraw found at ${DCRAW_BINARY}"
fi

EXIV2_BINARY="$(which exiv2)"
if [ -z "${EXIV2_BINARY}" ]; then
    echo "No exiv2 found, exiting"
    exit 1
else
    echo "exiv2 found at ${EXIV2_BINARY}"
fi

DIRECTORY="$(pwd)"

# Eventually add this back as an option
#if [ -n "${1}" ]; then
#    DIRECTORY="${1}"
#fi

IFS=$'\n'

for d in $(find "${DIRECTORY}" -type d)
do
    if [ "${VERBOSE}" ]; then
        echo "Entering directory ${d}"
    fi
    cd "${d}" || exit
    for f in *
    do
        if [ "${VERBOSE}" ]; then
            echo "------------"
        fi
        echo "Working ${f}"
        if [ "$(file ${f} |cut -d \  -f 2)" != "directory" ] && [ "$(file --mime-type ${f} | awk '{print $2}' | cut -d \/ -f 1)" = "image" ]; then
            if [ "${VERBOSE}" ]; then
                echo "Found image ${f}"
            fi
            EXIV_TIME=""
            STAT_TIME=""
            EXIV_TIMELINE=""
            EXIV_TIME=""
            STAT_TIME=$(stat --format=%y "${f}" | cut -d \. -f 1)
            EXIV_TIMELINE=$(exiv2 "${f}" | grep --text "Image timestamp")
            EXIV_TIME=$(echo "${EXIV_TIMELINE}" | grep --text "Image timestamp" | cut -s -d : -f 2-20 | sed 's/^[ \t]*//g' | sed s/:/-/ | sed s/:/-/ )
            if [ "${VERBOSE}" ];then
                echo "STAT_TIME     : ${STAT_TIME}"
                echo "EXIV_TIMELINE : ${EXIV_TIMELINE}"
                echo "EXIV_TIME     : ${EXIV_TIME}"
            fi
            if [ "${EXIV_TIME}" = " " ] ; then
                echo "EXIV PARSE ERROR"
                exit 1
            fi
            if [ -n "${EXIV_TIME}" ] && [ "${EXIV_TIME:0:1}" != " " ]; then
                if [ "${EXIV_TIME}" != "${STAT_TIME}" ]; then
                    echo "Changing from ${STAT_TIME} to ${EXIV_TIME}"
                    if [ "$VERBOSE" = "true" ]; then echo "touch -m -d ${EXIV_TIME} ${f}"; fi
                    if [ "$DRYRUN" != "true" ]; then touch -m -d "${EXIV_TIME}" "${f}"; fi
                else
                    echo "Already set to ${STAT_TIME}"
                fi
            else
                echo "No useful exiv2 data found"
            fi
        else
            echo "Can't be identified"
        fi
    done
done
