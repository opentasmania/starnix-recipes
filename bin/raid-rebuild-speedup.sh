#!/bin/bash

# raid-rebuild-speedup.sh - a script to speed up raid rebuilds
# Copyright (C) Peter Lawler
# Snail mail: PO Box 195
#             Lindisfarne, Tasmania
#             AUSTRALIA 7015
# email:      relwalretep@gmail.com

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

# For some information on what's going on here, see the rather useful:
# https://www.cyberciti.biz/tips/linux-raid-increase-resync-rebuild-speed.html

unset HAVE_SUDO
unset HAVE_BLOCKDEV
unset HAVE_MDADM
unset HAVE_SMARTCTL

if [ ${UID} != "0" ]; then
    echo "Not root user"
    if hash sudo 2>/dev/null; then
        HAVE_SUDO=$(command -v sudo 2>/dev/null)
        echo "sudo(1) found at ${HAVE_SUDO}"
    else
        echo "Need root/sudo access"
        exit 1
    fi
fi

if ${HAVE_SUDO} bash -c "hash blockdev" 2 > /dev/null ; then
    HAVE_BLOCKDEV=$(${HAVE_SUDO} bash -c "command -v blockdev" 2>/dev/null)
    echo "blockdev(1) found at ${HAVE_BLOCKDEV}"
else
    echo "No blockdev(1)"
fi

if ${HAVE_SUDO} bash -c "hash mdadm" 2>/dev/null; then
    HAVE_MDADM=$(${HAVE_SUDO} bash -c "command -v mdadm" 2>/dev/null)
    echo "mdam(1) found at ${HAVE_MDADM}"
else
    echo "No mdadm(1)"
fi

if ${HAVE_SUDO} bash -c "hash smartctl" 2>/dev/null; then
    HAVE_SMARTCTL=$(${HAVE_SUDO} bash -c "command -v smartctl" 2>/dev/null)
    echo "smartctl(1) found at ${HAVE_SMARTCTL}"
else
    echo "No smartctl(1)"
fi

# MiB for BLOCKDEV_READAHEAD (512 byte sectors)
BLOCKDEV_READAHEAD=16
BLOCKDEV_READAHEAD=$((BLOCKDEV_READAHEAD * 512))

# 16MiB
MD_STRIPE_CACHE_SIZE=16
MD_STRIPE_CACHE_SIZE=$((MD_STRIPE_CACHE_SIZE * 1024))

# 1MiB
QUEUE_READAHEAD=1
QUEUE_READAHEAD=$((QUEUE_READAHEAD * 1024))

unset RAIDS
RAIDS=$(cat /proc/mdstat |grep active|awk '{print $1}') || exit 1
if [ "${RAIDS}" = "" ]; then
    echo "No raid devices detected"
    exit 1
fi

${HAVE_SUDO}  sysctl -w dev.raid.speed_limit_max=200000
${HAVE_SUDO}  sysctl -w dev.raid.speed_limit_min=50000

for R in $RAIDS; do
    if [ ${HAVE_BLOCKDEV} ]; then
        # blockdevice readahead
        echo "Setting block device read-ahead to ${BLOCKDEV_READAHEAD} on ${R}"
        ${HAVE_SUDO} ${HAVE_BLOCKDEV} --setra ${BLOCKDEV_READAHEAD} /dev/${R}
    fi

    if [ -e /sys/block/${R}/md/stripe_cache_size ]; then
        # memory_consumed = system_page_size * nr_DISKS * stripe_cache_size
        # To set stripe_cache_size to 16 MiB
        echo "Setting stripe cache size to ${MD_STRIPE_CACHE_SIZE} on ${R}"
        ${HAVE_SUDO}  bash -c "echo ${MD_STRIPE_CACHE_SIZE} > /sys/block/${R}/md/stripe_cache_size"
    fi

    DISKS=$(${HAVE_SUDO} ${HAVE_MDADM} --detail /dev/${R} | grep "/dev/sd" | awk '{print $NF}' | cut -c6-8)
    for D in $DISKS; do

        if [ ${HAVE_BLOCKDEV} ] && [ -e /dev/${D} ]; then
            echo "Setting block device read-ahead to ${BLOCKDEV_READAHEAD} on ${D}"
            ${HAVE_SUDO} ${HAVE_BLOCKDEV} --setra ${BLOCKDEV_READAHEAD} /dev/${D}
        fi

        echo "Setting queue read_ahead_kb to 1024 on ${D}"
        ${HAVE_SUDO}  bash -c "echo 1024 > /sys/block/${D}/queue/read_ahead_kb"

        echo "Setting nr_requests to 256 on ${D}"
        ${HAVE_SUDO}  bash -c "echo 256 > /sys/block/${D}/queue/nr_requests"

        echo "Disabling NCQ on ${D}"
        ${HAVE_SUDO}  bash -c "echo 1 > /sys/block/${D}/device/queue_depth"

        if [ ${HAVE_SMARTCTL} ]; then
            echo "Disabling apm on ${D}"
            ${HAVE_SUDO}  bash -c "${HAVE_SMARTCTL} --quietmode=errorsonly --set=apm,off /dev/${D}"

            echo "Disabling standby on ${D}"
            ${HAVE_SUDO}  bash -c "${HAVE_SMARTCTL} --quietmode=errorsonly --set=standby,off /dev/${D}"

            echo "Disabling write cache on ${D}"
            ${HAVE_SUDO}  bash -c "${HAVE_SMARTCTL} --quietmode=errorsonly --set=wcache,off /dev/${D}"
        fi

    done
done

