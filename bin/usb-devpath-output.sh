#!/bin/bash

# from
# https://unix.stackexchange.com/questions/144029/command-to-determine-ports-of-a-device-like-dev-ttyusb0/144735
# note also:
# udevadm info -a -p  $(udevadm info -q path -n /dev/ttyUSB*)

for sysdevpath in $(find /sys/bus/usb/devices/usb*/ -name dev); do
    (
        # Want the directory path so strip off /dev
        syspath="${sysdevpath%/dev}"
        # Path in /dev that corresponds to /sys device
        devname="$(udevadm info -q name -p $syspath)"
        # Filter out things which aren't actual devices
        [[ "$devname" == "bus/"* ]] && continue
        # Lists device properties in a format that can be parsed by the shell into variables
        eval "$(udevadm info -q property --export -p $syspath)"
        # More filtering of things that aren't actual devices.
        [[ -z "$ID_SERIAL" ]] && continue
        echo -e "/dev/$devname \t $ID_VENDOR_ID:$ID_MODEL_ID"
        echo -e "\t\t $ID_VENDOR $ID_VENDOR_FROM_DATABASE $ID_MODEL $ID_MODEL_FROM_DATABASE"
    )
done
