#!/usr/bin/env bash

# async zfs import

# Copyright (C) 2025 Peter Lawler <relwalretep@gmail.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -o errexit  # Exit on any error

pool_name="$1"
timeout_in_seconds="300"
echo "SCRIPT_TIMEOUT=${timeout_in_seconds}"

coproc import_process { zpool import -N "$pool_name"; }
pid=${import_process[0]}

if timeout "$timeout_in_seconds" wait "$pid"; then
    import_status=0
else
    import_status=124
fi

exit_code=0  # Initialize exit code to success

if [[ "$import_status" -eq 0 ]]; then
    zfs mount -a
    echo "Pool '$pool_name' imported and mounted successfully."
else
    echo "Error: Pool '$pool_name' import timed out." >&2
    exit_code=1
fi

zpool_status=$(zpool status "$pool_name" 2>&1)
echo "$zpool_status" >&2  # Log the status

# Kill the import process if it's still running (combined kill and error check)
kill -TERM "$pid" 2>/dev/null || echo "Warning: Error killing import process" >&2


exit "$exit_code"