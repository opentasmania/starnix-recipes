#!/bin/bash -e

##########################################################################
# Lazy person's *nix updater
# Copyright (C) Peter Lawler
# Snail mail: PO Box 195
#             Lindisfarne, Tasmania
#             AUSTRALIA 7015
# email:      relwalretep@gmail.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
##########################################################################

# Shellcheck disable
# SC2034: VARIABLE appears unused.
# SC2086: Double quote to prevent globbing and word splitting.
# shellcheck disable=2034,2086

# We don't want 'set -e' as we're testing for existence of update commands
# using subshells which is not necessarily useful
# See http://mywiki.wooledge.org/BashFAQ/105
# set -e
set -u
set -o pipefail
IFS=$'\n\t'

function fail {
  echo "$1" >&2
  exit 1
}

function retry {
  local retry_count=0
  local max_retries=5
  local retry_delay=15
  while true; do
    "$@" && break || {
      ((retry_count++))
      if [[ $retry_count -lt $max_retries ]]; then
        echo "Command failed on attempt $retry_count of $max_retries. Retrying in $retry_delay seconds..."
        sleep $retry_delay
      else
        fail "The command has failed after $retry_count attempts."
      fi
    }
  done
}

function wait_for_file {
  local file="$1"
  local wait_seconds="${2:-10}"

  if [ "$wait_seconds" -lt 1 ]; then
    echo 'At least 1 second is required'
    return 1
  fi

  until [ "$wait_seconds" -eq 0 ] || [ -e "$file" ]; do
    sleep 1
    wait_seconds=$((wait_seconds - 1))
  done

  if [ -e "$file" ]; then
    return 0 # File appeared within wait time
  else
    return 1 # Timeout reached without file appearing
  fi
}

function command_exists {
  command -v "$1" >/dev/null 2>&1
}

function init_text_formatting {
  if command_exists tput; then
    TEXT_BOLD="$(tput bold)"
    TEXT_UL_ON="$(tput smul)"
    TEXT_UL_OFF="$(tput rmul)"
    TEXT_REVERSE="$(tput rev)"
    TEXT_NORMAL="$(tput sgr0)"
    TEXT_RED=$(tput setaf 1)
    TEXT_GREEN=$(tput setaf 2)
    TEXT_YELLOW=$(tput setaf 3)
    TEXT_GREY=$(tput setaf 8)
  else
    TEXT_BOLD=""
    TEXT_UL_ON=""
    TEXT_UL_OFF=""
    TEXT_REVERSE=""
    TEXT_NORMAL=""
    TEXT_RED=""
    TEXT_GREEN=""
    TEXT_YELLOW=""
    TEXT_GREY=""
  fi
}

function detect_os() {
  if [[ ! -f /etc/os-release ]]; then
    echo "${TEXT_BOLD}Initial OS detection failure (no /etc/os-release)${TEXT_NORMAL}"
    [[ -n $BEEP ]] && $BEEP
    exit 1
  fi
  if [[ -f /etc/openwrt_release ]]; then
    OS="OpenWRT"
  else
    OS=$(hostnamectl status | grep "Operating System" | cut -d ':' -f2 | sed 's/^[[:space:]]//g')
  fi
  [[ -z "$OS" ]] && fail "OS NOT DETECTED. EXITING"
  echo "Detected OS: ${TEXT_UL_ON}${OS}${TEXT_UL_OFF}"
}

function setup_package_manager() {
  if [[ $OS == *"OpenWRT"* ]]; then
    PACKAGE_MANAGER=$(command -v opkg 2>/dev/null)
    [[ -z "$PACKAGE_MANAGER" ]] && fail "opkg(1) not found (System: ${OS})"
  elif [[ $OS == *"Debian"* || $OS == *"Raspbian"* || $OS == *"Kali"* ]]; then
    PACKAGE_MANAGER=$(command -v apt 2>/dev/null)
    [[ -z "$PACKAGE_MANAGER" ]] && fail "apt(1) not found (System: ${OS})"
  elif [[ $OS == *"CentOS"* ]]; then
    fail "${OS} not implemented"
  else
    fail "${OS} not supported"
  fi
  echo "${PACKAGE_MANAGER}(1) found at $PACKAGE_MANAGER"
}

function init() {
  local opt
  # BEEP="$(command -v beep 2>/dev/null)"
  # ${BEEP}
  unset APT_LOCK_TIMEOUT
  unset CLEAN_OPT
  unset DUMMY_OPT
  unset PURGE_OPT
  CLEAN_OPT="true"
  DUMMY_OPT="false"
  PURGE_OPT="false"
  APT_LOCK_TIMEOUT="15"
  init_text_formatting
  detect_os
  setup_package_manager

  while getopts "cfp" opt; do
    case $opt in
    c) echo "Setting ${TEXT_REVERSE}clean${TEXT_NORMAL}" && CLEAN_OPT="true" ;;
    f) echo "Setting ${TEXT_REVERSE}flash${TEXT_NORMAL}" && FLASH_OPT="true" ;;
    p) [[ $OS == *"CentOS"* ]] && fail "-p not available on ${OS}" || echo "Setting ${TEXT_REVERSE}purge${TEXT_NORMAL}" && PURGE_OPT="true" ;;
    *) exit 1 ;;
    esac
  done
}

function check_sudo() {
  if [[ $UID -ne 0 ]]; then
    echo "Not root user"
    command_exists sudo || echo "su -c \"apt --assume-yes install sudo\""
    HAVE_SUDO=$(command -v sudo 2>/dev/null)
    echo "sudo(1) found at $HAVE_SUDO"
  else
    echo "Running as root, sudo(1) not needed."
    HAVE_SUDO=""
  fi
}

function apt_check() {
  local PURGE_LIST
  local RELEASE
  local APT_CONFIG
  local DEINSTALL_LIST
  local UPGRADABLE
  local AUTOREMOVE
  echo "Checking apt"
  RELEASE="$(grep PRETTY_NAME /etc/os-release | awk '{print $3}' | sed s/\"//g)"
  #    APT_CONFIG="-o Acquire::http::AllowRedirect=false"
  APT_CONFIG=""
  echo "Release ${RELEASE}, using ${TEXT_UL_ON}${PACKAGE_MANAGER}(8)${TEXT_UL_OFF}"
  if [[ "${CLEAN_OPT}" == "true" ]]; then
    echo "${TEXT_BOLD}${HAVE_SUDO} ${PACKAGE_MANAGER} clean all${TEXT_NORMAL}" &&
      ${HAVE_SUDO} ${PACKAGE_MANAGER} clean all
  fi
  echo "${TEXT_BOLD}${HAVE_SUDO} ${PACKAGE_MANAGER} update${TEXT_NORMAL}" &&
    retry ${HAVE_SUDO} ${PACKAGE_MANAGER} ${APT_CONFIG} update
  set +e
  UPGRADABLE=$(apt --option Debug::NoLocking=true list --upgradable 2>/dev/null | grep -v Listing)
  set -e
  if [[ -n ${UPGRADABLE} ]]; then
    echo "${TEXT_BOLD}${HAVE_SUDO} ${PACKAGE_MANAGER} ${APT_CONFIG} --assume-yes upgrade${TEXT_NORMAL}" &&
      retry ${HAVE_SUDO} ${PACKAGE_MANAGER} ${APT_CONFIG} --assume-yes upgrade &&
      echo "${TEXT_BOLD}${HAVE_SUDO} ${PACKAGE_MANAGER} ${APT_CONFIG} --assume-yes dist-upgrade${TEXT_NORMAL}" &&
      retry ${HAVE_SUDO} ${PACKAGE_MANAGER} ${APT_CONFIG} --assume-yes dist-upgrade
  else
    echo "Nothing to upgrade"
  fi
  if [[ "${PURGE_OPT}" == "true" ]]; then
    set +e
    AUTOREMOVE=$(apt --dry-run autoremove 2>/dev/null | grep -Po '^Remv \K[^ ]+')
    set -e
    if [[ -n ${AUTOREMOVE} ]]; then
      echo "${TEXT_BOLD}Purging${TEXT_NORMAL} via autoremove" &&
        ${HAVE_SUDO} ${PACKAGE_MANAGER} --assume-yes --purge autoremove
    fi
    set +e
    PURGE_LIST="$(dpkg --list | grep '^rc' | awk '{ print $2 }')"
    set -e
    if [[ -n "${PURGE_LIST}" ]]; then
      echo "Purging via list"
      ${HAVE_SUDO} ${PACKAGE_MANAGER} --assume-yes purge $PURGE_LIST
    fi
    set +e
    DEINSTALL_LIST="$(dpkg --get-selections | grep deinstall | cut -f 1)"
    set -e
    if [[ -n ${DEINSTALL_LIST} ]]; then
      echo "Deinstalling"
      ${HAVE_SUDO} ${PACKAGE_MANAGER} --assume-yes purge $DEINSTALL_LIST
    fi
  fi
}

function opkg_check() {
  local UPGRADABLE
  # TODO: work out how to exclude currently active luci theme (if being used) until the very end
  ${HAVE_SUDO} opkg update
  UPGRADABLE="$(${HAVE_SUDO} opkg list-upgradable | awk '{print $1}')"
  if [[ -n "${UPGRADABLE}" ]]; then
    echo "Checking opkg"
    for upgrade in ${UPGRADABLE}; do
      echo "Upgrading $upgrade"
      ${HAVE_SUDO} bash -c "opkg upgrade $upgrade"
    done
  else
    echo "No upgradable packages detected."
  fi
}

function pip_check() {
  local PIP
  local PIP_TYPES
  local PIP_UPDATES
  # TODO: Decide if I need to split this to cover pre python 3.(7?) changed or desupport earlier versions
  PIP=""
  PIP_TYPES=(pip2 pip3)
  PIP_UPDATES=""
  #shellcheck disable=SC2068
  for pip in ${PIP_TYPES[@]}; do
    if (command -v $pip >/dev/null 2>&1); then
      PIP=$(command -v $pip)
      echo "$PIP upgrades"
      # get list of user pip installed foo
      PIP_UPDATES=$("$PIP" freeze --user | sed s/==/\ /g | cut -d \  -f 1)
      if [[ "$PIP_UPDATES" != "" ]]; then
        for update in ${PIP_UPDATES[@]}; do
          echo "Checking $TEXT_BOLD$update$TEXT_NORMAL"
          if [[ -n ${DUMMY_OPT} ]]; then
            $PIP install "$update" --upgrade
          fi
        done
      fi
    fi
  done
}

function venv_check() {
  if (command -v lsvirtualenv >/dev/null 2>&1); then
    echo "Checking python virtual environments"
    for v in $(lsvirtualenv -b); do
      echo "Checking ${v}"
      workon $v
      pip_check
      deactivate
    done
  fi
}

function bb_update() {
  local BBDOTORG_OVERLAYS_GIT_LOCATION
  local DOWNLOAD_LOCATION
  local KERNEL_RELEASE
  local KERNEL_UPDATE_REPO
  local KERNEL_UPDATE_REPO
  local RELEASE
  local VERSION
  local LTS_SWITCH
  local AVAILABLE
  local CURRENT_KERNEL
  local KERNEL_PACKAGES_TO_REMOVE
  local pkg_check
  local deb_pkgs
  local check_for_overlays
  BBDOTORG_OVERLAYS_GIT_LOCATION=https://github.com/BeagleBoard/bb.org-overlays.git
  DOWNLOAD_LOCATION=/var/tmp
  KERNEL_RELEASE="$(uname -r)"
  KERNEL_UPDATE_REPO="https://rcn-ee.com/repos/latest/"
  RELEASE="$(grep ^VERSION_CODENAME /etc/os-release | cut -d '=' -f 2)"
  if [[ -z "${RELEASE}" ]]; then
    RELEASE="$(cat /etc/debian_version | cut -d '/' -f 1)"
  fi
  if [[ -z "${RELEASE}" ]]; then
    echo "Cannot determine RELEASE"
    exit 2
  fi
  # Need to do something with setting this from the cmd line, and/or from current kernel version etc.
  VERSION="LTS$(uname -r | cut -d \. -f 1,2 | sed s/\\.//)"
  LTS_SWITCH=$(grep ${VERSION}: /opt/scripts/tools/update_kernel.sh | cut -f 3 -d \  | tr -d \")

  AVAILABLE="$(curl --silent ${KERNEL_UPDATE_REPO}/${RELEASE}-armhf/LATEST-ti | grep ${VERSION} | awk '{print $3}')"

  if [[ "${AVAILABLE}" == "${KERNEL_RELEASE}" ]]; then
    echo "No BB kernel update required: ${KERNEL_RELEASE}"
    return
  else
    echo "Updating ${KERNEL_RELEASE} to ${AVAILABLE}"
  fi

  # update the machine to ensure caches are full before checking for packages
  echo "apt update" && ${HAVE_SUDO} ${PACKAGE_MANAGER} update
  echo "-----------------------------------"

  # package check
  # TODO: Needs fixing
  echo "Checking for support tools"
  deb_pkgs=()
  pkg_check=('bash-completion' 'bison' 'build-essential' 'curl' 'flex' 'git' 'man')
  for pkg in "${pkg_check[@]}"; do
    if ! dpkg --list | awk '{print $2}' | grep -q "^${pkg}"; then
      deb_pkgs+=("$pkg")
    fi
  done
  if [[ ${#deb_pkgs[@]} -ne 0 ]]; then
    echo "Installing: ${deb_pkgs[*]}"
    "${HAVE_SUDO}" "${PACKAGE_MANAGER}" --assume-yes install "${deb_pkgs[@]}"
    "${HAVE_SUDO}" "${PACKAGE_MANAGER}" clean
  fi

  echo "-----------------------------------"

  # remove old kernels, ugly but effective
  # TODO: if something got removed here, we're possibly on a 'new' kernel and thus need to install the overlays below
  echo "Checking for old kernels"
  CURRENT_KERNEL=$(uname -r)
  KERNEL_PACKAGES_TO_REMOVE=$(dpkg --list | grep 'linux.*image' | grep -Ev "base|${CURRENT_KERNEL}" | grep ^ii | awk '{print $2}')
  if [[ "$KERNEL_PACKAGES_TO_REMOVE" != "" ]]; then
    ${HAVE_SUDO} ${PACKAGE_MANAGER} --assume-yes purge $KERNEL_PACKAGES_TO_REMOVE
    check_for_overlays=""
  else
    echo "No old kernels to remove"
    check_for_overlays="TRUE"
  fi

  echo "-----------------------------------"

  # upgrade the machine
  # need to do better checks and exit rather than just the &&
  # NOTE: We don't --assume-yes here for safety's sake.
  # echo "apt update" && ${HAVE_SUDO} ${PACKAGE_MANAGER}  update && \
  #    echo "apt upgrade" && ${HAVE_SUDO} ${PACKAGE_MANAGER}  upgrade && \
  #    echo "apt dist-upgrade" && ${HAVE_SUDO} ${PACKAGE_MANAGER}  dist-upgrade && \
  #    echo "apt --purge autoremove" && ${HAVE_SUDO} ${PACKAGE_MANAGER}  --purge autoremove && \
  #    echo "apt autoclean" && ${HAVE_SUDO} ${PACKAGE_MANAGER}  autoclean && \
  #    echo "-----------------------------------"

  # update official support scripts
  echo "Checking for official support scripts"
  if [[ -f /opt/scripts/.git/config ]]; then
    echo "git -C /opt/scripts pull"
    git -C /opt/scripts pull
  fi
  echo "-----------------------------------"

  # ensure there's a nice working directory for downloads
  echo "Checking for download directory"
  if [[ ! -d ${DOWNLOAD_LOCATION}/ ]]; then
    echo "Making download directory"
    ${HAVE_SUDO} mkdir -p ${DOWNLOAD_LOCATION}/
    ${HAVE_SUDO} chown debian:debian ${DOWNLOAD_LOCATION}/
  fi
  echo "-----------------------------------"

  # if the overlays haven't already been grabbed, get them
  echo "Checking for overlays"
  if [[ ! -d ${DOWNLOAD_LOCATION}/bb.org-overlays/.git ]]; then
    echo "Cloning overlays"
    git -C ${DOWNLOAD_LOCATION} clone ${BBDOTORG_OVERLAYS_GIT_LOCATION}
  else
    echo "Updating overlays"
    git -C ${DOWNLOAD_LOCATION}/bb.org-overlays pull
  fi
  echo "-----------------------------------"

  # check for overlays
  if [[ ${check_for_overlays} ]]; then
    echo "Checking for overlays installed"
    # do stuff
  fi
  echo "-----------------------------------"

  # update to the latest kernel, if successful also install the firmware image
  echo "Updating kernel to ${AVAILABLE}"
  ${HAVE_SUDO} /opt/scripts/tools/update_kernel.sh --ti-channel "${LTS_SWITCH}" # && \
  #   ${HAVE_SUDO} ${PACKAGE_MANAGER} install linux-firmware-image-"${AVAILABLE}"

}

function rpi_update() {
  if (command -v rpi-update >/dev/null 2>&1); then
    ${HAVE_SUDO} rpi-update
  fi
}

function flatpak_check() {
  if (command -v flatpak >/dev/null 2>&1); then
    echo "Flatpak user updates"
    flatpak --assumeyes --user update
    echo "Flatpak system updates"
    ${HAVE_SUDO} flatpak --assumeyes --system update
  fi
}

UTCDATE="$(TZ=UTC date +%Y-%m-%d\ %H:%M:%S\ %Z)"
LOCALDATE="$(date +%Y-%m-%d\ %H:%M:%S\ %Z)"
echo "Starting up at ${LOCALDATE} [${UTCDATE}]"

function main() {
  check_sudo
  if [[ "$OS" == *"OpenWRT"* ]]; then
    opkg_check
  elif [[ "$OS" == *"Debian"* ]] || [[ "$OS" == *"Raspbian"* ]] || [[ "$OS" == *"Kali"* ]]; then
    apt_check
    # embedded linux systems
    if [[ -f /etc/dogtag ]] || [[ -f /etc/rpi-issue ]]; then
      if [[ -f /etc/dogtag ]]; then
        bb_update
      else
        if [[ "${FLASH_OPT}" == "true" ]]; then
          rpi_update
        fi
      fi
    fi
    pip_check
    venv_check
    flatpak_check
    if (command -v /usr/sbin/needrestart >/dev/null 2>&1); then
      /usr/sbin/needrestart -r l -k -l -w
    else
      echo "needrestart(1) not found"
    fi
  fi
}

init "$@"

wait_for_file /var/lib/apt/lists/lock ${APT_LOCK_TIMEOUT} || {
  echo "apt lock timeout"
  exit 1
}
main

UTCDATE="$(TZ=UTC date +%Y-%m-%d\ %H:%M:%S\ %Z)"
LOCALDATE="$(date +%Y-%m-%d\ %H:%M:%S\ %Z)"
echo "Finishing up at ${LOCALDATE} [${UTCDATE}]"
exit 0
