#!/bin/bash
# qnap-helper.sh - Help keep my qnap's firmware backed up and restorable'
# Copyright (C) Peter Lawler
# Snail mail: PO Box 195
#             Lindisfarne, Tasmania
#             AUSTRALIA 7015
# email:      relwalretep@gmail.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

# References:
# https://wiki.qnap.com/wiki/Debian_Installation_On_QNAP
# https://wiki.qnap.com/wiki/NAS_Firmware_Update_When_No_HDD(s)_Installed
# http://www.cyrius.com/debian/kirkwood/qnap/
# http://gentoo-en.vfose.ru/wiki/QNAP_TS-41X


DEV_PATH=sdz1
MOUNT_PATH=media
UTCDATE=$(TZ=UTC date +%Y-%m-%d:%H:%M:%S" "%Z)
UTCDATE_FILENAME=$(TZ=UTC date +%Y-%m-%d:%H:%M:%S"-"%Z)


dev_check () {
    if [ ! -e "/dev/${1}" ]; then
        echo "${1} doesn't exist!"
        exit 1
    fi
}

mount () {
    dev_check "${DEV_PATH}"
    if [ ! -d "/${MOUNT_PATH}/${DEV_PATH}" ]; then
        mkdir /${MOUNT_PATH}/${DEV_PATH}
    fi
    mount /dev/${DEV_PATH} /${MOUNT_PATH}/${DEV_PATH}

}

umount () {
    dev_check "${DEV_PATH}"
    if [ -d "/${MOUNT_PATH}/${DEV_PATH}" ]; then
        umount /${MOUNT_PATH}/${DEV_PATH}
    fi
}

backup_flash () {
    echo "Backing up flash at ${UTCDATE}"
    if [ ! -d "/${MOUNT_PATH}/${DEV_PATH}/${UTCDATE_FILENAME}" ]; then
        mkdir "/${MOUNT_PATH}/${DEV_PATH}/${UTCDATE_FILENAME}"
    fi
    LATEST_INITRD="$(find /boot -name 'initrd*' -type f -printf '%T@ %P\n' 2>/dev/null|sort -n|tail -1|awk '{print $2}')"
    LATEST_VMLINUZ="$(find /boot -name 'vmlinuz*' -type f -printf '%T@ %P\n' 2>/dev/null|sort -n|tail -1|awk '{print $2}')"
    dd if="/boot/${LATEST_INITRD}" of="/${MOUNT_PATH}/${DEV_PATH}/${UTCDATE_FILENAME}/${LATEST_INITRD}".padded ibs=9437184 conv=sync
    cp "/boot/${LATEST_VMLINUZ}" "/${MOUNT_PATH}/${DEV_PATH}/${UTCDATE_FILENAME}/."

    for f in /dev/mtdblock*;do
        sudo bash -c "echo 'Backing up ${f}' && cat ${f} > /${MOUNT_PATH}/${DEV_PATH}/${UTCDATE_FILENAME}/$(basename \\"$f\\")"
    done

}

restore_flash () {
    echo "INCOMPLETE"
    exit 1

    echo "Restoring up flash at ${UTCDATE}"
    cat /${MOUNT_PATH}/${DEV_PATH}/vmlinuz > /dev/mtdblock1
    cat /${MOUNT_PATH}/${DEV_PATH}/initrd.padded > /dev/mtdblock2

    echo "Please reboot your device"
}

chroot_reflash () {

    echo "INCOMPLETE"
    exit 1
    mount drive
    cd drive. || exit
    mount -t proc proc proc/
    mount -t sysfs sys sys/
    mount -o bind /dev dev/
    mount -o bind /dev/pts dev/pts
    chroot drive /bin/bash
    CURRENT_INSTALLED_KERNEL=$(uname -r)

    apt install --reinstall "linux-image-${CURRENT_INSTALLED_KERNEL}"

}

case $1 in
    deb_install)
        if [ ! -d /etc/qsync ]; then
            echo "This doesn't look like a QNAP box. Exiting."
            exit 1
        fi
        cd /tmp || exit
        # cpuinfo should contain "Processor name" or "Processor"
        cpu=$(grep "^Processor" /proc/cpuinfo)
        if [ -z "$cpu" ]; then
            echo "Cannot determine CPU from /proc/cpuinfo"
            exit 1
        fi
    case "$cpu" in
        *"ARM926EJ"*)
            kernel="kernel"
            ;;
        *"88F6281"*)
            kernel="kernel-6281"
            ;;
        *"88F6282"*)
            kernel="kernel-6282"
            ;;
        *)
            echo "Cannot determine CPU variant"
            exit 1
            ;;
    esac
        busybox wget http://ftp.debian.org/debian/dists/stable/main/installer-armel/current/images/kirkwood/network-console/qnap/ts-41x/initrd
        busybox wget http://ftp.debian.org/debian/dists/stable/main/installer-armel/current/images/kirkwood/network-console/qnap/ts-41x/${kernel}
        busybox wget http://ftp.debian.org/debian/dists/stable/main/installer-armel/current/images/kirkwood/network-console/qnap/ts-41x/flash-debian
        busybox wget http://ftp.debian.org/debian/dists/stable/main/installer-armel/current/images/kirkwood/network-console/qnap/ts-41x/model
        sh flash-debian
        ;;
    backup)
        backup_flash
        ;;
    restore)
        restore_flash
         ;;
    reflash)
        chroot_reflash
        ;;
    *)
        echo "Usage: $0 {backup|deb_install|restore|reflash}"
        exit 1
       ;;
esac

exit 0
