#!/bin/bash

# starnix-ufw.sh - set up basic ufw(1) rules and enables them
# Copyright (C) Peter Lawler <relwalretep@gmail.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
#along with this program.  If not, see <https://www.gnu.org/licenses/>.

# TODO:
#   Detect VPN connections
#   Accept arg as $MYNETWORK
#   Detect and work alongside fail2ban
#   Multiple addresses per single interface
#   IPv6

set -e
set -o pipefail
set -u
IFS=$'\n\t'

init () {

    unset DRY_RUN
    unset HAVE_SUDO
    unset HAVE_UFW
    unset INTERFACES
    unset MY_NETWORK
    unset MY_CIDR
    unset MY_IPV4
    unset OPTS
    unset REMOTE_IPV4
    unset VERBOSE
    unset VERBOSE_BASH
    unset VERBOSE_UFW
    unset VERBOSITY

    DRY_RUN=""
#   DRY_RUN="--dry-run"
    HAVE_SUDO=""
    HAVE_UFW=""
    HELP=""
    INTERFACES=""
    MY_NETWORK=""
    MY_IPV4=""
    OPTS=""
    REMOTE_IPV4="any"
    UFW_DRY_RUN=""
    UFW_VERBOSE=""
    VERBOSE=""
    VERBOSE_BASH=""
    VERBOSE_UFW=""
    VERBOSITY=""
}

check_sudo () {
    if [ ${UID} != "0" ]; then
        echo "Not root user"
        if ! hash sudo 2>/dev/null; then
            echo "su --login --command \"apt --assume-yes install sudo\""
            su --login --command "apt --assume-yes install sudo"
        fi
        HAVE_SUDO=$(command -v sudo 2>/dev/null)
        echo "sudo(1) found at ${HAVE_SUDO}"
    else
        /bin/echo "Running as root, sudo(1) not needed."
        HAVE_SUDO=""
    fi
}

check_prereq () {
    if [ -f /etc/os-release ]; then
        if /bin/grep "ID=debian" /etc/os-release || /bin/grep "ID=raspbian" /etc/os-release; then
            if ! [ /usr/bin/dpkg-query -s ufw > /dev/null 2>&1 ]; then
                $HAVE_SUDO apt --assume-yes install ufw
            fi
        else
            /bin/echo "Only designed for Debian systems"
            exit 1
        fi
    else
        /bin/echo "Can't determine OS type"
        exit 1
    fi
    HAVE_UFW=$($HAVE_SUDO bash -c $VERBOSE_BASH "command -v ufw" 2>/dev/null)
    /bin/echo "ufw(1) found at $HAVE_UFW"
}

interface_detect () {
    INTERFACES=$(/bin/ip link show | /bin/grep -E "UP|LOWER_UP" | /bin/grep -Ev "DOWN|LOOPBACK" | /usr/bin/awk '{print $2}'| /bin/sed s/://g )

    SAVEIFS=$IFS
    IFS=$'\n'
    INTERFACES=($INTERFACES)
    IFS=$SAVEIFS
    if [ "${INTERFACES}" = "" ]; then
        /bin/echo "No interfaces detected"
        exit 1
    else
        /bin/echo "Interfaces detected:"
        /bin/echo "${INTERFACES[*]}"
    fi
    /bin/echo "REMOTE_IPV4=$REMOTE_IPV4"
}

ufw_reset () {
    $HAVE_SUDO bash -c $VERBOSE_BASH "/usr/sbin/ufw $UFW_DRY_RUN reset"
    $HAVE_SUDO bash -c $VERBOSE_BASH "/usr/sbin/ufw $UFW_DRY_RUN disable"
    $HAVE_SUDO bash -c $VERBOSE_BASH "/usr/sbin/ufw $UFW_DRY_RUN default allow outgoing"
    $HAVE_SUDO bash -c $VERBOSE_BASH "/usr/sbin/ufw $UFW_DRY_RUN default deny incoming"
}

fail2ban_ufw () {
    if [ -f /etc/fail2ban/action.d/ufw.conf ]; then
        $HAVE_SUDO bash -c $VERBOSE_BASH "sed --in-place s/app$/app\ comment\ \'fail2ban\'/g /etc/fail2ban/action.d/ufw.conf"
        $HAVE_SUDO bash -c $VERBOSE_BASH "printf '[DEFAULT]\nbanaction=ufw\n' > /etc/fail2ban/jail.local"
        $HAVE_SUDO bash -c $VERBOSE_BASH "systemctl restart fail2ban"
    fi
}

ufw_program () {
    # INTERFACES=$(/bin/ip link show | /bin/grep -E "UP|LOWER_UP" | /bin/grep -Ev "DOWN|LOOPBACK" | /usr/bin/awk '{print $2}'| /bin/sed s/://g )

    # TODO: programtically determine the network n mask n stuff

    for MY_INTERFACE in ${INTERFACES[*]}; do
        if [ "$VERBOSE" ]; then
            /bin/echo "INTERFACE  : $MY_INTERFACE"
        fi
        # TODO: Fix, this is not ideal because netmasks aren't always 255 but let's just go with it...
        unset MY_NETWORK
        unset MY_CIDR
        unset MY_IPV4
        # TODO: 'Fix' docker and other types of network devices
        MY_NETWORK="$(/bin/ip addr show up $MY_INTERFACE | /bin/grep "inet " | /bin/grep "global" | grep -v docker |/usr/bin/awk '{print $4}' | /usr/bin/cut -f1 -d "/" | sed s/255/0/g|head -1)"
        MY_IPV4="$(/bin/ip addr show up $MY_INTERFACE | /bin/grep "inet " -A1 | /bin/grep "global" | grep -v docker | /usr/bin/awk '{print $2}' | /usr/bin/cut -f1 -d "/"|head -1)"
        MY_CIDR="$(/bin/ip addr show up $MY_INTERFACE | /bin/grep "inet " -A1 | /bin/grep "global" | grep -v docker | /bin/grep -o '[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\/[0-9]\{1,\}' | /usr/bin/cut -f2 -d "/"|head -1)"
        /bin/echo "MY_NETWORK : ${MY_NETWORK}"
        /bin/echo "MY_IPV4    : ${MY_IPV4}"
        /bin/echo "MY_CIDR    : ${MY_CIDR}"
        if [ "${MY_IPV4}" != "" ]; then
            $HAVE_SUDO bash -c $VERBOSE_BASH "/usr/sbin/ufw $UFW_DRY_RUN $UFW_VERBOSE allow in on $MY_INTERFACE proto tcp from $REMOTE_IPV4 to $MY_IPV4 port ssh comment 'ssh'"
            # TODO: Use an command line arg to accept a string of options
            # mDNS
            # $HAVE_SUDO bash -c $VERBOSE_BASH "/usr/sbin/ufw $UFW_DRY_RUN $UFW_VERBOSE allow in on $MY_INTERFACE proto udp from $MY_NETWORK/$MY_CIDR to 224.0.0.251 port mdns comment 'mdns'"
            # All hosts multicast
            # $HAVE_SUDO bash -c $VERBOSE_BASH "/usr/sbin/ufw $UFW_DRY_RUN $UFW_VERBOSE allow in on $MY_INTERFACE proto igmp from $MY_NETWORK/$MY_CIDR to 224.0.0.1 comment 'all hosts multicast'"
            # SSDPS
            # $HAVE_SUDO bash -c $VERBOSE_BASH "/usr/sbin/ufw $UFW_DRY_RUN $UFW_VERBOSE allow in on $MY_INTERFACE proto igmp from $MY_NETWORK/$MY_CIDR to 239.255.255.250 comment 'SSDPS'"
            # $HAVE_SUDO bash -c $VERBOSE_BASH "/usr/sbin/ufw $UFW_DRY_RUN $UFW_VERBOSE allow in on $MY_INTERFACE proto tcp from any to $MY_NETWORK/$MY_CIDR port http comment 'http'"
            # $HAVE_SUDO bash -c $VERBOSE_BASH "/usr/sbin/ufw $UFW_DRY_RUN $UFW_VERBOSE allow in on $MY_INTERFACE proto tcp from any to $MY_NETWORK/$MY_CIDR port https comment 'https'"
            # $HAVE_SUDO bash -c $VERBOSE_BASH "/usr/sbin/ufw $UFW_DRY_RUN $UFW_VERBOSE allow in on $MY_INTERFACE proto tcp from any to $MY_NETWORK/$MY_CIDR port nfs comment 'nfs'"
            # $HAVE_SUDO bash -c $VERBOSE_BASH "/usr/sbin/ufw $UFW_DRY_RUN $UFW_VERBOSE allow in on $MY_INTERFACE proto tcp from any to $MY_NETWORK/$MY_CIDR port 5901 comment 'vnc'"
            # # https://kubernetes.io/docs/tasks/tools/install-kubeadm/
            # # Master node
            # $HAVE_SUDO bash -c $VERBOSE_BASH "/usr/sbin/ufw $UFW_DRY_RUN $UFW_VERBOSE allow in on $MY_INTERFACE proto tcp from $MY_NETWORK/$MY_CIDR to any port 6443 comment 'kube API server'"
            # $HAVE_SUDO bash -c $VERBOSE_BASH "/usr/sbin/ufw $UFW_DRY_RUN $UFW_VERBOSE allow in on $MY_INTERFACE proto tcp from $MY_NETWORK/$MY_CIDR to any port 2379:2380 comment 'etcd client server API'"
            # $HAVE_SUDO bash -c $VERBOSE_BASH "/usr/sbin/ufw $UFW_DRY_RUN $UFW_VERBOSE allow in on $MY_INTERFACE proto tcp from $MY_NETWORK/$MY_CIDR to any port 10250 comment 'kubelet API'"
            # $HAVE_SUDO bash -c $VERBOSE_BASH "/usr/sbin/ufw $UFW_DRY_RUN $UFW_VERBOSE allow in on $MY_INTERFACE proto tcp from $MY_NETWORK/$MY_CIDR to any port 10251 comment 'kube scheduler'"
            # $HAVE_SUDO bash -c $VERBOSE_BASH "/usr/sbin/ufw $UFW_DRY_RUN $UFW_VERBOSE allow in on $MY_INTERFACE proto tcp from $MY_NETWORK/$MY_CIDR to any port 10252 comment 'kube-controller-manager'"
            # # Worker nodes
            # $HAVE_SUDO bash -c $VERBOSE_BASH "/usr/sbin/ufw $UFW_DRY_RUN $UFW_VERBOSE allow in on $MY_INTERFACE proto tcp from $MY_NETWORK/$MY_CIDR to any port 10250 comment 'kubelet API'"
            # $HAVE_SUDO bash -c $VERBOSE_BASH "/usr/sbin/ufw $UFW_DRY_RUN $UFW_VERBOSE allow in on $MY_INTERFACE proto tcp from $MY_NETWORK/$MY_CIDR to any port 30000:32767 comment 'NodePort Services***'"
        fi
    done
    $HAVE_SUDO bash -c $VERBOSE_BASH "/usr/sbin/ufw $UFW_DRY_RUN logging low"
}

ufw_enable () {
    if ! [ "${DRY_RUN}" ]; then
        # flush all chains
        if [ "$VERBOSE" ]; then
            /bin/echo "Flushing chains"
        fi
        $HAVE_SUDO iptables -F
        $HAVE_SUDO iptables -t nat -F
        $HAVE_SUDO iptables -t mangle -F
        if [ "$VERBOSE" ]; then
            /bin/echo "Deleting all chains"
        fi
        $HAVE_SUDO iptables -X
        $HAVE_SUDO iptables -t nat -X
        $HAVE_SUDO iptables -t mangle -X
    fi
    $HAVE_SUDO bash -c $VERBOSE_BASH "/usr/sbin/ufw $UFW_DRY_RUN enable"
    # $HAVE_SUDO bash -c $VERBOSE_BASH" "/usr/sbin/ufw $UFW_DRY_RUN show raw"
    $HAVE_SUDO bash -c $VERBOSE_BASH "/usr/sbin/ufw $UFW_DRY_RUN status $UFW_VERBOSE numbered"
}

show_help () {
    /bin/echo "debian_helper_ufw.sh - quick and dirty setup of ufw"
    /bin/echo "-h --help Help"
    /bin/echo "-r --remote-address [ADDRESS] Remote IPv4 address"
    /bin/echo "-v --verbose Verbose"
    /bin/echo "-d --dry-run Dry run ***WARNING: Not fully implemented***"
}


# MAIN
init

# OPTS=$(getopt --options dhv --longoptions dry-run,help,verbose -n 'parse-options' -- "$@")
# if [ $? != 0 ] ; then
if ! OPTS=$(getopt --options dhr:v --longoptions dry-run,help,remote-ip:,verbose -n 'parse-options' -- "$@") ; then
    /bin/echo "Failed parsing options." >&2
    show_help
    exit 1
fi
eval set -- "$OPTS"
while true; do
    case "$1" in
        -d | --dry-run )
            DRY_RUN=true
            UFW_DRY_RUN="--dry-run"
            echo "WARNING: --dry-run **NOT FULLY IMPLEMENTED**"
            shift
            ;;
        -h | --help )
            HELP=true
            shift
            ;;
        -r | --remote-ip )
            REMOTE_IPV4=${2}
            # TODO: Checking
            echo "WARNING:"
            echo "Accepting $REMOTE_IPV4 as a valid address with no checks"
            shift 2
            ;;
        -v | --verbose )
            VERBOSE=true
            VERBOSE_BASH="-x"
            VERBOSE_UFW="verbose"
            VERBOSITY="--verbosity=debug"
            shift
            ;;
        -- ) shift;
             break
             ;;
        * )
            break
            ;;
    esac
done
if [ "${HELP}" = "true" ]; then
    show_help
    exit 0
fi

check_sudo
check_prereq
interface_detect
fail2ban_ufw
ufw_reset
ufw_program
ufw_enable
