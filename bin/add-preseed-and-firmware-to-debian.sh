#!/bin/bash

# add-preseed-and-firmware-to-debian: Add non-free firmware and preseed to Debian install media
#
# Copyright (C) 2016-2017 Hedi Nasr <h.nsr69@gmail.com>
# Copyright (C) 2022 Peter Lawler <relwalretep@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Changelog:
# 2017.07.05 - Initial creation
# 2022.07.13 - Peter Lawer update
#
# https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-11.4.0-amd64-netinst.iso

if [ "$(id -u)" != "0" ]
then
  echo "This script must be run as root" 1>&2
  exit 1
fi

if [ $# -ne 2 ]
then
    echo "Usage: $0 <input.iso> <ouput.iso> <preseed.file>"
    exit 1
fi

iso=$1
output=$2
if [ "z$3" != "z" ]; then
    preseed=$3
fi
echo "No preseed"

lpdir=__loopdir__
cddir=__cd__
irdir=__irmod__

#bnx2="firmware-bnx2_0.43_all.deb"
bnx2="firmware-bnx2_20210818-1_all.deb"
#bnx2x="firmware-bnx2x_0.43_all.deb"
bnx2x="firmware-bnx2x_20210818-1_all.deb"
sha512_bnx2="1cbfab8ff1ffdb2975c1b52d727be5bf0f7abe2c6f7a3044da1ce1c2e576c1e78035ae596f5d37878d33eeecf843393d04ea96ff5c44e9b0abde6873231c06ac"
sha512_bnx2x="ae31ac0814353b6516b04aa5acac389853653872468c1ad978e66096451c8422f5a28f490f98056fd223d92a5b50a37711c18301b428d1946f429ed3b7a0bff4"

# Fichier md5
echo $sha512_bnx2 $bnx2 > $bnx2.sha512
echo $sha512_bnx2x $bnx2x > $bnx2x.sha512

# On télécharge les deux fichier .deb
wget http://ftp.debian.org/debian/pool/non-free/f/firmware-nonfree/$bnx2
wget http://ftp.debian.org/debian/pool/non-free/f/firmware-nonfree/$bnx2x

if sha512sum -c $bnx2.sha512 && sha512sum -c $bnx2x.sha512
then
    echo "sha512 passed"
else
    echo "sha512 failed!"
    exit 1
fi

# Copie de l'original
mkdir $lpdir
mount -o loop $iso $lpdir
rm -rf $cddir
mkdir $cddir
rsync -a -H --exclude=TRANS.TBL $lpdir/ $cddir
umount $lpdir

# Update initrd
mkdir $irdir
cd $irdir
gzip -d < ../$cddir/install.amd/initrd.gz | \
    cpio --extract --make-directories --no-absolute-filenames
if [ "z$3" != "z" ]; then
    cp ../$preseed preseed.cfg
fi
find . | cpio -H newc --create | \
    gzip -9 > ../$cddir/install.amd/initrd.gz
cd ../
rm -rf $irdir

# On ajoute les firmware
cp $bnx2 $cddir/firmware
cp $bnx2x $cddir/firmware

# Fix checksum
cd $cddir
md5sum `find -follow -type f` > md5sum.txt
cd ..

# Create iso
genisoimage -o $output -r -J -no-emul-boot -boot-load-size 4 -boot-info-table -b isolinux/isolinux.bin -c isolinux/boot.cat ./$cddir

# Cleaning up
rm -rf $lpdir
rm -rf $cddir
rm -f $bnx2 $bnx2x
rm -f $bnx2.sha512 $bnx2x.sha512

echo "Created $output!"
