#!/bin/bash
#Convert an ExpressVPN ovpn to config files usable by OpenWRT openvpn
#Copyright (C) Peter Lawler <relwalretep@gmail.com>
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program.  If not, see <https://www.gnu.org/licenses/>.

### NOTE:
# Will not work with BusyBox awk, sed, cat and grep.
# Please don't try and run this on your OpenWRT machine unless you've
# installed the coreutils or procps-ng equivalents

set -e

test_command () {
    COMMAND=$1
    if [ "$('$COMMAND --version' 2>&1|grep -i busybox)" == "" ]; then
        return
    else
        echo "This script has not been tested with BusyBox version of $COMMAND."
        echo "Please install the full version, or comment out the test."
        echo "Exiting."
        exit 1
        return
    fi
}

test_command "awk"
test_command "cat"
test_command "grep"
test_command "sed"

# TODO: Better handling of $1

if [ ! -f "$1" ]; then
    echo "Need to supply valid OVPNNAME as an arg, \"$1\" don't cut it kid.'"
    exit 1
fi
OVPNNAME="$(echo $1|sed s/\.ovpn//)"
OVPNFILENAME="$OVPNNAME.ovpn"
OWRT_VPN_CONFIG_OUT="$OVPNNAME.config"

# Extract certs and key
awk '/<ca>/{flag=1;next}/<\/ca>/{flag=0}flag' "${OVPNFILENAME}"                > "/etc/openvpn/${OVPNNAME}-ca.crt"
awk '/<cert>/{flag=1;next}/<\/cert>/{flag=0}flag' "${OVPNFILENAME}"            > "/etc/openvpn/${OVPNNAME}-client.crt"
awk '/<key>/{flag=1;next}/<\/key>/{flag=0}flag' "${OVPNFILENAME}"              > "/etc/openvpn/${OVPNNAME}-client.key"
awk '/<tls-auth>/{flag=1;next}/<\/tls-auth>/{flag=0}flag' "${OVPNFILENAME}"    > "/etc/openvpn/${OVPNNAME}-client.tls"

# Create p12
openssl pkcs12 -export \
    -in "/etc/openvpn/${OVPNNAME}-client.crt" \
    -inkey "/etc/openvpn/${OVPNNAME}-client.key" \
    -out "/etc/openvpn/${OVPNNAME}-client.p12" \
    -passout pass:

# Create blank auth file
touch /etc/openvpn/${OVPNNAME}.auth.txt
# TODO: Read in username/password pair

# Fix file permissions
chmod 600 "/etc/openvpn/${OVPNNAME}-ca.crt" "/etc/openvpn/${OVPNNAME}-client.crt"  "/etc/openvpn/${OVPNNAME}-client.key" "/etc/openvpn/${OVPNNAME}-client.tls" "/etc/openvpn/${OVPNNAME}-client.p12" "/etc/openvpn/${OVPNNAME}.auth.txt"

# Get essential settings from existring file
REMOTE_HOST=$(grep ^remote\  ${OVPNFILENAME} |awk '{print $2}')
REMOTE_PORT=$(grep ^remote\  ${OVPNFILENAME} |awk '{print $3}')

uci set openvpn.$OVPNNAME=openvpn
uci set openvpn.$OVPNNAME.enabled='0'
uci set openvpn.$OVPNNAME.dev='tun'
uci set openvpn.$OVPNNAME.persist_key='1'
uci set openvpn.$OVPNNAME.persist_tun='1'
uci set openvpn.$OVPNNAME.nobind='1'
uci set openvpn.$OVPNNAME.remote='$REMOTE_HOST'
uci set openvpn.$OVPNNAME.rport='$REMOTE_PORT'
uci set openvpn.$OVPNNAME.remote_random='1'
uci set openvpn.$OVPNNAME.pull='1'
uci set openvpn.$OVPNNAME.compress='lzo'
uci set openvpn.$OVPNNAME.comp_lzo='adaptive'
uci set openvpn.$OVPNNAME.tls_client='1'
uci set openvpn.$OVPNNAME.single_session='1'
uci set openvpn.$OVPNNAME.tls_exit='1'
uci set openvpn.$OVPNNAME.verify_x509_name='Server name-prefix'
uci set openvpn.$OVPNNAME.remote_cert_tls='server'
uci set openvpn.$OVPNNAME.key_direction='1'
uci set openvpn.$OVPNNAME.route_method='exe'
uci set openvpn.$OVPNNAME.route_delay='2'
uci set openvpn.$OVPNNAME.tun_mtu='1500'
uci set openvpn.$OVPNNAME.fragment='1300'
uci set openvpn.$OVPNNAME.mssfix='1450'
uci set openvpn.$OVPNNAME.verb='3'
uci set openvpn.$OVPNNAME.cipher='AES-256-CBC'
uci set openvpn.$OVPNNAME.auth='SHA512'
uci set openvpn.$OVPNNAME.fast_io='1'
uci set openvpn.$OVPNNAME.sndbuf='524288'
uci set openvpn.$OVPNNAME.rcvbuf='524288'
uci set openvpn.$OVPNNAME.auth_user_pass='/etc/openvpn/$OVPNNAME.auth.txt'
uci set openvpn.$OVPNNAME.ca='/etc/openvpn/$OVPNNAME-ca.crt'
uci set openvpn.$OVPNNAME.key='/etc/openvpn/$OVPNNAME-client.key'
uci set openvpn.$OVPNNAME.cert='/etc/openvpn/$OVPNNAME-client.crt'
uci set openvpn.$OVPNNAME.tls_auth='/etc/openvpn/$OVPNNAME-client.tls'
uci set openvpn.$OVPNNAME.status='/tmp/openvpn-$OVPNNAME-status.log'
uci set openvpn.$OVPNNAME.log='/tmp/openvpn-$OVPNNAME.log'

