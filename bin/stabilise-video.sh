#!/bin/bash
# https://askubuntu.com/questions/19095/video-stabilisation-software

# The first step creates the stabilization profile

ffmpeg -i input.mp4 -vf vidstabdetect=shakiness=10:accuracy=15:result="data-for-stabilisation.trf" -f null -

# The second step creates the stabilized video
ffmpeg -i input.mp4 -vf vidstabtransform=smoothing=30:input="data-for-stabilisation.trf" -c:a copy output-stabilized.mp4


# Other options for the second step are:
ffmpeg -i input.mp4 -vf vidstabtransform=input="data-for-stabilisation.trf",unsharp=5:5:0.8:3:3:0.4 -c:v libx264 -crf 16 -c:a copy -preset fast output-stabilized.mp4
# high quality output
ffmpeg -i input.mp4 -vf vidstabtransform=smoothing=30:input="data-for-stabilisation.trf" -codec:v libx264 -crf 18 -preset veryslow -pix_fmt yuv420p -c:a copy output-stabilized.mp4

# https://www.paulirish.com/2021/video-stabilization-with-ffmpeg-and-vidstab/
# Bonus: create a comparison video
# vertically stacked
ffmpeg -i input.mp4 -i output-stabilized.mp4  -filter_complex vstack output-comparison-stacked.mp4

# side-by-side
ffmpeg -i input.mp4 -i output-stabilized.mp4  -filter_complex hstack output-comparison-sxs.mp4

