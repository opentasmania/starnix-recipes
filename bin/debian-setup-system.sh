1#!/bin/bash

# Menu driven setup of admin and default user
# Copyright (C) Peter Lawler <relwalretep@gmail.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# trap ctrl-c and call ctrl_c()
trap ctrl_c INT

# TODO: Trap locale config a lot earlier
# TODO: use read if dialog isn't available

set -e
set -u
set -o pipefail

init() {
  if [ "${BASH_VERSINFO:-0}" -lt 5 ]; then
    echo "Need bash(1) version 5 minimum"
    exit 1
  fi
  if (command -v tput >/dev/null 2>&1); then
    TEXT_BOLD="$(tput bold)"
    TEXT_UL_ON="$(tput smul)"
    TEXT_UL_OFF="$(tput rmul)"
    # TEXT_REVERSE="$(tput rev)"
    TEXT_NORMAL="$(tput sgr0)"
  else
    TEXT_BOLD=""
    TEXT_UL_ON=""
    TEXT_UL_OFF=""
    # TEXT_REVERSE=""
    TEXT_NORMAL=""
  fi

  if [ ! -f /etc/os-release ]; then
    echo "${TEXT_BOLD}Initial OS detection failure (no /etc/os-release)${TEXT_NORMAL}"
    $BEEP
    exit 1
  fi
  CPU_TYPE=$(dpkg --print-architecture)
  # shellcheck disable=SC1001
  OS="$(hostnamectl status | grep "Operating System" | cut -d \: -f 2 | sed s/^[[:space:]]//g)"
  if [ ! "${OS}" ]; then
    echo "OS NOT DETECTED. EXITING"
    $BEEP
    exit 1
  fi
  echo "Detected OS: ${TEXT_UL_ON}${OS}${TEXT_UL_OFF}"
  APT=$(command -v apt 2>/dev/null)
  /bin/echo "apt(1) found at $APT"
  if [ -z "${APT}" ]; then
    echo "apt(1) not found (System: ${OS})"
    $BEEP
    exit 1
  fi
  if [ ${UID} != "0" ]; then
    echo "Not root user"
    if ! hash sudo 2>/dev/null; then
      echo "sudo(1) not available, attempting to force install"
      echo "su --login --command \"apt update\""
      su --login --command "apt update" ||
        {
          echo >&2 "Cannot issue 'apt update'. Aborting."
          exit 1
        }
      echo "su --login --command \"apt --assume-yes install sudo\""
      su --login --command "apt --assume-yes install sudo" ||
        {
          echo >&2 "Cannot issue 'apt --assume-yes install sudo'. Aborting."
          exit 1
        }
    fi
    HAVE_SUDO=$(command -v sudo 2>/dev/null)
    echo "sudo(1) found at ${HAVE_SUDO}"
    SUDO_USER=$USER
  else
    /bin/echo "Running as root, sudo(1) not needed."
    HAVE_SUDO=""
  fi
  ${HAVE_SUDO} ${APT} update

  if [ "${HAVE_SUDO-default}" = "-default" ]; then
    NEWUSER="debian"
  else
    NEWUSER="${USER}"
  fi
  PASSWORD="temppwd"
  EMAIL="${NEWUSER}"

  read dialog <<<"$(which whiptail dialog 2>/dev/null)"
  [[ "$dialog" ]] || {
    echo -e 'neither whiptail nor dialog found' >&2
    echo "Installing dialog(1)"
    install_packages "dialog"
  }
  MKDIR="$(which mkdir) --parents --verbose"
  RM="$(which rm) --interactive --recursive --verbose"
  # TODO: Used for dialog (or is this redundant in later releases?)
  TMPFILE=$(mktemp --tmpdir="$(dirname "$0")")
}

finish() {
  echo "Removing tempfile"
  ${HAVE_SUDO} ${RM} "${TMPFILE}"
  echo "Removing redundant packages"
  remove_redundant_packages
}

function ctrl_c() {
  echo "** Trapped CTRL-C"
  cleanup
  exit 1
}

function apt_update() {
  ${HAVE_SUDO} ${APT} update
}

function remove_redundant_packages() {
  apt_update
  local unwanted_pkgs=""
  local pkg_check=("$@")
  for pkg in "${pkg_check[@]}"; do
    LC_ALL=C /usr/bin/dpkg --list | awk '{print $2}' | grep "^${pkg}" >/dev/null || unwanted_pkgs="${unwanted_pkgs}${pkg} "
  done

  if [ "${unwanted_pkgs}" ]; then
    echo "Removing: ${unwanted_pkgs}"
    # shellcheck disable=SC2086
    ${HAVE_SUDO} ${APT} --assume-yes --purge remove ${unwanted_pkgs}
    cleanup_packages
    # shellcheck disable=SC2086
    ${HAVE_SUDO} "${APT}" clean
  fi
}

function install_packages() {
  # TODO: prereq dpkg and apt
  local needed_pkgs=""
  local pkg_check=("$@")
  for pkg in "${pkg_check[@]}"; do
    LC_ALL=C /usr/bin/dpkg --list | awk '{print $2}' | grep "^${pkg}" >/dev/null || needed_pkgs="${needed_pkgs}${pkg} "
  done

  if [ "${needed_pkgs}" ]; then
    echo "Installing: ${needed_pkgs}"
    # shellcheck disable=SC2086
    ${HAVE_SUDO} ${APT} --assume-yes install ${needed_pkgs}
    cleanup_packages
    # shellcheck disable=SC2086
    ${HAVE_SUDO} ${APT} clean
  fi
}

function cleanup_packages() {
  local PURGE_LIST
  local DEINSTALL_LIST
  echo "${TEXT_BOLD}autoremove${TEXT_NORMAL}"
  # shellcheck disable=SC2086
  ${HAVE_SUDO} ${APT} --assume-yes --purge autoremove
  PURGE_LIST="$(dpkg --list | grep '^rc' | awk '{ print $2 }')"
  if [[ ${PURGE_LIST} != "" ]]; then
    echo "${TEXT_BOLD}Additional purge${TEXT_NORMAL}"
    # shellcheck disable=SC2086
    ${HAVE_SUDO} ${APT} --assume-yes purge $PURGE_LIST
  fi
  DEINSTALL_LIST="$(dpkg --get-selections | grep deinstall | cut -f 1)"
  if [[ ${DEINSTALL_LIST} != "" ]]; then
    echo "${TEXT_BOLD}Deinstalling${TEXT_NORMAL}"
    # shellcheck disable=SC2086
    ${HAVE_SUDO} ${APT} --assume-yes purge "$DEINSTALL_LIST"
  fi
}

function core_packages() {
  CORE_PACKAGES=(
   'alpine' 'apt-listchanges' 'atop' 'archivemount'
   'bash-completion' 'beep'
   'ca-certificates' 'cowsay' 'cpufrequtils' 'crash' 'curl'
   'devio' 'debconf-utils' 'debfoster' 'debsecan' 'dialog' 'dnsutils'
   'elinks' 'ethtool'
   'fail2ban' 'fdupes' 'fonts-powerline'
   'git' 'gpw'
   'hwinfo'
   'iftop' 'iotop' 'libtinfo5'
   'locales' 'localepurge' 'lshw'
   'mailutils' 'man-db' 'minicom'
   'ncurses-term' 'needrestart' 'netselect-apt'
   'openssh-server'
   'powertop' 'powerline' 'pwgen'
   'rsync'
   'screen' 'shellcheck'
   'tree' 'tzdata'
   'ufw' 'unattended-upgrades'
   'vim' 'vim-addon-manager'
   'wget' 'whiptail')
  install_packages "${CORE_PACKAGES[@]}"
}

function adminsetup() {
  # install admin tools
  local FIRMWARES
  local NETWORK_MANAGER
  local MEMTEST
  local KEXECTOOLS
  local DEFAULT_PACKAGES
  local PACKAGES
  local UBOOT_TOOLS
  local MACHINE
  local DEFAULT_PACKAGES
  MACHINE=$(/bin/uname -m)
  core_packages
  # TODO: work out how to tell kvm from docker/lxc/etc (imvirt(1))
  if [[ "$(grep hyperv </proc/cpuinfo)" == "" ]]; then
    # not inside LXC
    # TODO: need better tests for non amd/intel, arm, etc.
    if [ "${MACHINE}" = "x86_64" ] || [ "${MACHINE}" = "386" ] || [ "${MACHINE}" = "686" ]; then
      KEXECTOOLS=('kdump-tools' 'kexec-tools' 'makedumpfile')
      # MBR="mbr"
      MEMTEST=('memtester' 'memtest86' 'memtest86+')
      # TODO: conman support
      NETWORK_MANAGER=("network-manager")
      UBOOT_TOOLS=""
    else
      # TODO: Better alternates
      UBOOT_TOOLS=('u-boot-tools')
    fi
  else
    echo "Hopefully correctly identified running in a container or VM so at least install something..."
    VMTOOLS=("spice-vdagent")
  fi

  echo "Preparing: apt (install base requirements)"
  PACKAGES+=("${DEFAULT_PACKAGES[@]}" "${FIRMWARES[@]}" "${NETWORK_MANAGER[@]}" "${VMTOOLS[@]}" "${KEXECTOOLS[@]}" "${MEMTEST[@]}" "${UBOOT_TOOLS[@]}")
  install_packages "${PACKAGES[@]}"

  # There is always at least one line output from 'apt list --upgradable'
  if [[ $(${HAVE_SUDO} ${APT} list --upgradable 2>/dev/null | wc -l) -gt 1 ]]; then
    ${HAVE_SUDO} ${APT} --assume-yes upgrade
  fi

  if [ -f /etc/wpa_supplicant/wpa_supplicant.conf ]; then
    ${HAVE_SUDO} grep -qxF "tls_disable_tlsv1_0=1" /etc/wpa_supplicant/wpa_supplicant.conf ||
      echo "tls_disable_tlsv1_0=1" | ${HAVE_SUDO} tee --append /etc/wpa_supplicant/wpa_supplicant.conf
    ${HAVE_SUDO} grep -qxF "tls_disable_tlsv1_1=1" /etc/wpa_supplicant/wpa_supplicant.conf ||
      echo "tls_disable_tlsv1_1=1" | ${HAVE_SUDO} tee --append /etc/wpa_supplicant/wpa_supplicant.conf
    ${HAVE_SUDO} systemctl restart wpa_supplicant
  fi

  if [ "$(systemctl list-unit-files "ModemManager" | wc -l)" -gt 3 ]; then
    echo "Disabling: ModemManager"
    ${HAVE_SUDO} systemctl stop ModemManager
    ${HAVE_SUDO} systemctl disable ModemManager
  fi

  # if [ LAPTOP ]; then
  #    sudo apt-get --assume-yes install gpm ${LAPTOPMODETOOLS} smartmontools
  # fi

  # TODO: something about setting nfsv4 everywhere possible, even if it's not going to be used. to be sure to be sure.
  # TODO: something about setting smb3 everywhere possible, even if it's not going to be used. to be sure to be sure.

  echo "Preparing: unattended upgrades"
  ${HAVE_SUDO} dpkg-reconfigure --frontend=noninteractive --priority=low unattended-upgrades
  ${HAVE_SUDO} bash -c 'echo "Unattended-Upgrade::Mail \"root\";" >> /etc/apt/apt.conf.d/50unattended-upgrades'
  ${HAVE_SUDO} bash -c 'echo "Unattended-Upgrade::SyslogEnable \"true\";" >> /etc/apt/apt.conf.d/50unattended-upgrades'

  echo "Preparing: needrestart"
  if [ ! -d /etc/needrestart/conf.d/ ]; then
    ${HAVE_SUDO} mkdir --parents --verbose /etc/needrestart/conf.d/
    ${HAVE_SUDO} grep -qx --fixed-strings "\$nrconf{restart} = 'a'" /etc/needrestart/conf.d/autorestart.conf ||
      echo "\$nrconf{restart} = 'a'" | ${HAVE_SUDO} tee --append /etc/needrestart/conf.d/autorestart.conf
  fi

  # TODO: journalctl
  # TODO: persistence
  # TODO: etc.

  # set alternatives
  echo "Preparing: alternatives"
  ${HAVE_SUDO} update-alternatives --set editor /usr/bin/vim.basic
}

function usersetup() {
  local NEWUSER="$1"
  local PASSWORD="$2"
  local EMAIL="$3"
  cleanup_packages
  local ADDGROUPS
  local NEWGECOS
  local NEWHOMEDIR
  local NEEDED_DIRS
  # TODO: document /etc/default/useradd
  # TODO: Reintergrated admin/desktop user
  ADDGROUPS="--groups=adm,systemd-journal,staff,sudo"
  # NEWGECOS="$(tr '[:lower:]' '[:upper:]' <<<${NEWUSER:0:1})${NEWUSER:1}"
  NEWGECOS="$(tr '[:lower:]' '[:upper:]' <<<"${NEWUSER:0:1}")${NEWUSER:1}"
  NEWHOMEDIR="/home/${NEWUSER}"
  core_packages
  if [ "$(getent passwd "$NEWUSER")" != "" ]; then
    # TODO: test confirm overwrite
    echo "User \"${NEWUSER}\" exists."
    read -r -p "Clumsily update? [y/N] " response
    response=${response,,} # tolower
    if [[ "$response" =~ ^(yes|y)$ ]]; then
      ${HAVE_SUDO} usermod --comment "${NEWGECOS}" \
        --shell /bin/bash \
        --append $ADDGROUPS "${NEWUSER}"
    fi
  else
    echo "Creating $NEWUSER"
    ${HAVE_SUDO} useradd --comment "${NEWGECOS}" --create-home \
      --shell /bin/bash --user-group \
      ${ADDGROUPS} "${NEWUSER}"
    if [[ -n "${PASSWORD}" ]]; then
      echo "${NEWUSER}:${PASSWORD}" | ${HAVE_SUDO} chpasswd
    else
      echo "Password error"
      exit 1
    fi
  fi
  if [[ -r "${NEWHOMEDIR}" ]]; then
    echo "chmod 750 ${NEWHOMEDIR}"
    ${HAVE_SUDO} chmod 750 "${NEWHOMEDIR}"
  fi
  if ! hash loginctl 2>/dev/null; then
    if loginctl show-user "$NEWUSER" --property=Linger | grep -q 'yes'; then
      echo "loginctl enable-linger ${NEWUSER}"
      ${HAVE_SUDO} loginctl enable-linger "${NEWUSER}"
    else
      echo "loginctl not available"
    fi
  fi

  # TODO: Handle no root entry in alias table
  # TODO: local to remote email address mapping
  echo "Mail aliases"
  if [ -f /etc/aliases ]; then
    # shellcheck disable=SC1001
    ${HAVE_SUDO} sed -i s/^root\:/root\:\ "${NEWUSER}"/g /etc/aliases
    ${HAVE_SUDO} newaliases
  fi

  echo "make ${NEWHOMEDIR}/.ssh"
  ${HAVE_SUDO} mkdir --verbose "${NEWHOMEDIR}/.ssh" &&
    ${HAVE_SUDO} chmod --verbose 700 "${NEWHOMEDIR}/.ssh" &&
    ${HAVE_SUDO} chown --verbose "${NEWUSER}" "${NEWHOMEDIR}/.ssh"

  echo "Checking for git(1)"
  if ! hash git 2>/dev/null; then
    echo "${HAVE_SUDO} ${APT} --assume-yes install git"
    ${HAVE_SUDO} ${APT} --assume-yes install git
  fi
  local HAVE_GIT
  HAVE_GIT=$(command -v git 2>/dev/null)
  echo "git(1) found at ${HAVE_GIT}"

  # TODO: Don't overwrite
  echo "Setting git config user.email"
  if [[ ! -n "$EMAIL" ]]; then
    git config --global --add user.email "$EMAIL"
    git config --global --get user.email
  fi
  echo "Setting git config user.name"
  git config --global --add user.name "$NEWGECOS"
  git config --global --get user.name

  echo "Creating config directories"
  NEEDED_DIRS=(.config/autostart .config/git .config/gtk-3.0 .vim/backup .vim/autostart .vim/tmp bin lib share tmp Projects Programs)
  for dir in "${NEEDED_DIRS[@]}"; do
    if [[ ! -d "${NEWHOMEDIR}/${dir}" ]]; then
      mkdir --parents --verbose "${NEWHOMEDIR}/${dir}"
    fi
  done

  # TODO: test for existence
  if [ ! -d "${NEWHOMEDIR}/Projects/starnix-recipes/.git" ]; then
    ${HAVE_GIT} -C "${NEWHOMEDIR}/Projects" clone https://gitlab.com/opentasmania/starnix-recipes.git
  fi

  echo "Creating autostarts"
  if [[ -f "/usr/share/applications/org.gnome.Terminal.desktop" ]] && [[ ! -f "${NEWHOMEDIR}/.config/autostart/org.gnome.Terminal.desktop" ]]; then
    echo "gnome terminal"
    ln --symbolic --verbose /usr/share/applications/org.gnome.Terminal.desktop "${NEWHOMEDIR}/.config/autostart/."
  fi

  echo "Linking lazy-update.sh"
  if [[ ! -f "${NEWHOMEDIR}/bin/lazy-update.sh" ]]; then
    ln --symbolic --verbose "${NEWHOMEDIR}/Projects/starnix-recipes/bin/lazy-update.sh" "${NEWHOMEDIR}/bin"
  fi

  echo "Linking configs"
  local LINK_CONFIGS=(.bash_aliases .curlrc .config/gtk-3.0/settings.ini .config/git/gitk .screenrc .toprc .vimrc)
  for conf in "${LINK_CONFIGS[@]}"; do
    echo "Testing for conf: ${conf}"
    if [[ ! -f "${NEWHOMEDIR}/${conf}" ]] && [[ ! -L "${NEWHOMEDIR}/${conf}" ]]; then
      ln --symbolic --verbose "${NEWHOMEDIR}/Projects/starnix-recipes/usr/share/dotfiles/${conf}" "${NEWHOMEDIR}/${conf}"
    fi
  done

  echo "Copying configs"
  local COPY_CONFIGS=(.config/git/config .bashrc)
  for conf in "${COPY_CONFIGS[@]}"; do
    cp --preserve --recursive --verbose "${NEWHOMEDIR}/Projects/starnix-recipes/usr/share/dotfiles/${conf}" "${NEWHOMEDIR}/${conf}"
    chmod 640 "${NEWHOMEDIR}/${conf}"
  done

  echo "Checking Python support"
  if hash pip3 2>/dev/null; then
    echo "pip3 install wheel"
    pip3 install wheel
    # A sysadmin with no serial tools is no sysadmin
    echo "pip3 install adafruit-ampy esptool rshell pyserial"
    pip3 install adafruit-ampy esptool rshell pyserial
    echo "pip3 install setuptools virtualenvwrapper"
    pip3 install setuptools virtualenvwrapper
  fi

  echo "Fix file ownership on all files in ${NEWHOMEDIR}"
  chown --recursive --verbose "${NEWUSER}:${NEWUSER}" "${NEWHOMEDIR}"

  ### https://wiki.archlinux.org/index.php/GNOME
  ## Todo:- force dark theme
  ##      green on black terminal

  # ${HAVE_SUDO} apt install dbus-x11 gconf-gsettings-backend gconf-service gconf2-common

  # dbus-launch gsettings set org.gnome.desktop.background color-shading-type "solid"
  # dbus-launch gsettings set org.gnome.desktop.background picture-opacity 100
  # dbus-launch gsettings set org.gnome.desktop.background picture-options 'none'
  # dbus-launch gsettings set org.gnome.desktop.background primary-color "#000000"
  # dbus-launch gsettings set org.gnome.desktop.background secondary-color "#000000"
  # dbus-launch gsettings set org.gnome.desktop.background show-desktop-icons false
  # dbus-launch gsettings set org.gnome.desktop.background picture-uri ''

  # dbus-launch gsettings set org.gnome.desktop.datetime automatic-timezone true

  # dbus-launch gsettings set org.gnome.desktop.default-applications.terminal exec-arg '-e'
  # dbus-launch gsettings set org.gnome.desktop.default-applications.terminal exec 'x-terminal-emulator'

  # dbus-launch gsettings set org.gnome.desktop.interface can-change-accels false
  # dbus-launch gsettings set org.gnome.desktop.interface clock-format '24h'
  # dbus-launch gsettings set org.gnome.desktop.interface clock-show-date true
  # dbus-launch gsettings set org.gnome.desktop.interface clock-show-seconds true
  # dbus-launch gsettings set org.gnome.desktop.interface cursor-blink-time 1200
  # dbus-launch gsettings set org.gnome.desktop.interface cursor-blink-timeout 10
  # dbus-launch gsettings set org.gnome.desktop.interface cursor-blink true
  # dbus-launch gsettings set org.gnome.desktop.interface cursor-size 24
  # dbus-launch gsettings set org.gnome.desktop.interface cursor-theme 'Adwaita'
  # dbus-launch gsettings set org.gnome.desktop.interface document-font-name 'Sans 11'
  # dbus-launch gsettings set org.gnome.desktop.interface enable-animations true
  # dbus-launch gsettings set org.gnome.desktop.interface font-name 'Cantarell 11'
  # dbus-launch gsettings set org.gnome.desktop.interface gtk-color-palette 'black:white:gray50:red:purple:blue:light blue:green:yellow:orange:lavender:brown:goldenrod4:dodger blue:pink:light green:gray10:gray30:gray75:gray90'
  # dbus-launch gsettings set org.gnome.desktop.interface gtk-color-scheme ''
  # dbus-launch gsettings set org.gnome.desktop.interface gtk-im-module 'gtk-im-context-simple'
  # dbus-launch gsettings set org.gnome.desktop.interface gtk-im-preedit-style 'callback'
  # dbus-launch gsettings set org.gnome.desktop.interface gtk-im-status-style 'callback'
  # dbus-launch gsettings set org.gnome.desktop.interface gtk-key-theme 'Default'
  # dbus-launch gsettings set org.gnome.desktop.interface gtk-theme 'Adwaita'
  # dbus-launch gsettings set org.gnome.desktop.interface gtk-timeout-initial 200
  # dbus-launch gsettings set org.gnome.desktop.interface gtk-timeout-repeat 20
  # dbus-launch gsettings set org.gnome.desktop.interface icon-theme 'Adwaita'
  # dbus-launch gsettings set org.gnome.desktop.interface menubar-accel 'F10'
  # dbus-launch gsettings set org.gnome.desktop.interface menubar-detachable false
  # dbus-launch gsettings set org.gnome.desktop.interface menus-have-tearoff false
  # dbus-launch gsettings set org.gnome.desktop.interface monospace-font-name 'Monospace 11'
  # dbus-launch gsettings set org.gnome.desktop.interface scaling-factor "uint32 0"
  # dbus-launch gsettings set org.gnome.desktop.interface show-battery-percentage false
  # dbus-launch gsettings set org.gnome.desktop.interface text-scaling-factor 1.0
  # dbus-launch gsettings set org.gnome.desktop.interface toolbar-detachable false
  # dbus-launch gsettings set org.gnome.desktop.interface toolbar-icons-size 'large'
  # dbus-launch gsettings set org.gnome.desktop.interface toolbar-style 'both-horiz'
  # dbus-launch gsettings set org.gnome.desktop.interface toolkit-accessibility false

  # dbus-launch gsettings set org.gnome.desktop.lockdown disable-application-handlers false
  # dbus-launch gsettings set org.gnome.desktop.lockdown disable-command-line false
  # dbus-launch gsettings set org.gnome.desktop.lockdown disable-lock-screen false
  # dbus-launch gsettings set org.gnome.desktop.lockdown disable-log-out false
  # dbus-launch gsettings set org.gnome.desktop.lockdown disable-printing false
  # dbus-launch gsettings set org.gnome.desktop.lockdown disable-print-setup false
  # dbus-launch gsettings set org.gnome.desktop.lockdown disable-save-to-disk false
  # dbus-launch gsettings set org.gnome.desktop.lockdown disable-user-switching false
  # dbus-launch gsettings set org.gnome.desktop.lockdown user-administration-disabled false

  # dbus-launch gsettings set org.gnome.desktop.media-handling automount-open true
  # dbus-launch gsettings set org.gnome.desktop.media-handling automount true
  # dbus-launch gsettings set org.gnome.desktop.media-handling autorun-never true
  # dbus-launch gsettings set org.gnome.desktop.media-handling autorun-x-content-ignore "@as []"
  # dbus-launch gsettings set org.gnome.desktop.media-handling autorun-x-content-open-folder "@as []"
  # dbus-launch gsettings set org.gnome.desktop.media-handling autorun-x-content-start-app "['x-content/unix-software']"

  # dbus-launch gsettings set org.gnome.desktop.privacy hide-identity false
  # dbus-launch gsettings set org.gnome.desktop.privacy old-files-age "uint32 5"
  # dbus-launch gsettings set org.gnome.desktop.privacy recent-files-max-age 7
  # dbus-launch gsettings set org.gnome.desktop.privacy remember-app-usage true
  # dbus-launch gsettings set org.gnome.desktop.privacy remember-recent-files true
  # dbus-launch gsettings set org.gnome.desktop.privacy remove-old-temp-files true
  # dbus-launch gsettings set org.gnome.desktop.privacy remove-old-trash-files true
  # dbus-launch gsettings set org.gnome.desktop.privacy report-technical-problems false
  # dbus-launch gsettings set org.gnome.desktop.privacy send-software-usage-stats false
  # dbus-launch gsettings set org.gnome.desktop.privacy show-full-name-in-top-bar true

  # dbus-launch gsettings set org.gnome.shell development-tools true
  # dbus-launch gsettings set org.gnome.shell favorite-apps "['org.gnome.Terminal.desktop', 'org.gnome.Nautilus.desktop']"
  # dbus-launch gsettings set org.gnome.shell.keybindings focus-active-notification "['<Super>n']"
  # dbus-launch gsettings set org.gnome.shell.keybindings open-application-menu "['<Super>F10']"
  # dbus-launch gsettings set org.gnome.shell.keybindings pause-resume-tweens "@as []"
  # dbus-launch gsettings set org.gnome.shell.keybindings toggle-application-view "['<Super>a']"
  # dbus-launch gsettings set org.gnome.shell.keybindings toggle-message-tray "['<Super>v', '<Super>m']"
  # dbus-launch gsettings set org.gnome.shell.keybindings toggle-overview "['<Super>s']"

  # some ambient light sensor/a bug causes very verbose logging
  # dbus-launch gsettings set org.gnome.settings-daemon.plugins.power ambient-enabled false

}

function dev() {
  array=("git" "bison"
    "build-essential" "bzr" "bzrtools" "flex"
    "gawk" "git" "git-daemon-run"
    "git-svn" "gnu-standards" "mercurial" "subversion"
    "cppcheck" "libperl-critic-perl" "python-flake8" "pep8"
    "pyflakes" "pylint" "shellcheck" "sparse" "splint" "subversion"
    "python3-examples" "python3-setuptools" "python3-venv"
    "python-setuptools" "python-virtualenv" "python3-pip" "python-pygments"
    "rsync" "util-linux" "unzip" "wget" "zip" "asciidoc" "bash" "bc" "binutils" "bzip2" "fastjar"
    "flex" "git" "gcc" "libgtk2.0-dev" "intltool"
    "zlib1g-dev" "make" "genisoimage" "libncurses5-dev" "libssl-dev" "patch"
    "sdcc" "gettext" "xsltproc" "zlib1g-dev" "perl-tk"
    "libboost-dev" "libxml-parser-perl" "libusb-dev" "bin86" "bcc" "sharutils"
    "devhelp" "source-highlight" "epubcheck" "autoconf-archive" "dvipng" "dvidvi"
    "latexdiff" "latexmk" "fragmaster" "lacheck" "chktex" "purifyeps" "psutils"
    "libspreadsheet-parseexcel-perl" "libntl-dev" "doc-base"
    "libmail-box-perl" "sdcc-ucsim" "prerex" "dot2tex" "xmltex"
    "libssl-dev" "docbook"
    "latex-cjk-all" "dh-make" "dbtoepub" "docbook-xsl-saxon"
    "libasprintf-dev" "libgettextpo-dev" "libboost-atomic-dev"
    "libboost-chrono-dev" "libboost-context-dev"
    "libboost-coroutine-dev" "libboost-date-time-dev"
    "libboost-exception-dev" "libboost-fiber-dev"
    "libboost-filesystem-dev" "libboost-graph-dev"
    "libboost-graph-parallel-dev" "libboost-iostreams-dev"
    "libboost-locale-dev" "libboost-log-dev"
    "libboost-math-dev" "libboost-mpi-dev"
    "libboost-mpi-python-dev" "libboost-program-options-dev"
    "libboost-python-dev" "libboost-random-dev"
    "libboost-regex-dev" "libboost-serialization-dev"
    "libboost-system-dev"
    "libboost-test-dev" "libboost-thread-dev"
    "libboost-timer-dev" "libboost-type-erasure-dev"
    "libboost-wave-dev" "libboost-tools-dev"
    "libmpfrc++-dev")
  # Java
  #array=("ant" "ant-optional" 'ca-certificates-java"
  #  "default-jre-headless" "fop" "libfop-java"
  #  "openjdk-11-jre-headless" "jikespg"
  #  "libsaxon-java" "libxalan2-java" "libxslthl-java" "xalan")
  core_packages
  install_packages "${array[@]}"
}

function boot_grubsetup() {
  local f
  local HIBERNATE
  local INIT_TUNE
  local MACHINE
  local CLOSE_ENCOUNTERS
  #  local CMDLINE_LINUX
  local RESUME_UUID

  # TODO:
  #  https://github.com/docker/docker/issues/4250#issuecomment-35566530
  #GRUB_CMDLINE_LINUX="resume=/dev/sdXY" Where XY is the swap partition location, which can be found by 'sudo swapon'
  #GRUB_CMDLINE_LINUX="resume=`fdisk -l | grep -i swap|awk '{print $1}'" # or try 'sudo swapon'
  MACHINE=$(/bin/uname -m)
  if [ "${MACHINE}" != "x86_64" ] && [ "${MACHINE}" != "386" ] && [ "${MACHINE}" != "686" ]; then
    /bin/echo "Failed to detect x86_64 or x86_32 hardware"
    exit 1
  fi
  #INIT TUNES
  CLOSE_ENCOUNTERS="220 900 2 1000 2 800 2 400 2 600 3"
  #  FUR_ELISE="480 420 1 400 1 420 1 400 1 420 1 315 1 370 1 335 1 282 3 180 1 215 1 282 1 315 3 213 1 262 1 315 1 335 3 213 1 420 1 400 1 420 1 400 1 420 1 315 1 370 1 335 1 282 3 180 1 215 1 282 1 315 3 213 1 330 1 315 1 282 3"
  #  Hz="480 432 1 435 1 440 1"
  #  IMPERIAL_MARCH="480 440 4 440 4 440 4 349 3 523 1 440 4 349 3 523 1 440 8 659 4 659 4 659 4 698 3 523 1 415 4 349 3 523 1 440 8"
  #  MARIO1="1750 523 1 392 1 523 1 659 1 784 1 1047 1 784 1 415 1 523 1 622 1 831 1 622 1 831 1 1046 1 1244 1 1661 1 1244 1 466 1 587 1 698 1 932 1 1195 1 1397 1 1865 1 1397 1"
  #  MARIO2="1000 334 1 334 1 0 1 334 1 0 1 261 1 334 1 0 1 392 2 0 4 196 2"
  #  WOLFENSTEIN_3D="300 131 1 196 1 196 1 196 1 294 1 196 1 294 1 196 1 131 1"

  INIT_TUNE="${CLOSE_ENCOUNTERS}"
  #  CMDLINE_LINUX=$(cat /proc/cmdline)
  grub_packages=("hwinfo")
  install_packages "${grub_packages[@]}"

  largest_swap_size=0
  largest_swap_device=""
  while read -r line
  do
      device=$(echo $line | awk '{print $1}')
      size=$(echo $line | awk '{print $3}')
      if [[ $size -gt $largest_swap_size ]]
      then
          largest_swap_size=$size
          largest_swap_device=$device
      fi
  done < <(${HAVE_SUDO} swapon -s | tail -n -1)
  RESUME_UUID=$(${HAVE_SUDO} /usr/sbin/blkid -s UUID -o value "$largest_swap_device")
  if [ -n "${RESUME_UUID}" ]; then
    local HIBERNATE="resume=${RESUME_UUID}"
    /bin/echo "Hibernation partition set to ${RESUME_UUID}"
  else
    /bin/echo "No hibernation partition set"
  fi

  # TODO: should really ask about apparmor and append 'apparmor=1 security=apparmor' if necessary
  if [ ! -d /usr/share/images/starnix ]; then
    ${HAVE_SUDO} ${MKDIR} --parents /usr/share/images/starnix/
  fi
  # Remove the 'quiet' keyword from the GRUB_CMDLINE_LINUX_DEFAULT in /etc/default/grub
  if [ -f /etc/default/grub ] && grep -q "^GRUB_CMDLINE_LINUX_DEFAULT=.*quiet.*" /etc/default/grub; then
    ${HAVE_SUDO} sed -i 's/\bquiet\b//g' /etc/default/grub
    echo "The 'quiet' keyword has been removed from the GRUB_CMDLINE_LINUX_DEFAULT."
  fi
  if [ ! -d /etc/default/grub.d/ ]; then
    ${HAVE_SUDO} ${MKDIR} /etc/default/grub.d/
  fi
  ${HAVE_SUDO} ${MKDIR} /usr/share/images/starnix/ /etc/default/grub.d/
  # TODO: If there are no local images, get the from the internet
  ${HAVE_SUDO} /bin/cp --interactive --recursive --verbose "$(dirname $(pwd)/"$0")/../usr/share/images/starnix" /usr/share/images/.
  if [ ! -d /etc/default/grub.d/ ]; then
    ${HAVE_SUDO} ${MKDIR} /etc/default/grub.d/
  fi
  GFX_MODE="$(${HAVE_SUDO} hwinfo --monitor | grep '^  Resolution: ' | sed s/'^  Resolution: '//| sed s/'x'/' '/g | sed s/'@'/' '/g | sed s/'Hz'//g | sort -n -k1,1 -k2,2 -k3,3 | head -n 1 | awk '{print $1"x"$2}')"
  if [ -z "$GFX_MODE" ]
  then
    GFX_MODE="640x480"
  fi
  # TODO: Add args to GRUB_CMDLINE_LINUX_DEFAULT only if theyu don't already exist anywhere in any /etc/default/grub* config file
  echo -e "GRUB_CMDLINE_LINUX_DEFAULT=\"\$GRUB_CMDLINE_LINUX_DEFAULT crashkernel=auto systemd.unified_cgroup_hierarchy=1 swapaccount=1 resume=UUID= fsck.mode=auto fsck=preen memhp_default_state=online cgroup_enable=memory apparmor=1 security=apparmor $HIBERNATE\"" | ${HAVE_SUDO} tee /etc/default/grub.d/starnix_cmdline_linux_default.cfg
  # TODO: Work out why /etc/default/grub.d/starnix/*.cfg didn't work, if it was me fix it and if it wasn't yell at someone else.
  echo -e 'GRUB_BACKGROUND="/usr/share/images/starnix/Pirate_fish_flag_with_swords.png"' | ${HAVE_SUDO} tee /etc/default/grub.d/starnix_background.cfg
  echo -e 'GRUB_DEFAULT="saved"' | ${HAVE_SUDO} tee /etc/default/grub.d/starnix_default.cfg
  echo -e 'GRUB_DISABLE_OS_PROBER="false"' | ${HAVE_SUDO} tee /etc/default/grub.d/starnix_disable_os_prober.cfg
  echo -e 'GRUB_DISABLE_RECOVERY="true"' | ${HAVE_SUDO} tee /etc/default/grub.d/starnix_disable_recovery.cfg
  echo -e 'GRUB_DISABLE_SUBMENU="true"' | ${HAVE_SUDO} tee /etc/default/grub.d/starnix_disable_submenu.cfg
  echo -e "GRUB_GFXMODE=\"${GFX_MODE}x8,auto\"" | ${HAVE_SUDO} tee /etc/default/grub.d/starnix_gfx_mode.cfg
  echo -e 'GRUB_GFXPAYLOAD_LINUX="keep"' | ${HAVE_SUDO} tee /etc/default/grub.d/starnix_gfx_payload.cfg
  echo -e "GRUB_INIT_TUNE=\"${INIT_TUNE}\"" | ${HAVE_SUDO} tee /etc/default/grub.d/starnix_init_tune.cfg
  echo -e 'GRUB_SAVEDEFAULT="true"' | ${HAVE_SUDO} tee /etc/default/grub.d/starnix_savedefault.cfg
  echo -e 'GRUB_TERMINAL_OUTPUT="gfxterm console"' | ${HAVE_SUDO} tee /etc/default/grub.d/starnix_terminal_output.cfg
  echo -e 'GRUB_TIMEOUT="10"' | ${HAVE_SUDO} tee /etc/default/grub.d/starnix_timeout.cfg
  echo -e 'LANG="en_AU"' | ${HAVE_SUDO} tee /etc/default/grub.d/starnix_lang.cfg
  echo -e 'LANGUAGE="en_AU"' | ${HAVE_SUDO} tee /etc/default/grub.d/starnix_language.cfg
  # TODO: Fix
  # echo -e "RESUME=UUID=$RESUME_UUID" | ${HAVE_SUDO} tee /etc/initramfs-tools/conf.d/resume
  ${HAVE_SUDO} /usr/sbin/update-initramfs -k all -u -v
  ${HAVE_SUDO} /usr/sbin/grub-mkconfig -o /boot/grub/grub.cfg
}

function watchdog_setup() {
  echo "Preparing: watchdog system"
  # watchdog
  array=('watchdog')
  install_packages "${array[@]}"
  ${HAVE_SUDO} systemctl stop watchdog
  ${HAVE_SUDO} systemctl disable watchdog
  # TODO: Detect existing stanza and decide what to do
  if [[ -f /etc/watchdog.conf ]]; then
    ${HAVE_SUDO} sed -i s/^/\#/g /etc/watchdog.conf
    ${HAVE_SUDO} bash -c "echo -e 'log-dir         = /var/log/watchdog
max-load-1              = 12
max-load-15             = 6
max-load-5              = 9
pidfile         = /var/run/rsyslogd.pid
ping                    = 10.2.0.1
priority                = 1
realtime                = yes
watchdog-device = /dev/watchdog' >>   /etc/watchdog.conf"
  fi
}

function ntpsetup() {
  # TODO: Prompt to set main NTP pool
  # edit /etc/systemd/timesyncd.conf to enable ntp
  if [[ -f /etc/systemd/timesyncd.conf ]]; then
    echo "sed -i s/^/#/ /etc/systemd/timesyncd.conf"
    ${HAVE_SUDO} sed -i s/^/#/ /etc/systemd/timesyncd.conf
    # TODO: test region
    echo "Hardcode /etc/systemd/timesyncd.conf for Oceania"
    # TODO: Only change if it doesn't contain the config
    ${HAVE_SUDO} bash -c 'echo "[Time]
NTP=0.oceania.pool.ntp.org 1.oceania.pool.ntp.org 2.oceania.pool.ntp.org 3.oceania.pool.ntp.org
FallbackNTP=0.debian.pool.ntp.org 1.debian.pool.ntp.org 2.debian.pool.ntp.org 3.debian.pool.ntp.org
RootDistanceMaxSec=5
PollIntervalMinSec=32
PollIntervalMaxSec=2048" >>  /etc/systemd/timesyncd.conf'
    echo "timedatectl set-ntp true"
    ${HAVE_SUDO} timedatectl set-ntp true
  fi
}

function iptablessetup() {
  # TODO: implement REMOTE_IP
  echo "REMOTE_IP not set"
  echo "LAYER2HW1 not set"
  echo "SERVER_IP not set"
  exit 1
  # TODO: Device/ipaddr detection
  # local REMOTE_IP="xx.xx.xx.xx"
  # local LAYER2HW1="y2z1s"
  # local SERVER_IP="$(ip addr show dev ${LAYER2HW1} | grep "inet\ " | sed s/$"[[:space:]]"*// | cut -d \  -f 2)"
  echo "Reset all iptables"
  # NOTE: Must do this in a single call
  ${HAVE_SUDO} bash -c "
iptables -F && iptables -X  && \
iptables -t nat -F && iptables -t nat -X && \
iptables -t mangle -F && iptables -t mangle -X  && \
iptables -P INPUT DROP && iptables -P OUTPUT DROP && iptables -P FORWARD DROP && \
iptables -A INPUT -i lo -j ACCEPT && \
iptables -A OUTPUT -o lo -j ACCEPT
"

  echo "Allowing remote SSH in"

  #  echo "ALLOW SSH ('$REMOTE_IP')"
  #  ${HAVE_SUDO}iptables -A INPUT -p tcp -d "$SERVER_IP "-s "$REMOTE_IP" --sport 513:65535 --dport 22 -m state --state NEW,ESTABLISHED -j ACCEPT
  #  ${HAVE_SUDO}iptables -A OUTPUT -p tcp -s "$SERVER_IP" -d "$REMOTE_IP" --sport 22 --dport 513:65535 -m state --state ESTABLISHED -j ACCEPT

  grep nameserver /etc/resolv.conf | sed s/nameserver\ //g | grep -v ^\# | while IFS= read -r dnsip; do
    echo "ALLOW DNS (to 'dnsip')"
    ${HAVE_SUDO} iptables -A OUTPUT -p udp -d "${dnsip}" --dport dns -m state --state NEW,ESTABLISHED -j ACCEPT
    ${HAVE_SUDO} iptables -A INPUT -p udp -s "${dnsip}" --sport dns -m state --state ESTABLISHED -j ACCEPT
    ${HAVE_SUDO} iptables -A OUTPUT -p tcp -d "${dnsip}" --dport dns -m state --state NEW,ESTABLISHED -j ACCEPT
    ${HAVE_SUDO} iptables -A INPUT -p tcp -s "${dnsip}" --sport dns -m state --state ESTABLISHED -j ACCEPT
  done

  echo "ALLOW OUT HTTP"
  ${HAVE_SUDO} iptables -A OUTPUT -p tcp --dport http -m state --state NEW,ESTABLISHED -j ACCEPT

  echo "ALLOW OUT HTTPS"
  ${HAVE_SUDO} iptables -A OUTPUT -p tcp --dport https -m state --state NEW,ESTABLISHED -j ACCEPT

  echo "ALLOW OUT ICMP (pings,...)"
  ${HAVE_SUDO} iptables -A OUTPUT -p icmp -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
  echo "ALLOW IN ICMP (pings,...)"
  ${HAVE_SUDO} iptables -A INPUT -p icmp -m state --state ESTABLISHED,RELATED -j ACCEPT

  echo "ALLOW OUT NTPSYNC"
  ${HAVE_SUDO} iptables -A OUTPUT -p udp --dport ntp -m state --state NEW,ESTABLISHED -j ACCEPT
  echo "ALLOW IN NTPSYNC"
  ${HAVE_SUDO} iptables -A INPUT -p udp --sport ntp -m state --state ESTABLISHED -j ACCEPT

  echo "ALLOW IN LOCALHOST"
  ${HAVE_SUDO} iptables -A INPUT -i lo -j ACCEPT
  echo "ALLOW OUT LOCALHOST"
  ${HAVE_SUDO} iptables -A OUTPUT -o lo -j ACCEPT

  # Log what was incoming but denied (optional but useful).
  echo "INPUT LIMIT"
  ${HAVE_SUDO} iptables -A INPUT -m limit --limit 5/min -j LOG --log-prefix "iptables_INPUT_denied: " --log-level 7
  echo "INPUT DROP"
  ${HAVE_SUDO} iptables -A INPUT -j DROP

  # Log any traffic that was sent to you for forwarding (optional but useful).
  echo "FORWARD LIMIT"
  ${HAVE_SUDO} iptables -A FORWARD -m limit --limit 5/min -j LOG --log-prefix "iptables_FORWARD_denied: " --log-level 7
  echo "FORWARD DROP"
  ${HAVE_SUDO} iptables -A FORWARD -j DROP

  ${HAVE_SUDO} iptables -A OUTPUT -j DROP

  ${HAVE_SUDO} iptables commit
  ${HAVE_SUDO} iptables save
}

function nftables() {
  array=("nftables")
  install_packages "${array[@]}"
  ${HAVE_SUDO} systemctl enable nftables.service
}

function pentest() {
  # TODO: Cleanup
  echo "stuff"
  array=("aesfix" "aeskeyfind" "afflib-dbg"
    "afflib-tools" "autopsy" "bilibop" "bilibop-common" "bilibop-lockfs"
    "bruteforce-luks" "bruteforce-salted-openssl"
    "cewl" "chaosreader" "dc3dd" "dcfldd" "disktype" "dislocker" "ed2k-hash"
    "ewf-tools" "exifprobe" "ext3grep" "ext4magic" "fcrackzip" "forensics-all"
    "forensics-colorize" "forensics-extra" "forensics-extra-gui" "galleta"
    "gifshuffle" "gpart" "grokevt" "hashdeep" "hashrat" "hexcompare" "hexedit"
    "lime-forensics-dkms" "mac-robber" "magicrescue" "masscan" "metacam"
    "missidentify" "myrescue" "nasty" "ncrack" "ndiff" "nmap" "nmapsi4" "p0f" "pads"
    "pasco" "pdfcrack" "pipebench" "pnscan" "pompem" "python3-dfdatetime"
    "python3-dfwinreg" "python3-scapy" "python-dfdatetime" "python-dfvfs"
    "python-dfwinreg" "python-libewf" "recoverdm" "recoverjpeg" "reglookup"
    "reglookup-doc" "rifiuti" "rifiuti2" "rsakeyfind" "safecopy" "scalpel"
    "scrounge-ntfs" "shed" "sleuthkit" "ssdeep" "sslsplit" "tableau-parm"
    "tcpxtract" "testdisk" "testdisk-dbg" "undbx" "unhide" "vinetto"
    "volatility" "volatility-tools" "winregfs" "wipe" "xprobe" "zenmap"
    "aircrack-ng" "arj" "audacity" "audacity-data"
    "bfbtester" "binwalk" "bruteforce-luks" "catdoc" "chntpw" "clamav" "clamav-base"
    "clamav-freshclam" "cmospwd" "crack-md5" "crunch" "cryptmount" "cupp" "dcfldd" "disktype"
    "dissy" "ethstatus" "exiftags" "exiv2" "fatcat" "fdupes" "firebird3.0-common"
    "firebird3.0-common-doc" "flasm" "fonts-lyx" "foremost" "forensics-extra"
    "forensics-extra-gui" "geoip-bin" "gifshuffle" "gwenview" "hashcat" "hashcat-data"
    "hashid" "hexcompare" "hexedit" "hydra" "ieee-data" "kactivities-bin"
    "kactivitymanagerd" "kamera" "kio-extras" "kio-extras-data" "lcrack"
    "maskprocessor" "mc" "mc-data" "medusa" "memstat"
    "mirage" "mpack" "mtd-utils" "nasm" "ncompress" "ncrack" "neopi" "ophcrack"
    "ophcrack-cli" "outguess" "patator" "pcapfix" "pdfcrack" "pecomato" "pev"
    "polenum" "pyrit" "python-attr" "python-click" "python-colorama"
    "python-constantly" "python-dns" "python-egenix-mxdatetime"
    "python-egenix-mxtools" "python-impacket" "python-incremental"
    "python-ipy" "python-matplotlib-data" "python-mysqldb" "python-paramiko"
    "python-pcapy" "python-psycopg2" "python-pyasn1-modules" "python-pyexiv2"
    "python-pyexiv2-doc" "python-pysnmp4" "python-pysnmp4-apps"
    "python-pysnmp4-mibs" "python-rsvg" "python-scapy" "python-service-identity"
    "python-sqlalchemy" "python-sqlalchemy-ext" "python-twisted" "python-twisted-bin"
    "python-twisted-core" "python3-binwalk" "python3-cycler" "python3-dateutil"
    "python3-decorator" "python3-matplotlib" "python3-numpy" "python3-opengl"
    "python3-pyparsing" "python3-pyqt4" "python3-pyqtgraph" "python3-scipy"
    "python3-tk" "python3-tz" "qt5-image-formats-plugins" "rarcrack" "reaver"
    "rfkill" "samdump2" "shotwell" "shotwell-common" "sipcrack" "smb-nat" "smitools"
    "snowdrop" "squashfs-tools" "statsprocessor" "steghide" "stegosuite" "stegsnow"
    "sucrack" "sxiv" "tcpflow" "tcpick" "tcpreplay" "tcpxtract" "testdisk" "tshark"
    "uni2ascii" "unrar-free" "wamerican-huge" "wamerican-insane" "wamerican-large"
    "wamerican-small" "wbrazilian" "wbritish" "wbritish-huge" "wbritish-insane"
    "wbritish-large" "wbritish-small" "wbulgarian" "wcanadian" "wcanadian-huge"
    "wcanadian-insane" "wcanadian-large" "wcanadian-small" "wcatalan" "weplab"
    "wfrench" "wgaelic" "wgerman-medical" "wifite" "wirish" "witalian" "wmanx"
    "wpolish" "wportuguese" "wspanish" "wswedish" "wukrainian")
  install_packages "${array[@]}"
}

function forensics() {
  # TODO: Split out Disaster Recovery/Forensics
  echo "Forensics (metapackage?)"
  array=("forensics-all" "forensics-colorizea" "forensics-extra" "forensics-extra-gui")
  install_packages "${array[@]}"
}

function aptconfig() {
  # TODO: split apt config and proxy configs
  if [[ ! -f /etc/apt/apt.conf.d/00proxy-pdiffs-disable ]]; then
    echo -e 'Acquire::PDiffs "false";' | ${HAVE_SUDO} tee /etc/apt/apt.conf.d/00proxy-pdiffs-disable
  else
    echo "Existing apt pdiffs config found"
    ls -la /etc/apt/apt.conf.d/00proxy-pdiffs-disable
  fi
  if [[ ! -f /etc/apt/apt.conf.d/99apt-cacher-ng ]]; then
    echo -e 'Acquire::http::proxy "http://127.0.0.1:3142/";' | ${HAVE_SUDO} tee /etc/apt/apt.conf.d/99apt-cacher-ng >/dev/null
  else
    echo "Existing apt proxy config found"
    ls -la /etc/apt/apt.conf.d/99apt-cacher-ng
  fi
  if [[ -f /etc/apt-cacher-ng/acng.conf ]]; then
    echo "PassThroughPattern: .*443" | ${HAVE_SUDO} tee -a /etc/apt-cacher-ng/acng.conf
  else
    echo "No apt-cacher-ng server config found"
  fi
}

function beagleboard() {
  git -C /opt/scripts pull
  ${HAVE_SUDO} sed -i s/\ quiet//g /boot/uEnv.txt
  ${HAVE_SUDO} /opt/scripts/tools/update_kernel.sh
  echo "Installing firmware image"
  #${HAVE_SUDO} ${APT} install "linux-firmware-image-$(uname -r)"
  ${HAVE_SUDO} ${APT} --purge remove bb-node-red-installer bone101 bonescript c9-core-installer doc-beaglebone-getting-started
  ${HAVE_SUDO} ${APT} --purge autoremove
  # TODO: Only pin kernel and firmwares to rcnee
  # TODO: Detect running on microSD card eg /dev/mmcblk0p1
  # TODO: Test whether already grown
  # ${HAVE_SUDO} /opt/scripts/tools/grow_partition.sh
  # TODO: Test against current bootloader
  # ${HAVE_SUDO} /opt/scripts/tools/developers/update_bootloader.sh
  if [[ -f /etc/default/bb-wl18xx ]]; then
    # Disable SoftAP0
    ${HAVE_SUDO} sed -i s/TETHER_ENABLED=yes/TETHER_ENABLED=no/ /etc/default/bb-wl18xx
  fi
  # shellcheck disable=2016
  ${HAVE_SUDO} bash -c 'echo -e "Beaglebone\n$(cat /etc/issue)" > /etc/issue'
  # shellcheck disable=2016
  ${HAVE_SUDO} bash -c 'echo -e "Beaglebone\n$(cat /etc/issue.net)" > /etc/issue.net'
  ${HAVE_SUDO} systemctl stop bonescript bonescript.socket
  ${HAVE_SUDO} systemctl disable bonescript bonescript.socket
}

function rpi() {
  ${HAVE_SUDO} bash -c "${APT} --assume-yes install --upgrade raspi-config rpi-update"
  # TODO: Update to latest raspios
  # disable remote pi login
  #if [ ${NEWUSER} = "pi" ]; then
  #    ${HAVE_SUDO} bash -c 'echo "Deny User pi" >> /etc/ssh/sshd_config'
  #fi
  ## enable camera??
  ## Set gfx memory allocation
  ${HAVE_SUDO} rpi-update
  ${HAVE_SUDO} raspi-config
}

function single_board_computer() {
  if [[ -f "/etc/rpi-issue" ]] && [[ -f $(which raspi-config) ]] && [[ -x $(which raspi-config) ]] && [[ -x $(which rpi-update) ]]; then
    echo "rpi foundation detected"
    rpi
  elif [[ -f "/etc/dogtag" ]] && [[ -f "/boot/uEnv.txt" ]]; then
    echo "BeagleBoard detected"
    beagleboard
  else
    echo "SBC not detected"
  fi
  watchdog_setup
}

function kiosk_system() {
  array=("chromium lightdm openbox unclutter xserver-xorg x11-xserver-utils xinit xorg")
  install_packages "${array[@]}"
  # TODO: Don't append if already there
  sed -i 's/$/ fbcon=rotate:2/' /boot/firmware/cmdline.txt
  if [[ -f "/etc/rpi-issue" ]] && [[ -f $(which raspi-config) ]] && [[ -x $(which raspi-config) ]] && [[ -x $(which rpi-update) ]]; then
    echo "Raspberry Pi detected"
    ${HAVE_SUDO} raspi-config
  fi
}

function dockersetup() {
  RELEASE=$(hostnamectl | grep "Operating System" | awk '{print $6}' | sed s/\(// | sed s/\)//)
  if [ -z "${RELEASE}" ]; then
    echo "Error: Cannot parse hostnamectl(1) output for debian release name"
    exit 1
  fi
  array=("software-properties-common")
  install_packages "${array[@]}"
  ${HAVE_SUDO} add-apt-repository --yes "deb [arch=${CPU_TYPE}] https://download.docker.com/linux/debian ${RELEASE} stable"
  curl -fsSL https://download.docker.com/linux/debian/gpg | ${HAVE_SUDO} apt-key add -
  ${HAVE_SUDO} ${APT} update
  array=("docker-ce" "docker-ce-cli" "containerd.io")
  install_packages "${array[@]}"
  if [[ "${SUDO_USER}-default" != "-default" ]]; then
    echo "Adding ${SUDO_USER} to docker group"
    ${HAVE_SUDO} usermod --append --groups docker "${SUDO_USER}"
  fi
}

function ufw() {
  array=("ufw")
  install_packages "${array[@]}"
  # TODO: Much better network and IP detection
  # TODO: IPv6
  MY_IP4_NETWORK=$(ip route | tail -1 | cut -f 1 -d \ )
  MY_IP4_ADDRESS=$(ip route | tail -1 | cut -f 9 -d \ )
  MY_DEVICE=$(ip route | grep default | cut -f 5 -d \ )
  echo "DHCP"
  ${HAVE_SUDO} ufw allow in on "${MY_DEVICE}" from any port 68 to any port 67 proto udp comment "DHCP"
  echo "Multicast"
  ${HAVE_SUDO} ufw allow from "${MY_IP4_NETWORK}" to 224.0.0.0/4 proto udp comment "Multicast"
  echo "mDNS"
  ${HAVE_SUDO} ufw allow from "${MY_IP4_NETWORK}" to "${MY_IP4_ADDRESS}" port 5353 comment "mDNS"
  echo "DNS"
  ${HAVE_SUDO} ufw allow from "${MY_IP4_NETWORK}" to "${MY_IP4_ADDRESS}" port 53 comment "DNS"
  echo "LLMNR"
  ${HAVE_SUDO} ufw allow from "${MY_IP4_NETWORK}" to "${MY_IP4_ADDRESS}" port 5355 comment "Link Local Multicast Name Resolution"
  echo "sss"
  ${HAVE_SUDO} ufw allow from "${MY_IP4_NETWORK}" to "${MY_IP4_ADDRESS}" port 22 proto tcp comment "ssh"
  echo "www"
  ${HAVE_SUDO} ufw allow from "${MY_IP4_NETWORK}" to "${MY_IP4_ADDRESS}" port 80 proto tcp comment "www"
  ${HAVE_SUDO} ufw default deny
  ${HAVE_SUDO} ufw enable
}

function fwsetup() {
  # TODO: Move to using nftables
  # iptablessetup
  # nftables
  ufw
}

function lxc {
  array=("lxc" "libvirt0" "libpam-cgfs" "bridge-utils" "uidmap")
  install_packages "${array[@]}"
}

function ansible {
  array=("ansible")
  install_packages "${array[@]}"
}

function tzdataconfig {
  echo "Preparing: dpkg-reconfigure tzdata"
  ${HAVE_SUDO} dpkg-reconfigure tzdata
}

function localessetupconfig {
  echo "Preparing: dpkg-reconfigure locales"
  ${HAVE_SUDO} dpkg-reconfigure locales
}

function checkopts() {
  # TODO: checkopts
  echo
}

function menu() {
  local choice
  local mainmenuncmd
  local mainmenuoptions
  # TODO: detect --shadow
  mainmenuncmd=($dialog --separate-output --title "Setup McSetupface" --checklist "Select options:" 22 76 16)
  mainmenuoptions=(a "locales" on
    b "tzdata" on
    c "fw" off
    d "ntp" off
    e "iptables" off
    f "apt" off
    g "grub" off
    h "admin" off
    i "user" on
    j "dev" off
    k "pentest" off
    l "forensics" off
    m "sbc" off
    n "kiosk" off
    o "docker" off
    p "lxc" off
    q "ansible" off
  )
  mainmenuchoices=$("${mainmenuncmd[@]}" "${mainmenuoptions[@]}" 2>&1 >/dev/tty)
  clear
  for choice in $mainmenuchoices; do
    case $choice in
    a)
      echo "localessetupconfig"
      localessetupconfig
      ;;
    b)
      echo "tzdataconfig"
      tzdataconfig
      ;;
    c)
      echo "fwsetup"
      fwsetup
      ;;
    d)
      echo "ntpsetup"
      ntpsetup
      ;;
    e)
      echo "iptablessetup"
      iptablessetup
      ;;
    f)
      echo "aptconfig"
      aptconfig
      ;;
    g)
      echo "boot_grubsetup"
      boot_grubsetup
      ;;
    h)
      echo "admin"
      adminsetup
      ;;
    i)
      echo "user"
      # todo: more choices, eg we set access to sudo but perhaps just a couple of selected services
      usercmd=(dialog --shadow --form "User creation" 15 30 0)
      useroptions=(
        "username:" 1 1 "$NEWUSER" 1 11 10 0
        "password:" 2 1 "$PASSWORD" 2 11 10 0
        "email:   " 3 1 "$EMAIL" 3 11 10 0
      )
      userchoices=($("${usercmd[@]}" "${useroptions[@]}" 2>&1 >/dev/tty))
      NEWUSER="${userchoices[0]}"
      if [[ ! -v "${userchoices[1]}" ]]; then
        PASSWORD="${userchoices[1]}"
      fi
      if [[ ! -v "${userchoices[2]}" ]]; then
        EMAIL="${userchoices[2]:-}"
      fi
      usersetup "$NEWUSER" "$PASSWORD" "$EMAIL"
      ;;
    j)
      echo "dev"
      dev
      ;;
    k)
      echo "pentest"
      pentest
      ;;
    l)
      echo "4nsex"
      forensics
      ;;
    m)
      echo "sbc"
      single_board_computer
      ;;
    n)
      echo "kiosk"
      kiosk_system
      ;;
    o)
      echo "docker"
      dockersetup
      ;;
    p)
      echo "lxc"
      lxc
      ;;
    q)
      echo "ansible"
      ansible
      ;;
    esac
  done
}

init
#ARGS=$(getopt -o --long 'firewall:,menu::,ntp::,apt-config::,grub::,admin::,user::,developer::,pentest::,sbc::,verbose' -- "$@") || exit
menu
finish
