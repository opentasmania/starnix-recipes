#!/bin/bash
# Get the support pages for installed Gnome3 extensions
# Copyright (C) Peter Lawler <relwalretep@gmail.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

#We want the new-line to remain
old_ifs=${IFS}
IFS=$'
'

#Get the names of the extensions
EXT=$(grep "name\":" ~/.local/share/gnome-shell/extensions/*/metadata.json /usr/share/gnome-shell/extensions/*/metadata.json | awk -F '"name": "|",' '{print $2}')

#Get hostname
HOSTNAME=$(hostnamectl --static)

echo "[Looking up extensions...]"
echo

#Find urls to the extensions
for n in ${EXT};
do
	echo "Processing ${n}"
	EXTFIND=$(grep -l "${n}" ~/.local/share/gnome-shell/extensions/*/metadata.json /usr/share/gnome-shell/extensions/*/metadata.json)

	#Curl don't like blank spaces so use %20
	i=$(echo "${n}" | sed 's/ /%20/g')

	URL=$(curl -A 'Mozilla/5.0 (X11; Linux i686; rv:17.0) Gecko/20100101 Firefox/17.0' --silent "https://www.google.com/search?q=site:extensions.gnome.org%20${i}" | grep -io "<a href=\"https://extensions.gnome.org/extension/.*(this,'','','','1'," | awk -F '<a href="|"*' '{print $2}')

	#If urls not on extensions.gnome.org use the one in metadata.json
	if [ "${URL}" = "" ]; then
		URL=$(grep "url\":" ${EXTFIND} | awk -F '"url": "|",*' '{print $2}')
	fi

	#Print out the nice list
	echo "$n - (${URL})" >> GnomeShellExtensionList-${HOSTNAME}.txt

done

zenity --text-info --width=850 --height=800 --title="My extensions" --filename=GnomeShellExtensionList-${HOSTNAME}.txt
#rm GnomeShellExtensionList-${HOSTNAME}.txt

echo
echo "[Done!]"

IFS=${old_ifs}
