#!/bin/bash

# (C) Peter Lawler relwalretep@plnom.com
# LGPL2 licence goes here

# FYI: Here be dragons. Nasty hacks are nasty
set -e

init() {
  if ! (command -v certbot >/dev/null 2>&1); then
    echo "No certbot(1) available, exiting"
    exit 255
  fi
  # Note: first entry in array is used by letsencrypt for directory naming purposes on
  #       multiple host certificates
  DOMAIN_ARRAY=(a.test b.invalid c.example)
  KEYSIZE=4096
  DATESTRING="$(date +%Y_%m_%d_%H_%M_%S_%Z)"
  CURRENTPID=$$
  BACKUPDIR=/var/backups/certbot-${DATESTRING}-${CURRENTPID}
  DRYRUN_SWITCH=""
  VERBOSE_SWITCH=""
  check_sudo
}

check_sudo() {
  if [ ${UID} != "0" ]; then
    echo "Not root user"
    if ! hash sudo 2>/dev/null; then
      echo "su --login --command \"apt --assume-yes install sudo\""
      su --login --command "apt --assume-yes install sudo"
    fi
    HAVE_SUDO=$(command -v sudo 2>/dev/null)
    echo "sudo(1) found at ${HAVE_SUDO}"
  else
    /bin/echo "Running as root, sudo(1) not needed."
    HAVE_SUDO=""
  fi
}

check_all() {
  DIRSTOCHECK=$($HAVE_SUDO ls -l /etc/letsencrypt/live/ | grep ^d | awk '{print $9}')
  for dir in $DIRSTOCHECK; do
    echo "Checking $dir"
    $HAVE_SUDO ssl-cert-check -c /etc/letsencrypt/live/$dir/cert.pem
  done
}

dkim() {
  #NOTE: No dry run capacity
  $HAVE_SUDO mkdir --parents $VERBOSE_SWITCH $BACKUPDIR/dkim
  for domain in "${DOMAIN_ARRAY[@]}"; do
    echo "Working dkim on $domain"
    if [ -f /var/lib/dkim/$domain.pem ]; then
      $HAVE_SUDO mv $VERBOSE_SWITCH /var/lib/dkim/$domain.pem $BACKUPDIR/dkim/$domain.pem
      $HAVE_SUDO rm $VERBOSE_SWITCH /var/lib/dkim/$domain.pem
    fi
    $HAVE_SUDO /usr/sbin/amavisd-new genrsa /var/lib/dkim/$domain.pem $KEYSIZE &&
      $HAVE_SUDO chmod $VERBOSE_SWITCH 0400 /var/lib/dkim/$domain.pem &&
      $HAVE_SUDO chown $VERBOSE_SWITCH amavis:amavis /var/lib/dkim/$domain.pem
  done
  $HAVE_SUDO systemctl restart amavis
  $HAVE_SUDO /usr/sbin/amavisd-new showkeys
}

email() {
  local domain_string
  for domain in "${DOMAIN_ARRAY[@]}"; do
    domain_string="$domain_string -d mx.$domain "
  done
  echo $domain_string
  # note: preferred cert filename is first -d option
  $HAVE_SUDO certbot certonly \
    --agree-tos \
    $DRYRUN_SWITCH \
    $VERBOSE_SWITCH \
    --manual \
    --preferred-challenges=dns \
    --hsts \
    --email "certificates@${domain_string}" \
    --no-eff-email \
    --rsa-key-size=$KEYSIZE \
    $domain_string
}

web_sub() {
  local domain_func=$1
  local domains_to_register="${domain_func},*.${domain_func}"
  #    DRY_RUN="--dry-run"
  EMAIL="certificates@${domain_func}"
  KEYSIZE=4096
  VERBOSE_SWITCH="--verbose-switch"
  if [ "$VERBOSE_SWITCH" != "" ]; then
    echo "domain_func: $domain_func"
    echo "domains_to_register: $domains_to_register"
    echo "dry_run: $DRY_RUN"
    echo "email: $EMAIL"
    echo "keysize: $KEYSIZE"
  fi

  #   Can't use webroot plugin with wildcard domains,
  #   must use DNS
  #   --webroot-path "/var/www/${domain_func}/" \
  #   --authenticator webroot
  #   --preferred-challenges=http
  $HAVE_SUDO certbot certonly \
    $DRYRUN_SWITCH \
    --manual \
    --email $EMAIL \
    --rsa-key-size $KEYSIZE \
    --agree-tos \
    --must-staple \
    --hsts \
    --uir \
    --no-eff-email \
    --redirect \
    --preferred-challenges=dns \
    --debug-challenges \
    --force-renewal \
    -d "${domains_to_register}"
}

web() {
  echo "Running web"
  local domain
  for domain in "${DOMAIN_ARRAY[@]}"; do
    if ! web_sub "$domain"; then
      echo "Error processing certs for $domain"
      exit 255
    fi
    echo "Restarting services"
    $HAVE_SUDO systemctl restart postfix
    $HAVE_SUDO systemctl restart amavisd
    $HAVE_SUDO systemctl restart dovecot
    $HAVE_SUDO systemctl restart nginx
  done
}

show_help() {
  echo "---------------"
  echo "   --dkim"
  echo "-d --dry-run"
  echo "-e --email"
  echo "-r --renew"
  echo "-w --web"
  echo "-v  --verbose"
}

# Ensure verbose, and dry run options are processed first
if ! OPTS=$(getopt --options cvdehrw \
  --longoptions check,verbose,dry-run,dkim,email,help,renew,web \
  -n 'parse-options' -- "$@"); then
  /bin/echo "Failed parsing options." >&2
  help
  exit 1
fi

eval set -- "$OPTS"
while true; do
  case "$1" in
  -d | --dry-run)
    DRYRUN_SWITCH="--dry-run"
    shift
    ;;
  -v | --verbose)
    VERBOSE_SWITCH="--verbose"
    echo "VERBOSE_SWITCH: $VERBOSE_SWITCH"
    shift
    ;;
  --dkim)
    init
    dkim
    shift
    ;;
  -c | --check)
    init
    check_all
    shift
    ;;
  -e | --email)
    init
    email
    shift
    ;;
  -h | --help)
    HELP=true
    shift
    ;;
  -r | --renew)
    init
    renew
    shift
    ;;
  -w | --web)
    init
    web
    shift
    ;;
  --)
    shift
    break
    ;;
  *)
    break
    ;;
  esac
done
if [ "${HELP}" = "true" ]; then
  show_help
  exit 0
fi
