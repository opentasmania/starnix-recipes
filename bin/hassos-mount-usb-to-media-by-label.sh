#!/bin/bash

echo "For now, read the source"
exit 1

# https://community.home-assistant.io/t/solved-mount-usb-drive-in-hassio-to-be-used-on-the-media-folder-with-udev-customization/258406/68

# Probably eventuially write a whoile bunch of rules
# Sooner this changes to Python, thew probability of Better

# some UI thing to detect the drive
DRIVE_PATH=""
#mkdir $DRIVE_PATH/udev
# some UI thing to choose the rule to write

# some logic to decide which rule to run

cat << USB_UDEV_RULE | sudo tee "$DRIVE_PATH"/80-mount-usb-to-media-by-label.rules
#
# udev rule
#   Mount USB drive to the media directory using the partition name as mount point
#
# Description:
#   Created for Home Assistant OS, this rule mounts any USB drives
#   into the Hassio media directory (/mnt/data/supervisor/media).
#   When a USB drive is connected to the board, the rule creates one directory
#   per partition under the media directory. The newly created partition is named
#   as the partition name. If the partition does not have a name, then the following
#   name format is used: "usb-{block-name}" where the block name is sd[a-z][0-9].
#
# Note 1:
#   The rule name is always prefixed with a number. In this case, the rule uses 80.
#   This represents the order of the rule when multiple rules exists in udev.
#   Low numbers run first, high numbers run last. However, low numbers do not have all
#   the facilities than high numbers may have.
#   For this rule to run properly, use numbers equal or greater than 80.
#
# Note 2:
#   This rule will skip mounting the 'CONFIG' USB key.
#   https://github.com/home-assistant/operating-system/blob/a6445af71282045c2ea92f1dea8d2f9e518bd008/Documentation/configuration.md
#
# Note 3:
#   This rule will mount the OS partitions if the OS is sorted on a USB drive (i.e. USB booting).
#   To prevent this issue from happening, update the rule to skip the booting USB drive.
#   See the CAUTION message below.
#
# Source of inspiration:
#   https://www.axllent.org/docs/auto-mounting-usb-storage/
#
# Useful links:
#   https://wiki.archlinux.org/index.php/Udev
#
# udev commands:
#   - Restart udev to reload new rules:
#       udevadm control --reload-rules
#   - List device attributes of sdb1:
#       udevadm info --attribute-walk --name=/dev/sdb1
#   - List environment variables of sdb1:
#       udevadm info /dev/sdb1
#   - Trigger add/remove event for sdb1:
#       udevadm trigger --verbose --action=add --sysname-match=sdb1
#       udevadm trigger --verbose --action=remove --sysname-match=sdb1
#


# Filter on block devices, exit otherwise
# CAUTION: Change to 'sd[a-z][0-9]' if booting from a USB drive (e.g.: sda)
KERNEL!="sd[b-z][0-9]", GOTO="abort_rule"

# Skip none USB devices (e.g.: internal SATA drive)
ENV{ID_PATH}!="*-usb-*", GOTO="abort_rule"

# Import the partition info into the environment variables
IMPORT{program}="/usr/sbin/blkid -o udev -p %N"

# Exit if partition is not a filesystem
ENV{ID_FS_USAGE}!="filesystem", GOTO="abort_rule"

# Exit if this is the 'CONFIG' USB key
ENV{ID_FS_LABEL}=="CONFIG", GOTO="abort_rule"

# Get the partition name if present, otherwise create one
ENV{ID_FS_LABEL}!="", ENV{dir_name}="%E{ID_FS_LABEL}"
ENV{ID_FS_LABEL}=="", ENV{dir_name}="usb-%k"

# Determine the mount point
ENV{mount_point}="/mnt/data/supervisor/media/%E{dir_name}"

# If filesystem is ntfs, use the ntfs3 driver, so we get rw support
# Home Assistant Operating System 8.0 or later required (linux kernel 5.15)
ENV{ID_FS_TYPE}=="ntfs", ENV{fstype}="-t ntfs3"
ENV{ID_FS_TYPE}!="ntfs", ENV{fstype}="-t auto"

# Mount the device on 'add' action (a.k.a. plug the USB drive)
ACTION=="add", RUN{program}+="/usr/bin/mkdir -p %E{mount_point}", RUN{program}+="/usr/bin/systemd-mount %E{fstype} --no-block --automount=no --collect $devnode %E{mount_point}"

# Umount the device on 'remove' action (a.k.a unplug or eject the USB drive)
ACTION=="remove", ENV{dir_name}!="", RUN{program}+="/usr/bin/systemd-umount %E{mount_point}", RUN{program}+="/usr/bin/rmdir %E{mount_point}"

# Exit
LABEL="abort_rule"
USB_UDEV_RULE

cat << SSD_UDEV_RULE | sudo tee "$DRIVE_PATH"/80-mount-usb-to-media-by-label.rules
#
# udev rule
#   Mount USB drive to the media directory using the partition name as mount point
#
# Description:
#   Created for Home Assistant OS, this rule mounts any USB drives
#   into the Hassio media directory (/mnt/data/supervisor/media).
#   When a USB drive is connected to the board, the rule creates one directory
#   per partition under the media directory. The newly created partition is named
#   as the partition name. If the partition does not have a name, then the following
#   name format is used: "usb-{block-name}" where the block name is sd[a-z][0-9].
#
# Note 1:
#   The rule name is always prefixed with a number. In this case, the rule uses 80.
#   This represents the order of the rule when multiple rules exists in udev.
#   Low numbers run first, high numbers run last. However, low numbers do not have all
#   the facilities than high numbers may have.
#   For this rule to run properly, use numbers equal or greater than 80.
#
# Note 2:
#   This rule will skip mounting the 'CONFIG' USB key.
#   https://github.com/home-assistant/operating-system/blob/a6445af71282045c2ea92f1dea8d2f9e518bd008/Documentation/configuration.md
#
# Note 3:
#   This rule will mount the OS partitions if the OS is sorted on a USB drive (i.e. USB booting).
#   To prevent this issue from happening, update the rule to skip the booting USB drive.
#   See the CAUTION message below.
#
# Source of inspiration:
#   https://www.axllent.org/docs/auto-mounting-usb-storage/
#
# Useful links:
#   https://wiki.archlinux.org/index.php/Udev
#
# udev commands:
#   - Restart udev to reload new rules:
#       udevadm control --reload-rules
#   - List device attributes of sdb1:
#       udevadm info --attribute-walk --name=/dev/sdb1
#   - List environment variables of sdb1:
#       udevadm info /dev/sdb1
#   - Trigger add/remove event for sdb1:
#       udevadm trigger --verbose --action=add --sysname-match=sdb1
#       udevadm trigger --verbose --action=remove --sysname-match=sdb1
#


# Filter on block devices, exit otherwise
# CAUTION: Change to 'sd[b-z][0-9]' if booting from a USB drive (e.g.: sda)
KERNEL!="sd[b-z][0-9]", GOTO="abort_rule"

# Skip none USB devices (e.g.: internal SATA drive)
ENV{ID_PATH}!="*-usb-*", GOTO="abort_rule"

# Import the partition info into the environment variables
IMPORT{program}="/usr/sbin/blkid -o udev -p %N"

# Exit if partition is not a filesystem
ENV{ID_FS_USAGE}!="filesystem", GOTO="abort_rule"

# Exit if this is the 'CONFIG' USB key
ENV{ID_FS_LABEL}=="CONFIG", GOTO="abort_rule"

# Get the partition name if present, otherwise create one
ENV{ID_FS_LABEL}!="", ENV{dir_name}="%E{ID_FS_LABEL}"
ENV{ID_FS_LABEL}=="", ENV{dir_name}="usb-%k"

# Determine the mount point
ENV{mount_point}="/mnt/data/supervisor/media/%E{dir_name}"

# If filesystem is ntfs, use the ntfs3 driver, so we get rw support
# Home Assistant Operating System 8.0 or later required (linux kernel 5.15)
ENV{ID_FS_TYPE}=="ntfs", ENV{fstype}="-t ntfs3"
ENV{ID_FS_TYPE}!="ntfs", ENV{fstype}="-t auto"

# Mount the device on 'add' action (a.k.a. plug the USB drive)
ACTION=="add", RUN{program}+="/usr/bin/mkdir -p %E{mount_point}", RUN{program}+="/usr/bin/systemd-mount %E{fstype} --no-block --automount=no --collect $devnode %E{mount_point}"

# Umount the device on 'remove' action (a.k.a unplug or eject the USB drive)
ACTION=="remove", ENV{dir_name}!="", RUN{program}+="/usr/bin/systemd-umount %E{mount_point}", RUN{program}+="/usr/bin/rmdir %E{mount_point}"

# Exit
LABEL="abort_rule"
SSD_UDEV_RULE