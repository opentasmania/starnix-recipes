#!/bin/bash
# eagle2kicad-helper.sh - a script to help convert Eagle to KiCAD design files
# Copyright (C) 2017 Peter Lawler
# Snail mail: PO Box 195
#             Lindisfarne, Tasmania
#             AUSTRALIA 7015
# email:      relwalretep@gmail.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

set -e

function exit_with_error
{
	echo "CODE: $1" 1>&2
	exit 1
}


if [ ! "$1" ]; then
    echo "Syntax: $0 URL - git repo to clone"
    exit 1
fi

ADDRESS=$1
ADDRESS_PROTOCOL="$(echo $ADDRESS | grep :// | sed -e's,^\(.*://\).*,\1,g')"
ADDRESS_URL="$(echo ${1/$ADDRESS_PROTOCOL/})"
ADDRESS_USER="$(echo $ADDRESS_URL | grep @ | cut -d@ -f1)"
ADDRESS_HOST="$(echo ${ADDRESS_URL/$user@/} | cut -d/ -f1)"
ADDRESS_PORT="$(echo $ADDRESS_HOST | sed -e 's,^.*:,:,g' -e 's,.*:\([0-9]*\).*,\1,g' -e 's,[^0-9],,g')"
ADDRESS_PATH="$(echo $ADDRESS_URL | grep / | cut -d/ -f2-)"


PROJECT_PREFIX="plaw"
DESCRIPTION="A one line description"
REMOTE_HOST="git"     # server on which we have ssh access to create git repo
REMOTE_DIR="/srv/git" # directory on remote server where git repos are stored
PWD=$(pwd)

EAGLE2KICAD_REPO_URL="https://github.com/lachlanA/eagle-to-kicad"
EAGLE2KICAD_REPO_NAME=$(echo ${EAGLE2KICAD_REPO_URL} |  awk -F'/' '{ print $(NF-1)"/"$NF}')
EAGLE2KICAD_DIR=$(echo ${EAGLE2KICAD_REPO_URL} |  awk -F'/' '{ print $NF}')
EAGLE2KICAD_REMOTE="${EAGLE2KICAD_DIR}_remote"

PROJECT2CONVERT_REPO_URL="${ADDRESS}"
PROJECT2CONVERT_REPO_NAME=$(echo ${PROJECT2CONVERT_REPO_URL} |  awk -F'/' '{ print $(NF-1)"/"$NF}')
PROJECT2CONVERT_DIR=$(echo ${PROJECT2CONVERT_REPO_URL} |  awk -F'/' '{ print $NF}')
PROJECT2CONVERT_REMOTE="${PROJECT2CONVERT_DIR}_remote"

MYPROJECT=${PROJECT_PREFIX}-${PROJECT2CONVERT_DIR}
PROJECT_DIR="${PWD}/${MYPROJECT}"

echo "Project:         ${MYPROJECT}"
echo "Description:     ${DESCRIPTION}"
echo "Remote location: ${REMOTE_HOST}:${REMOTE_DIR}"
echo "Local location : ${PROJECT_DIR}"
echo
echo "EAGLE2KICAD_REPO_URL:      ${EAGLE2KICAD_REPO_URL}"
echo "EAGLE2KICAD_REPO_NAME:     ${EAGLE2KICAD_REPO_NAME}"
echo "EAGLE2KICAD_DIR:           ${EAGLE2KICAD_DIR}"
echo "EAGLE2KICAD_REMOTE:        ${EAGLE2KICAD_REMOTE}"
echo
echo "PROJECT2CONVERT_REPO_URL:  ${PROJECT2CONVERT_REPO_URL}"
echo "PROJECT2CONVERT_REPO_NAME: ${PROJECT2CONVERT_REPO_NAME}"
echo "PROJECT2CONVERT_DIR:       ${PROJECT2CONVERT_DIR}"
echo "PROJECT2CONVERT_REMOTE:    ${PROJECT2CONVERT_REMOTE}"
echo

echo "Initialising ${REMOTE_HOST}:${REMOTE_DIR}/${MYPROJECT}.git"
# shellcheck disable=SC2029
ssh ${REMOTE_HOST} "git init --bare ${REMOTE_DIR}/${MYPROJECT}.git"
echo "Setting ${REMOTE_HOST}:${REMOTE_DIR}/${MYPROJECT}.git/description"
# shellcheck disable=SC2029
ssh ${REMOTE_HOST} "echo \"${DESCRIPTION}\" > ${REMOTE_DIR}/${MYPROJECT}.git/description"
echo

echo "git clone ${REMOTE_HOST}:${REMOTE_DIR}/${MYPROJECT}.git ${PROJECT_DIR}"
git clone "${REMOTE_HOST}:${REMOTE_DIR}/${MYPROJECT}.git" "${PROJECT_DIR}" || ( echo "Failed to 'git clone ${REMOTE_HOST}:${REMOTE_DIR}/${MYPROJECT}.git ${PROJECT_DIR}'"; exit_with_error "$ERRCODE" )
if [ $? -ne 0 ]; then
    echo "ERROR"
    exit 1
fi
echo

# Pull in eagle-to-kicad
echo "git -C ${PROJECT_DIR} remote add ${EAGLE2KICAD_REMOTE} ${EAGLE2KICAD_REPO_URL}"
git -C "${PROJECT_DIR}" remote add "${EAGLE2KICAD_REMOTE}" "${EAGLE2KICAD_REPO_URL}" || ( echo "Failed to 'git -C ${PROJECT_DIR} remote add ${EAGLE2KICAD_REMOTE} ${EAGLE2KICAD_REPO_URL}'"; exit_with_error "$ERRCODE" )
echo
echo "git -C ${PROJECT_DIR} fetch --verbose ${EAGLE2KICAD_REMOTE}"
git -C "${PROJECT_DIR}" fetch --verbose "${EAGLE2KICAD_REMOTE}" || ( echo "Failed to 'git -C ${PROJECT_DIR} fetch --verbose ${EAGLE2KICAD_REMOTE}'"; exit_with_error "$ERRCODE" )
echo
echo "git -C ${PROJECT_DIR} merge --verbose --allow-unrelated-histories -s ours --no-commit ${EAGLE2KICAD_REMOTE}/master"
git -C "${PROJECT_DIR}" merge --verbose --allow-unrelated-histories -s ours --no-commit "${EAGLE2KICAD_REMOTE}/master"  || ( echo "Failed to 'git -C ${PROJECT_DIR} merge --verbose --allow-unrelated-histories -s ours --no-commit ${EAGLE2KICAD_REMOTE}/master'"; exit_with_error "$ERRCODE" )
echo
echo "git -C ${PROJECT_DIR} read-tree -v --prefix=${EAGLE2KICAD_DIR}/ -u ${EAGLE2KICAD_REMOTE}/master"
git -C "${PROJECT_DIR}" read-tree -v --prefix="${EAGLE2KICAD_DIR}/" -u "${EAGLE2KICAD_REMOTE}/master" || ( echo "Failed to 'git -C ${PROJECT_DIR} read-tree -v --prefix=${EAGLE2KICAD_DIR}/ -u ${EAGLE2KICAD_REMOTE}/master'"; exit_with_error "$ERRCODE" )
echo
echo "git -C ${PROJECT_DIR} commit -m \"Imported ${EAGLE2KICAD_REPO_NAME} as subtree.\""
git -C "${PROJECT_DIR}" commit -m "Imported ${EAGLE2KICAD_REPO_NAME} as subtree."  || ( echo "Failed to 'git -C ${PROJECT_DIR} commit -m \"Imported ${EAGLE2KICAD_REPO_NAME} as subtree.\"'"; exit_with_error "$ERRCODE" )
echo
echo "git -C ${PROJECT_DIR} push"
git -C "${PROJECT_DIR}" push  || ( echo "Failed to 'git -C ${PROJECT_DIR} push'"; exit_with_error "$ERRCODE" )
echo
echo "git -C ${PROJECT_DIR} pull --verbose --strategy subtree ${EAGLE2KICAD_REMOTE} master"
git -C "${PROJECT_DIR}" pull --verbose --strategy subtree "${EAGLE2KICAD_REMOTE}" master || ( echo "Failed to 'git -C ${PROJECT_DIR} pull --verbose --strategy subtree ${EAGLE2KICAD_REMOTE} master'"; exit_with_error "$ERRCODE" )
echo

# Clean up files left behind from subtree conversion
echo "cd ${PROJECT_DIR}"
cd "${PROJECT_DIR}" || ( echo "Failed to 'cd ${PROJECT_DIR}'"; exit_with_error "$ERRCODE" )
echo

echo "git rm --force -r examples"
git rm --force -r examples # why git rm doesn't honour -C I have no idea...

echo "git rm ./*.ulp ./*.inc LICENSE.txt README.md"
git rm ./*.ulp ./*.inc LICENSE.txt README.md
echo
echo "git -C ${PROJECT_DIR} cm \"Clean up after subtree imports\""
git -C "${PROJECT_DIR}" cm "Clean up after subtree imports"
echo
echo "cd ${PWD}"
cd "${PWD}" || ( echo "Failed to 'cd ${PWD}'"; exit_with_error "$ERRCODE" )
echo

# Pull in project to convert
echo "git -C ${PROJECT_DIR} remote add ${PROJECT2CONVERT_REMOTE} ${PROJECT2CONVERT_REPO_URL}"
git -C "${PROJECT_DIR}" remote add "${PROJECT2CONVERT_REMOTE}" "${PROJECT2CONVERT_REPO_URL}" || ( echo "Failed to 'git -C ${PROJECT_DIR} remote add ${PROJECT2CONVERT_REMOTE} ${PROJECT2CONVERT_REPO_URL}'"; exit_with_error "$ERRCODE" )
echo
echo "git -C ${PROJECT_DIR} fetch --verbose ${PROJECT2CONVERT_REMOTE}"
git -C "${PROJECT_DIR}" fetch --verbose "${PROJECT2CONVERT_REMOTE}" || ( echo "Failed to 'git -C ${PROJECT_DIR} fetch --verbose ${PROJECT2CONVERT_REMOTE}'"; exit_with_error "$ERRCODE" )
echo
echo "git -C ${PROJECT_DIR} merge --verbose --allow-unrelated-histories -s ours --no-commit ${PROJECT2CONVERT_REMOTE}/master"
git -C "${PROJECT_DIR}" merge --verbose --allow-unrelated-histories -s ours --no-commit "${PROJECT2CONVERT_REMOTE}/master" || ( echo "Failed to 'git -C ${PROJECT_DIR} merge --verbose --allow-unrelated-histories -s ours --no-commit ${PROJECT2CONVERT_REMOTE}/master'"; exit_with_error "$ERRCODE" )
echo
echo "git -C ${PROJECT_DIR} read-tree -v --prefix=${PROJECT2CONVERT_DIR}/ -u ${PROJECT2CONVERT_REMOTE}/master"
git -C "${PROJECT_DIR}" read-tree -v --prefix="${PROJECT2CONVERT_DIR}/" -u "${PROJECT2CONVERT_REMOTE}/master" || ( echo "Failed to 'git -C ${PROJECT_DIR} read-tree -v --prefix=${PROJECT2CONVERT_DIR}/ -u ${PROJECT2CONVERT_REMOTE}/master'"; exit_with_error "$ERRCODE" )
echo
echo "git -C ${PROJECT_DIR} commit -m \"Imported ${PROJECT2CONVERT_REPO_NAME} as a subtree\""
git -C "${PROJECT_DIR}" commit -m "Imported ${PROJECT2CONVERT_REPO_NAME} as a subtree" || ( echo "Failed to 'git -C ${PROJECT_DIR} commit -m \"Imported ${PROJECT2CONVERT_REPO_NAME} as a subtree\"'"; exit_with_error "$ERRCODE" )
echo
echo "git -C ${PROJECT_DIR} push"
git -C "${PROJECT_DIR}" push || ( echo "Failed to 'git -C ${PROJECT_DIR} push'"; exit_with_error "$ERRCODE" )
echo
echo "git -C ${PROJECT_DIR} pull --verbose --strategy subtree ${PROJECT2CONVERT_REMOTE} master"
git -C "${PROJECT_DIR}" pull --verbose --strategy subtree "${PROJECT2CONVERT_REMOTE}" master || ( echo "Failed to 'git -C ${PROJECT_DIR} pull --verbose --strategy subtree ${PROJECT2CONVERT_REMOTE} master'"; exit_with_error "$ERRCODE" )

# Set .eaglerc defaults for script location
echo "sed --in-place /^ULP:Eagle.To.KiCad.Libs.SCRIPT_ULP/d ~/.eaglerc"
sed --in-place /^ULP:Eagle.To.KiCad.Libs.SCRIPT_ULP/d ~/.eaglerc
echo "sed --in-place /^ULP:Eagle.To.KiCad.Libs.TARGET_DIR/d ~/.eaglerc"
sed --in-place /^ULP:Eagle.To.KiCad.Libs.TARGET_DIR/d ~/.eaglerc
echo
echo "echo \"ULP:Eagle.To.KiCad.Libs.SCRIPT_ULP = \"$PROJECT_DIR/eagle-to-kicad/\"
ULP:Eagle.To.KiCad.Libs.TARGET_DIR = \"$PROJECT_DIR/${PROJECT2CONVERT_DIR}-kicad/\" >> ~/.eaglerc"
echo "ULP:Eagle.To.KiCad.Libs.SCRIPT_ULP = \"$PROJECT_DIR/eagle-to-kicad\"
ULP:Eagle.To.KiCad.Libs.TARGET_DIR = \"$PROJECT_DIR/${PROJECT2CONVERT_DIR}-kicad/\"" >> ~/.eaglerc
echo

# Prepare the output directory
mkdir --parents --verbose "${PROJECT_DIR}"/"${PROJECT2CONVERT_DIR}"-kicad
echo "KiCAD files for ${PROJECT2CONVERT_DIR}" > "${PROJECT_DIR}"/"${PROJECT2CONVERT_DIR}"-kicad/README.txt
git -C "${PROJECT_DIR}" add "${PROJECT2CONVERT_DIR}"-kicad/README.txt
git -C "${PROJECT_DIR}" commit --message "Add README.txt to target project"

echo "*.bak
*.kicad_pcb-bak" > "${PROJECT_DIR}"/.gitignore
git -C "${PROJECT_DIR}" add .gitignore
git -C "${PROJECT_DIR}" commit --message "Add .gitignore" 

git -C "${PROJECT_DIR}" push

echo "DONE"
