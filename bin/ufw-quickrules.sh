#!/bin/bash
# simple script to set a ufw(1) rule, aka easy to read the format and extend
# Copyright (C) Peter Lawler
# Snail mail: PO Box 195
#             Lindisfarne, Tasmania
#             AUSTRALIA 7015
# email:      relwalretep@gmail.com
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
##########################################################################
ADDRESS=$1
if [ -z "$ADDRESS" ]; then
    echo "need to specify an address"
    exit 1
fi

echo "${ADDRESS}"

sudo ufw allow from "${ADDRESS}" to 0.0.0.0/0 port 22 proto tcp comment "SSH"