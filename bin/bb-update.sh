#!/bin/bash
# bb-update.sh - A program to update a Beaglebone Black
# Copyright (C) Peter Lawler <relwalretep@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

BBDOTORG_OVERLAYS_GIT_LOCATION=https://github.com/BeagleBoard/bb.org-overlays.git
DOWNLOAD_LOCATION=/var/tmp
KERNEL_RELEASE="$(uname -r)"
KERNEL_UPDATE_REPO="http://rcn-ee.com/repos/latest/"
RELEASE="stretch"

# Need to do something with setting this from the cmd line, and/or from current kernel version etc.
VERSION="LTS$(uname -r|cut -d \. -f 1,2|sed s/\\.//)"
LTS_SWITCH=$(grep ${VERSION}: /opt/scripts/tools/update_kernel.sh | cut -f 3 -d \  | tr -d \" )

AVAILABLE="$(curl --silent ${KERNEL_UPDATE_REPO}/${RELEASE}-armhf/LATEST-ti | grep ${VERSION}| awk '{print $3}')"

if [ "${AVAILABLE}" = "${KERNEL_RELEASE}" ]; then
    echo "No update required: ${KERNEL_RELEASE}"
    exit
else
    echo "Updating ${KERNEL_RELEASE} to ${AVAILABLE}"
fi

check_dpkg () {
    LC_ALL=C dpkg --list | awk '{print $2}' | grep "^${pkg}" >/dev/null || deb_pkgs="${deb_pkgs}${pkg} "
}

# priv check
echo "Checking for sudo"
if [ "$(id | grep root )" ]; then
        echo "Probably shouldn't be run as root… but proceeding anyway"
elif [ "$(sudo -v)" ]; then
    echo "sudo privileges needed, exiting"
    exit
fi
echo "-----------------------------------"

# update the machine to ensure caches are full before checking for packages
echo "apt update" && sudo apt update 
echo "-----------------------------------"

# package check
echo "Checking for support tools"
unset deb_pkgs
pkg="bash-completion"
check_dpkg
pkg="bison"
check_dpkg
pkg="build-essential"
check_dpkg
pkg="curl"
check_dpkg
pkg="flex"
check_dpkg
pkg="git"
check_dpkg
pkg="git"
check_dpkg
pkg="man"
check_dpkg

if [ "${deb_pkgs}" ] ; then
    echo "Installing: ${deb_pkgs}"
    sudo apt --assume-yes install ${deb_pkgs}
    sudo apt clean
fi

echo "-----------------------------------"

# remove old kernels, ugly but effective
# TODO: if something got removed here, we're possibly on a 'new' kernel and thus need to install the overlays below
echo "Checking for old kernels"
CURRENT_KERNEL=$(uname -r)
KERNEL_PACKAGES_TO_REMOVE=$(dpkg --list | grep 'linux.*image' | grep -Ev "base|${CURRENT_KERNEL}" | grep ^ii |  awk '{print $2}')
if [ -n "${KERNEL_PACKAGES_TO_REMOVE}" ]; then
    sudo apt --assume-yes purge $KERNEL_PACKAGES_TO_REMOVE
else
    echo "No old kernels to remove"
    check_for_overlays="TRUE"
fi
echo "-----------------------------------"

# upgrade the machine
# need to do better checks and exit rather than just the &&
# NOTE: We don't --assume-yes here for safety's sake.
# echo "apt update" && sudo apt update && \
#    echo "apt upgrade" && sudo apt upgrade && \
#    echo "apt dist-upgrade" && sudo apt dist-upgrade && \
#    echo "apt --purge autoremove" && sudo apt --purge autoremove && \
#    echo "apt autoclean" && sudo apt autoclean && \
#    echo "-----------------------------------"

# update official support scripts
echo "Checking for official support scripts"
if [ -f /opt/scripts/.git/config ]; then
    echo "git -C /opt/scripts pull"
    git -C /opt/scripts pull
fi
echo "-----------------------------------"

# ensure there's a nice working directory for downloads
echo "Checking for download directory"
if  [ ! -d ${DOWNLOAD_LOCATION}/ ]; then
    echo "Making download directory"
    sudo mkdir -p ${DOWNLOAD_LOCATION}/
    sudo chown debian:debian ${DOWNLOAD_LOCATION}/
fi
echo "-----------------------------------"

# if the overlays haven't already been grabbed, get them
echo "Checking for overlays"
if [ ! -d ${DOWNLOAD_LOCATION}/bb.org-overlays/.git ]; then
    echo "Cloning overlays"
    git -C ${DOWNLOAD_LOCATION} clone ${BBDOTORG_OVERLAYS_GIT_LOCATION}
else
    echo "Updating overlays"
    git -C ${DOWNLOAD_LOCATION}/bb.org-overlays pull
fi
echo "-----------------------------------"

# check for overlays
if [ ${check_for_overlays} ]; then
    echo "Checking for overlays installed"
    # do stuff
fi
echo "-----------------------------------"

# update to the latest kernel, if successful also install the firmware image
echo "Updating kernel to ${AVAILABLE}"
sudo /opt/scripts/tools/update_kernel.sh --ti-kernel "${LTS_SWITCH}" && \
sudo apt install linux-firmware-image-"${AVAILABLE}" rtl8723bu-modules-"${AVAILABLE}"
