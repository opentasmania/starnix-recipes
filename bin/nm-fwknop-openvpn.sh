#!/bin/bash

# nm-fwknop-openvpn.sh
#   program to call fwknop, then networkmanager and optionally add routes
# Copyright (C) 2015-18 Peter Lawler
# Snail mail: PO Box 195
#             Lindisfarne, Tasmania
#             AUSTRALIA 7015
# email:      relwalretep@gmail.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

VPN_CONNECTION=""
FWKNOP_CONFIG=""
INTERNAL_ROUTERS=()
PRIVATE_IPS=()
MANAGER_IPS=()
REMOTE_ROUTES=(${INTERNAL_ROUTERS[@]} ${PRIVATE_IPS[@]} ${MANAGER_IPS[@]})

if ! sudo -v; then
    echo "sudo privileges needed, exiting"
    exit
fi

if [ -f $VPN_CONNECTION ]; then
    echo "No \$VPN_CONNECTION defined, exiting."
    exit 1
fi

if [ -f $FWKNOP_CONFIG ]; then
    echo "No \$FWKNOP_CONFIG defined, exiting."
    exit 1
fi

if [ "$(nmcli --terse --fields GENERAL.STATE  c show $VPN_CONNECTION | cut -d \: -f 2)" = "activated" ]; then
    echo "Closing connection $VPN_CONNECTION"
    /usr/bin/nmcli --pretty connection down $VPN_CONNECTION
fi

echo "Knocking $FWKNOP_CONFIG"
/usr/bin/fwknop --named-config="$FWKNOP_CONFIG" -R -s --verbose

echo "Establishing connection to $VPN_CONNECTION"
/usr/bin/nmcli --pretty connection up "$VPN_CONNECTION"

if [ $? = 0 ]; then
    echo "REMOTE_ROUTES=${REMOTE_ROUTES[*]}"
    REMOTE_ROUTE=$(/usr/bin/sudo /sbin/ip route show dev tun0 | grep kernel | awk '{print $1}')
    for route in "${REMOTE_ROUTES[@]}"; do
#        echo "/usr/bin/sudo /sbin/ip route add $route via $REMOTE_ROUTE dev tun0"
#        /usr/bin/sudo /sbin/ip route add "$route" via "$REMOTE_ROUTE" dev tun0
        echo "/usr/bin/sudo /sbin/ip route add ${route} dev tun0"
        /usr/bin/sudo /sbin/ip route add "${route}" dev tun0
    done
fi

