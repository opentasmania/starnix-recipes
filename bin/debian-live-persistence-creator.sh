#!/bin/bash -e
# A quick script to create a debian live usb stick with persistence
# Copyright (C) Peter Lawler
# Snail mail: PO Box 195
#             Lindisfarne, Tasmania
#             AUSTRALIA 7015
# email:      relwalretep@gmail.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

# MAKE YOUR OWN DEBIAN LIVEUSB STICK
# https://unix.stackexchange.com/questions/382817/uefi-bios-bootable-live-debian-stretch-amd64-with-persistence

# TODO: Other OS's n stuff
# TODO: Add extra packages

# TODO: Better args handling
SHORT_VERBOSE="-v"
LONG_VERBOSE="--verbose"
# TODO: Better image selector
ISO_FILENAME="debian-live-12.0.0-amd64-standard.iso"
#MYTMPDIR=$(mktemp --directory --tmpdir="/tmp/")
MYTMPDIR=/tmp

myMountPoints=("${MYTMPDIR}/usb-efi" "${MYTMPDIR}/usb-live" "${MYTMPDIR}/usb-persistence" "${MYTMPDIR}/live-iso")

usage() {
  echo "$0 [DEVICE]"
  echo "[DEVICE] is hot-pluggable device under /dev, do not include /dev in path"
}

if [ "$1" = "" ]; then
  echo "Needs hot-pluggable device for usage"
  usage
  exit
fi

echo "Checking for ${ISO_FILENAME}"
if [ ! -f "${ISO_FILENAME}" ]; then
  echo "${ISO_FILENAME} not in working directory ($(pwd))"
  exit 1
fi

# TODO: Better device selector
DEVICE=$1

# Make sure the requested hotplug device is available
PLUGGABLE_DEVICES=$(lsblk -o name,hotplug,type -P | grep "HOTPLUG=\"1\"" | grep "TYPE=\"disk\"")
echo "Hotplug devices: ${PLUGGABLE_DEVICES}"

echo "Requested ${DEVICE}"
if [[ ${PLUGGABLE_DEVICES} == *"${DEVICE}"* ]]; then
  echo "Device is hot-pluggable"
else
  echo "Requested device ${DEVICE} is not a hot-pluggable device (did you accidentially include /dev? that's the path not really the device itself - use sdX not /dev/sdX)"
  exit 1
fi
DEVICE="/dev/${DEVICE}"

# Privilege check
echo "Checking for sudo"
if id | grep -q root; then
  echo "Probably shouldn't be run as root... but proceeding anyway"
elif [ "$(sudo -v)" ]; then
  echo "sudo privileges needed, exiting"
  exit 1
fi

# Existing package check
echo "Checking for existing required packages"
NEEDED_TOOLS="dosfstools e2fsprogs grub2-common util-linux parted syslinux"
# TODO: Better package check
# shellcheck disable=2086
sudo apt install --assume-yes ${NEEDED_TOOLS}
retval=$?
if [ $retval -ne 0 ]; then
  echo "Error installing needed packages"
  exit $retval
fi

# 'set +e' so we don't error out if no partitions found
set +e
echo "Unmounting $DEVICE"
sudo umount "${LONG_VERBOSE}" ${DEVICE}*
set -e

# Zap the master boot record of the requesting device
echo "Zeroing MBR of ${DEVICE}"
sudo dd status=progress if=/dev/zero of="${DEVICE}" bs=512 count=1 conv=notrunc

# Create required partition tables
# TODO: Ensure sizes scale to device capacity
echo "Making partition tables"
sudo parted "${DEVICE}" --script mktable gpt
sudo parted "${DEVICE}" --script mkpart EFI fat16 1MiB 10MiB
sudo parted "${DEVICE}" --script mkpart live fat16 10MiB 3GiB
sudo parted "${DEVICE}" --script mkpart persistence ext4 3GiB 100%
sudo parted "${DEVICE}" --script set 1 msftdata on
sudo parted "${DEVICE}" --script set 2 legacy_boot on
sudo parted "${DEVICE}" --script set 2 msftdata on
echo "Settling"
# TODO: Test for devices to become visible
sleep 3

echo "Creating filesystems"
# TODO: Fail on fail
sudo mkfs.vfat "${SHORT_VERBOSE}" -n EFI "${DEVICE}1"
sudo mkfs.vfat "${SHORT_VERBOSE}" -n LIVE "${DEVICE}2"
sudo mkfs.ext4 "${SHORT_VERBOSE}" -F -L persistence "${DEVICE}3"

# If mountpoints are currently mounted, umount them
echo "Clearing out any previous data"
for myMount in "${myMountPoints[@]}"; do
  echo "Testing $myMount"
  if /bin/mountpoint --quiet "${myMount}"; then
    sudo umount "${LONG_VERBOSE}" "${myMount}"
  fi
#  if [ -d "${myMount}" ]; then
#    sudo rmdir "${LONG_VERBOSE}" "${myMount}"
#  fi
done

echo "Mounting directories and image"
for dirs in "${myMountPoints[@]}"; do
  mkdir "${LONG_VERBOSE}" "${dirs}"
done
sudo mount "${LONG_VERBOSE}" "${DEVICE}1" "${MYTMPDIR}/usb-efi"
sudo mount "${LONG_VERBOSE}" "${DEVICE}2" "${MYTMPDIR}/usb-live"
sudo mount "${LONG_VERBOSE}" "${DEVICE}3" "${MYTMPDIR}/usb-persistence"
sudo mount "${LONG_VERBOSE}" --options ro "${ISO_FILENAME}" "${MYTMPDIR}/live-iso"

echo "Copying files"
sudo cp --archive --recursive "${LONG_VERBOSE}" "${MYTMPDIR}/live-iso/"* "${MYTMPDIR}/usb-live"

echo "Configuring persistence"
echo "/ union" | sudo tee "${MYTMPDIR}/usb-persistence/persistence.conf"

echo "Install grub"
sudo grub-install --removable --target=x86_64-efi --boot-directory=/tmp/usb-live/boot/ --efi-directory=/tmp/usb-efi ${LONG_VERBOSE} ${DEVICE}
#sudo grub-install --removable --no-uefi-secure-boot --no-nvram --skip-fs-probe \
#  "${LONG_VERBOSE}" --target=x86_64-efi \
#  --boot-directory=${MYTMPDIR}/usb-live/boot/ \
#  --efi-directory=${MYTMPDIR}/usb-efi ${DEVICE}

echo "syslinux install"
echo "dd call"
sudo dd status=progress bs=440 count=1 conv=notrunc if=/usr/lib/syslinux/mbr/gptmbr.bin of="${DEVICE}"
echo "syslinux call"
sudo syslinux --install "${DEVICE}2"
echo "mv calls"
sudo mv "${LONG_VERBOSE}" "${MYTMPDIR}/usb-live/isolinux" "${MYTMPDIR}/usb-live/syslinux"
sudo mv "${LONG_VERBOSE}" "${MYTMPDIR}/usb-live/syslinux/isolinux.bin" "${MYTMPDIR}/usb-live/syslinux/syslinux.bin"
sudo mv "${LONG_VERBOSE}" "${MYTMPDIR}/usb-live/syslinux/isolinux.cfg" "${MYTMPDIR}/usb-live/syslinux/syslinux.cfg"

echo "Grub configuration"
sudo bash -c "sed --in-place 's#isolinux/splash#syslinux/splash#' ${MYTMPDIR}/usb-live/boot/grub/grub.cfg"
sudo bash -c "sed --in-place '0,/boot=live/{s/\(boot=live .*\)$/\1 persistence/}' ${MYTMPDIR}/usb-live/boot/grub/grub.cfg ${MYTMPDIR}/usb-live/syslinux/menu.cfg"
sudo bash -c "sed --in-place '0,/boot=live/{s/\(boot=live .*\)$/\1 keyboard-layouts=us locales=en_AU.UTF-8/}' ${MYTMPDIR}/usb-live/boot/grub/grub.cfg ${MYTMPDIR}/usb-live/syslinux/menu.cfg"

echo "Cleaning up"
# If mountpoints are currently mounted, umount them
for myMount in "${myMountPoints[@]}"; do
  if /bin/mountpoint --quiet "${myMount}"; then
    sudo umount "${LONG_VERBOSE}" "${myMount}"
  fi
  if [ -d "${myMount}" ]; then
    sudo rmdir "${LONG_VERBOSE}" "{$myMount}"
  fi
done
