#!/bin/sh

##########################################################################
# Keep a local mirror of frequently used software
# Copyright (C) Peter Lawler
# Snail mail: PO Box 195
#             Lindisfarne, Tasmania
#             AUSTRALIA 7015
# email:      relwalretep@gmail.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
##########################################################################

###################################################
# Control routines
###################################################

init () {
    printf "Starting at "
    echo "`date`"
    ## need to check /var/lock 1777 (sticky)
    LOCKFILE="/var/lock/mirror-script.$1.lock"
    if [ -f $LOCKFILE ]; then
        echo "$LOCKFILE exists, aborting..."
        exit 1
    fi
    # Linux should really use flock() but not portable enough oh well
    echo $$ > $LOCKFILE
    DATE=`/bin/date +%Y-%m-%d`
    NICE=`which nice`
    if [ -z "$2" ]; then
        LOCAL_MIRROR_DIR=/srv/mirror/pub
    else
        LOCAL_MIRROR_DIR="$2"
    fi
    echo "Local mirror dir set to ${LOCAL_MIRROR_DIR}"
    if [ ! -d $LOCAL_MIRROR_DIR ]; then
        echo "${LOCAL_MIRROR_DIR} NOT MOUNTED / DOESN'T EXIST!"
        exit 1
    fi
    AARNET_RSYNC_MIRROR_HOST="mirror.aarnet.edu.au"
    BERKELEY_RSYNC_MIRROR_HOST="mirrors.ocf.berkeley.edu"
    DEBIAN_MIRROR_HOST="ftp.debian.org"
    DEBIAN_AU_MIRROR_HOST="ftp.au.debian.org"
    DEBIAN_CD_MIRROR_HOST="cdimage.debian.org"
    HEANET_RSYNC_MIRROR_HOST="ftp.heanet.ie"
    INTERNODE_RSYNC_MIRROR_HOST="mirror.internode.on.net"
    KERNELORG_ARCHIVE_RSYNC_MIRROR_HOST="archive.kernel.org"
    KERNELORG_RSYNC_MIRROR_HOST="mirrors.kernel.org"
    LIMESTONE_RSYNC_MIRROR_HOST="mirror.lstn.net"
    LINUXDUKE_RSYNC_MIRROR_HOST="archive.linux.duke.edu"
    LUGMTUEDU_RSYNC_MIRROR_HOST="lug.mtu.edu"
    MIT_RSYNC_MIRROR_HOST="mirrors.mit.edu"
    RIT_RSYNC_MIRROR_HOST="mirrors.rit.edu"
    STEADFAST_RSYNC_MIRROR_HOST="mirror.steadfast.net"

    INTERNAL_MIRROR_HOST="mirror.int.addr"

    ARMHFP="--exclude"
    I386="--exclude"
    X86_64="--include"
    SOURCE="--exclude"
    DEBUG="--exclude"

    RSYNC_GLOBAL_EXCLUDES=""
    PROGRESS="--progress"
#    SYMLINKS="--hard-links --links"
    SYMLINKS="--hard-links --copy-links"
    MOTD="--no-motd"
    TIMEOUT="300" #seconds
#    QUIET="--quiet"
#    DRYRUN="--dry-run"
    if [ "$3" ]; then
        BWLIMIT="--bwlimit=$3"
        echo "BWLIMIT = '${BWLIMIT}'"
    fi
    RSYNC_OPTS="$DRYRUN $SYMLINKS $PROGRESS --numeric-ids --delete --delete-after --delay-updates \
         --times --verbose $QUIET $MOTD --stats --archive --human-readable --partial $BWLIMIT --timeout=${TIMEOUT}"
}

cleanup () {
#    for f in `find $LOCAL_MIRROR_DIR -name .~tmp~`; do
#        du -ksh $f;
#        rm -fr $f;
#    done
    rm $LOCKFILE
    printf "Finishing at "
    echo "`date`"
}

###################################################
# Mirror routines
###################################################

beaglebone_release () {
    printf "Beaglebone foo\n"
    BEAGLEBONE_LOCAL_MIRROR_DIR="${LOCAL_MIRROR_DIR}/BeagleBone"
    BB_REJECT='--reject "bbxm*" --reject "bbx15*" --reject "omap5*" --reject "index.html*"'

    RCN_ROOTFS_DATESTAMP="$(wget --user-agent='' http://rcn-ee.com/rootfs/ --output-document -|grep -v bb|grep -v ee|grep DIR|tail -1|head -1|cut -d \> -f 7|cut -d \/  -f 1)"
    RCN_ROOTFS_URL="https://rcn-ee.com/rootfs/${RCN_ROOTFS_DATESTAMP}/"
    echo "${RCN_ROOTFS_DATESTAMP}"
    echo "${RCN_ROOTFS_URL}"
    if [ ! -d ${BEAGLEBONE_LOCAL_MIRROR_DIR}/black/rcn-ee.com/rootfs/${RCN_ROOTFS_DATESTAMP} ]; then
        wget "${BB_REJECT}" --convert-links --mirror --directory-prefix=${BEAGLEBONE_LOCAL_MIRROR_DIR}/black/ --no-parent --level=4 -e robots=off ${RCN_ROOTFS_URL}
    fi
}

beaglebone_testing () {
    # Really need to just track http://rcn-ee.com/rootfs/bb.org/testing/keepers
    # Really just want console, iot, lxqt-2gb, lxqt-4gb, oemflasher versions
    RCN_BBO_TESTING_DATESTAMP="$(wget --user-agent='' http://rcn-ee.com/rootfs/bb.org/testing/ --output-document -|grep DIR|tail -1|head -1|cut -d \> -f 7|cut -d \/  -f 1)"
    RCN_BBO_TESTING_URL="https://rcn-ee.com/rootfs/bb.org/testing/${RCN_BBO_TESTING_DATESTAMP}"
    echo "RCN_BBO_TESTING_DATESTAMP=${RCN_BBO_TESTING_DATESTAMP=}"
    echo "RCN_BBO_TESTING_URL=${RCN_BBO_TESTING_URL}"
    if [ ! -d ${BEAGLEBONE_LOCAL_MIRROR_DIR}/black/rcn-ee.com/rootfs/bb.org/testing/${RCN_BBO_TESTING_DATESTAMP} ]; then
        wget --reject "bbxm*" --reject "bbx15*" --reject "omap5*" --reject "index.html*" --convert-links --mirror --directory-prefix=${BEAGLEBONE_LOCAL_MIRROR_DIR}/black/ --no-parent --level=4 -e robots=off ${RCN_BBO_TESTING_URL}
    fi
}
beaglebone () {
    beaglebone_release
    beaglebone_testing
}

big_dvcs () {
    for f in `find $LOCAL_MIRROR_DIR -maxdepth 7 -type d -name .bzr|sort|uniq`;do echo $f;bzr pull --directory $f/..;done
    for f in `find $LOCAL_MIRROR_DIR -maxdepth 7 -type d -name .git|sort|uniq`;do echo $f;git -C $f/.. pull;done
    for f in `find $LOCAL_MIRROR_DIR -maxdepth 7 -type d -name .hg|sort|uniq`;do echo $f;hg --cwd $f/.. pull;done
}

centos_init () {
    CENTOS_MIRROR_HOST=$AARNET_RSYNC_MIRROR_HOST
    CENTOS_MIRROR_HOST=$INTERNODE_RSYNC_MIRROR_HOST
    CENTOS_MIRROR_HOST=$KERNELORG_RSYNC_MIRROR_HOST
    CENTOS_DEBUGINFO_MIRROR_HOST="debuginfo.centos.org"
    CENTOS_OPTIONS="$RSYNC_GLOBAL_EXCLUDES $SOURCE=SRPMS $SOURCE=source --exclude=aarch64 --exclude=i386 --exclude=ppc --exclude=ppc64 --exclude=ppc64le  $X86_64=x86_64 $DEBUG=debug"
}

centos () {
    centos_init
    CENTOS="7"
    for f in ${CENTOS}; do
        CENTOS_MIRROR=$LOCAL_MIRROR_DIR/centos/$f && if [ ! -d $CENTOS_MIRROR ]; then mkdir -p $CENTOS_MIRROR; fi
        printf "\nCentOS $f\n"
        printf "rsync://$CENTOS_MIRROR_HOST/centos/$f/ $CENTOS_MIRROR/\n"
        $NICE /usr/bin/rsync $RSYNC_OPTS \
            $CENTOS_OPTIONS \
            rsync://$CENTOS_MIRROR_HOST/centos/$f/ $CENTOS_MIRROR/
    done
}

debian_init () {
    DEBIAN_MIRROR=$LOCAL_MIRROR_DIR/debian && if [ ! -d $DEBIAN_MIRROR ]; then mkdir -p $DEBIAN_MIRROR; fi
    DEBIAN_MIRROR_BACKPORTS=$LOCAL_MIRROR_DIR/debian-backports && if [ ! -d $DEBIAN_MIRROR_BACKPORTS ]; then mkdir -p $DEBIAN_MIRROR_BACKPORTS; fi
    DEBIAN_SECURITY_MIRROR=$LOCAL_MIRROR_DIR/debian-security && if [ ! -d $DEBIAN_SECURITY_MIRROR ]; then mkdir -p $DEBIAN_SECURITY_MIRROR; fi
    DEBIAN_CD_MIRROR=$LOCAL_MIRROR_DIR/debian-cd && if [ ! -d $DEBIAN_CD_MIRROR ]; then mkdir -p $DEBIAN_CD_MIRROR; fi
    DEBIAN_CDIMAGE_MIRROR=$LOCAL_MIRROR_DIR/debian-cdimage && if [ ! -d $DEBIAN_CDIMAGE_MIRROR ]; then mkdir -p $DEBIAN_CDIMAGE_MIRROR; fi
    DEBIAN_RSYNC_MIRROR_HOST="${DEBIAN_AU_MIRROR_HOST}"
    DEBIAN_CD_RSYNC_MIRROR_HOST="${DEBIAN_CD_MIRROR_HOST}"
}


debian_live () {
    DEBIAN_CD_LIVE_OPTIONS="--exclude=*update*DVD* --exclude=*update*CD*
    --exclude=*armel* --exclude=*arm64* --exclude=*armhf*
    --exclude=kfreebsd-* --include=project --exclude=/mips*
    --exclude=/s390x* --exclude=/powerpc* --include=/i386*
    --include=/amd64* --exclude=/*/bt-hybrid --exclude=/*/webboot
    --exclude=*jigdo* --exclude=*cinnamon* --exclude=*source*
    --exclude=*kde* --exclude=*mate* --exclude=*xfce*"
    printf "\nDebian CD current-live\n"
    printf "rsync $RSYNC_OPTS $DEBIAN_CD_LIVE_OPTIONS rsync://$DEBIAN_CD_RSYNC_MIRROR_HOST/debian-cd/current-live/ $DEBIAN_CD_MIRROR/current-live/\n"
    $NICE /usr/bin/rsync $RSYNC_OPTS \
    $DEBIAN_CD_LIVE_OPTIONS \
    rsync://$DEBIAN_CD_RSYNC_MIRROR_HOST/debian-cd/current-live/ $DEBIAN_CD_MIRROR/current-live/
}

debian_dvd () {
    DEBIAN_DVD_OPTIONS="--exclude=*update*DVD* --exclude=*CD*
    --exclude=*armel* --exclude=*arm64* --include=/armhf*
    --exclude=kfreebsd-* --include=project --exclude=/mips*
    --exclude=/powerpc --exclude=/s390x* --include=/i386*
    --include=/amd64* --exclude=/*/bt-hybrid --exclude=/*/webboot
    --exclude=*jigdo* --exclude=/multi* --exclude=/ppc64el*
    --exclude=*source*"
    printf "\nDebian DVD current\n"
    printf "rsync $RSYNC_OPTS $DEBIAN_DVD_OPTIONS rsync://$DEBIAN_CD_RSYNC_MIRROR_HOST/debian-cd/current/ $DEBIAN_CD_MIRROR/current/\n"
    $NICE /usr/bin/rsync $RSYNC_OPTS \
    $DEBIAN_DVD_OPTIONS \
    rsync://$DEBIAN_CD_RSYNC_MIRROR_HOST/debian-cd/current/ $DEBIAN_CD_MIRROR/current/
}

debian_dist () {
	export GNUPGHOME=$LOCAL_MIRROR_DIR/keyring
	arch=amd64
	#	arch=amd64,armel,armhf,i386,powerpc
	diff=mirror
	dist=jessie,jessie-updates,stretch,stretch-updates
	host=ftp.au.debian.org
	allow_dist_rename="--allow-dist-rename"
	ignore_missing_releases="--ignore-missing-release"
	ignore_release_gpg="--ignore-release-gpg"
	ignore_small_errors="--ignore-small-errors"
	method=http
	mirrordir=/$DEBIAN_MIRROR/
	passive="--passive"
	nosources="--nosource"
	i18n="--i18n"
	lang_includes="/i18n/Translation-en*.bz2"
	lang_excludes="/i18n/Translation-.*.bz2"
	root=debian
	section=main,contrib,non-free,main/debian-installer
#	section=main,contrib,non-free
	slowcpu="--slow-cpu"
	#	verbose="--verbose"
	printf "\nDebian $dist $arch Mirror\n"
	debmirror $DRYRUN \
		--arch=$arch \
		--diff=$diff \
		--dist=$dist \
		--rsync-options="-aIL --partial $BWLIMIT" \
		--section=$section \
		--host=$host \
		--root=$root \
		--progress \
		--method=$method \
		--di-dist=dists \
		--di-arch=arches \
		$i18n \
		--exclude="$lang_excludes" \
		--include="$lang_includes" \
		$allow_dist_rename \
		$ignore_missing_releases \
		$ignore_release_gpg \
		$ignore_small_errors \
		$nosources \
		$passive \
		$slowcpu \
		$verbose \
		$mirrordir
}

epel_init () {
    EPEL_MIRROR=$LOCAL_MIRROR_DIR/epel && if [ ! -d $EPEL_MIRROR ]; then mkdir -p $EPEL_MIRROR; fi
    EPEL_MIRROR_SELECTION="$INTERNODE_RSYNC_MIRROR_HOST"
    EPEL_MIRROR_PATH="epel"
#    EPEL_MIRROR_SELECTION="$LINUXDUKE_RSYNC_MIRROR_HOST"
#    EPEL_MIRROR_PATH="fedora-epel"
#    EPEL_MIRROR_SELECTION="$AARNET_RSYNC_MIRROR_HOST"
#    EPEL_MIRROR_PATH="EPEL"
    EPEL_MIRROR_SELECTION="$RIT_RSYNC_MIRROR_HOST"
    EPEL_MIRROR_PATH="epel"
    EPEL_OPTIONS="$RSYNC_GLOBAL_EXCLUDES $SOURCE=/SRPMS $SOURCE=/source --exclude=/aarch64 --exclude=/i386 --exclude=/ppc --exclude=/ppc64 --exclude=/ppc64le $X86_64=/x86_64 $DEBUG=debug"
}

epel () {
    epel_init
    EPELS="7"
    for f in ${EPELS}; do
        printf "\nEPELs $f\n"
        printf "rsync://$EPEL_MIRROR_SELECTION/$EPEL_MIRROR_PATH/$f/ $EPEL_MIRROR/$f/\n"
        $NICE /usr/bin/rsync $RSYNC_OPTS \
        $EPEL_OPTIONS \
        rsync://$EPEL_MIRROR_SELECTION/$EPEL_MIRROR_PATH/$f/ $EPEL_MIRROR/$f/
    done
}

elrepo_init () {
    ELREPO_HTTP_HOST="http://elrepo.org/linux/elrepo/"
    ELREPO_RSYNC_HOST="http://elrepo.org/linux/elrepo/"
    ELREPO_RSYNC_HOST="elrepo.reloumirrors.net/elrepo/elrepo/"
    ELREPO_HOST="$ELREPO_HTTP_HOST"
    ELREPO_MIRROR=$LOCAL_MIRROR_DIR/elrepo.org && if [ ! -d $ELREPO_MIRROR ]; then mkdir -p $ELREPO_MIRROR; fi

    ELREPO_OPTIONS="$RSYNC_GLOBAL_EXCLUDES $SOURCE=SRPMS $SOURCE=source --exclude=i386 --exclude=ppc --exclude=ppc64 $X86_64=x86_64 $DEBUG=debug"
}

elrepo () {
    elrepo_init
    ELREPOS="7"
    for f in ${ELREPOS}; do
        printf "ELREPO ${f}\n"
        printf "rsync://$ELREPO_RSYNC_HOST/el${f}/ $ELREPO_MIRROR/el${f}/\n"
        $NICE /usr/bin/rsync $RSYNC_OPTS \
        $ELREPO_OPTIONS \
            rsync://$ELREPO_RSYNC_HOST/el${f}/ $ELREPO_MIRROR/el${f}/
    done
}

fedora_init () {
    FEDORA_MIRROR=$LOCAL_MIRROR_DIR/fedora/linux && if [ ! -d $FEDORA_MIRROR ]; then mkdir -p $FEDORA_MIRROR; fi
#    FEDORA_MIRROR_SELECTION="$AARNET_RSYNC_MIRROR_HOST/fedora/linux"
    FEDORA_MIRROR_SELECTION="$LINUXDUKE_RSYNC_MIRROR_HOST/fedora/linux"
#    FEDORA_MIRROR_SELECTION="$MIT_RSYNC_MIRROR_HOST/fedora/linux"
#    FEDORA_MIRROR_SELECTION="$STEADFAST_RSYNC_MIRROR_HOST/fedora"
#    FEDORA_MIRROR_SELECTION="$KERNELORG_RSYNC_MIRROR_HOST/fedora"
    FEDORA_RELEASE_RSYNC_OPTIONS="
 $DEBUG=*/debug/* \
 $ARMHFP=*/armhfp/* \
 $SOURCE=*/source/* \
 $I386=*/i386/* \
"
    FEDORA_UPDATES_RSYNC_OPTIONS="
 $DEBUG=*/debug/* \
 $SOURCE=/SRPMS/* \
 $ARMHFP=/armhfp/* \
 $I386=/i386/* \
"
    FEDORA_RSYNC_OPTIONS="$RSYNC_GLOBAL_EXCLUDES $FEDORA_RELEASE_RSYNC_OPTIONS $FEDORA_UPDATES_RSYNC_OPTIONS"
#Todo: Autodetect
    FEDORA_RELEASE="27"
}

fedora_install () {
    for e in ${FEDORA_RELEASE}; do
        if [ ! -d $FEDORA_MIRROR/releases/$e/ ]; then mkdir -p $FEDORA_MIRROR/releases/$e/   ;fi
        printf "Install everything $e\n"
        printf "rsync://$FEDORA_MIRROR_SELECTION/linux/releases/$e/ $FEDORA_MIRROR/releases/$e/\n"
        $NICE /usr/bin/rsync $RSYNC_OPTS \
            $RSYNC_GLOBAL_EXCLUDES $FEDORA_RELEASE_RSYNC_OPTIONS \
            rsync://$FEDORA_MIRROR_SELECTION/releases/$e/ $FEDORA_MIRROR/releases/$e/
    done
}

fedora_update () {
    for e in ${FEDORA_RELEASE}; do
        printf "Updates $e\n"
        if [ ! -d $FEDORA_MIRROR/updates/$e/ ]; then mkdir -p  $FEDORA_MIRROR/updates/$e/   ;fi
        printf "rsync://$FEDORA_MIRROR_SELECTION/linux/updates/$e/ $FEDORA_MIRROR/updates/$e/\n"
        $NICE /usr/bin/rsync $RSYNC_OPTS \
            $RSYNC_GLOBAL_EXCLUDES $FEDORA_UPDATES_RSYNC_OPTIONS \
            rsync://$FEDORA_MIRROR_SELECTION/updates/$e/ $FEDORA_MIRROR/updates/$e/

#        printf "Updates testing $e\n"
#        if [ ! -d $FEDORA_MIRROR/updates/testing/$e/ ]; then mkdir -p $FEDORA_MIRROR/updates/testing/$e/   ;fi
#        printf "rsync://$FEDORA_MIRROR_SELECTION/linux/updates/testing/$e/ $FEDORA_MIRROR/updates/testing/$e/\n"
#        $NICE /usr/bin/rsync $RSYNC_OPTS \
#            $FEDORA_RSYNC_OPTIONS \
#            rsync://$FEDORA_MIRROR_SELECTION/updates/testing/$e/ $FEDORA_MIRROR/updates/testing/$e/
    done
}

openwrt () {
    OPENWRT_MIRROR=$LOCAL_MIRROR_DIR/openwrt.org && if [ ! -d $OPENWRT_MIRROR ]; then mkdir -p $OPENWRT_MIRROR; fi
    OPENWRT_MIRROR_HOST=downloads.openwrt.org
    OPENWRT_PACKAGES_VERSION="17.01"
    OPENWRT_FIRMWARE_VERSION="17.01.4"
    printf "OpenWRT\n"
    INCLUDES="--include='**archer-c5-v2*' --include='**tl-wr1043nd-v2*' --include='**tl-wa850re-v1*' --include='**tl-wdr4300-v1*' --include='config.seed' --include='sha256sums' --include='sha256sums.gpg' --include='**ar71xx-generic.manifest' --include='openwrt-imagebuilder*' --include='openwrt-sdk*' --include='packages**' --exclude='*'"
    mkdir -p $OPENWRT_MIRROR/downloads/releases/${OPENWRT_FIRMWARE_VERSION}/targets/ar71xx/generic
    printf "$NICE /usr/bin/rsync $RSYNC_OPTS $INCLUDES sync://$OPENWRT_MIRROR_HOST/downloads/releases/${OPENWRT_FIRMWARE_VERSION}/targets/ar71xx/generic/ $OPENWRT_MIRROR/downloads/releases/${OPENWRT_FIRMWARE_VERSION}/targets/ar71xx/generic/"
    $NICE /usr/bin/rsync $RSYNC_OPTS $INCLUDES rsync://$OPENWRT_MIRROR_HOST/downloads/releases/${OPENWRT_FIRMWARE_VERSION}/targets/ar71xx/generic/ $OPENWRT_MIRROR/downloads/releases/${OPENWRT_FIRMWARE_VERSION}/targets/ar71xx/generic/
}

libreoffice () {
    LIBREOFFICE_VERSION="6.0.3"
    LIBREOFFICE_MIRROR=$LOCAL_MIRROR_DIR/libreoffice && if [ ! -d $LIBREOFFICE_MIRROR ]; then mkdir -p $LIBREOFFICE_MIRROR; fi
    LIBREOFFICE_MIRROR_HOST=$INTERNODE_RSYNC_MIRROR_HOST
#    LIBREOFFICE_MIRROR_HOST=$AARNET_RSYNC_MIRROR_HOST
    #Win_x86 Linux_x86_64 Linux_x86
    printf "Libre Office\n"
    mkdir -p $LIBREOFFICE_MIRROR/stable/$LIBREOFFICE_VERSION
    printf "rsync://$LIBREOFFICE_MIRROR_HOST/libreoffice/stable/$LIBREOFFICE_VERSION/ $LIBREOFFICE_MIRROR/stable/$LIBREOFFICE_VERSION/"
    $NICE /usr/bin/rsync $RSYNC_OPTS --exclude=win/x86 --exclude=deb --exclude=mac --exclude=rpm --include=$LIBREOFFICE_VERSION \
        rsync://$LIBREOFFICE_MIRROR_HOST/libreoffice/stable/$LIBREOFFICE_VERSION/ $LIBREOFFICE_MIRROR/stable/$LIBREOFFICE_VERSION/
}

linuxconfau () {
    LCA_MIRROR=/srv/media/linuxconfau && if [ ! -d $LCA_MIRROR ]; then mkdir -p $LCA_MIRROR; fi
    LCA_MIRROR_SELECTION=mirror.linux.org.au/pub/linux.conf.au/
    LCA_RSYNC_OPTIONS="--exclude=*.mp4 --exclude=*.ogv"
    LCA_YEARS="2017"
    for e in ${LCA_YEARS}; do
        printf "LCA $e\n"
        printf "rsync://$LCA_MIRROR_SELECTION/$e/ $LCA_MIRROR/$e/\n"
        $NICE /usr/bin/rsync $RSYNC_OPTS $LCA_RSYNC_OPTIONS \
            rsync://$LCA_MIRROR_SELECTION/$e/ $LCA_MIRROR/$e/
    done
}

lulz () {
    LULZ_MIRROR_DIR=${LOCAL_MIRROR_DIR}/lulzbot
    wget --reject "index.html*" --convert-links --mirror --directory-prefix=$LULZ_MIRROR_DIR --no-parent --level=4 -e robots=off http://download.lulzbot.com/TAZ/4.0/
}

rpmfusion_init () {
    fedora_init
    RPMFUSION_MIRROR=$LOCAL_MIRROR_DIR/rpmfusion.org && if [ ! -d $RPMFUSION_MIRROR ]; then mkdir -p $RPMFUSION_MIRROR; fi
    RPMFUSION_HOST="fr2.rpmfind.net/linux"
#    RPMFUSION_HOST="mirror.hiwaay.net"
    RPMFUSION_HOST="$INTERNODE_RSYNC_MIRROR_HOST"
#    RPMFUSION_HOST="download1.rpmfusion.org"
    RPMFUSION_RSYNC_OPTIONS="$RSYNC_GLOBAL_EXCLUDES --exclude=/SRPMS --exclude=/armhfp/* --exclude=/source $DEBUG=*/debug/* $I386=/i386 $X86_64=/x86_64 --include=testing"
    RPMFUSION_EL_RSYNC_OPTIONS="$RSYNC_GLOBAL_EXCLUDES --exclude=/SRPMS --exclude=/source $DEBUG=*/debug/* --exclude=/i386 $X86_64=/x86_64 --include=testing"
}

rpmfusion_install_fedora () {
    rpmfusion_init
    RPMFRELEASE="free nonfree"
    for e in ${FEDORA_RELEASE}; do
        for f in ${RPMFRELEASE}; do
            printf "\nRPMFusion ${f} Fedora ${e} Everything\n"
            if [ ! -d $RPMFUSION_MIRROR/$f/fedora/releases/$e/Everything/ ]; then
                mkdir -p $RPMFUSION_MIRROR/$f/fedora/releases/$e/Everything/
            fi
            printf "rsync://$RPMFUSION_HOST/rpmfusion/$f/fedora/releases/$e/Everything/ $RPMFUSION_MIRROR/$f/fedora/releases/$e/Everything/\n"
            $NICE /usr/bin/rsync $RSYNC_OPTS \
            $RPMFUSION_RSYNC_OPTIONS \
            rsync://$RPMFUSION_HOST/rpmfusion/$f/fedora/releases/$e/Everything/ $RPMFUSION_MIRROR/$f/fedora/releases/$e/Everything/
        done
    done
}

rpmfusion_update_fedora () {
    rpmfusion_init
    RPMFRELEASE="free nonfree"
    for e in ${FEDORA_RELEASE}; do
        for f in ${RPMFRELEASE}; do
            printf "\nRPMFusion $f fedora $e updates\n"
            printf "rsync://$RPMFUSION_HOST/rpmfusion/$f/fedora/updates/$e/ $RPMFUSION_MIRROR/$f/fedora/updates/$e/\n"
            $NICE /usr/bin/rsync $RSYNC_OPTS \
            $RPMFUSION_EL_RSYNC_OPTIONS \
            rsync://$RPMFUSION_HOST/rpmfusion/$f/fedora/updates/$e/ $RPMFUSION_MIRROR/$f/fedora/updates/$e/

            printf "\nRPMFusion $f fedora $e updates testing\n"
            printf "rsync://$RPMFUSION_HOST/rpmfusion/$f/fedora/updates/testing/$e/ $RPMFUSION_MIRROR/$f/fedora/updates/testing/$e/\n"
            $NICE /usr/bin/rsync $RSYNC_OPTS \
            $RPMFUSION_EL_RSYNC_OPTIONS \
            rsync://$RPMFUSION_HOST/rpmfusion/$f/fedora/updates/testing/$e/ $RPMFUSION_MIRROR/$f/fedora/updates/testing/$e/
        done
    done
}

tinycorelinux () {
    TCL_MIRROR=$LOCAL_MIRROR_DIR/tinycorelinux && if [ ! -d $TCL_MIRROR ]; then mkdir -p $TCL_MIRROR; fi
    TCL_MIRROR_HOST=tinycorelinux.net
#    TCL_MIRROR_HOST=$HEANET_RSYNC_MIRROR_HOST/mirrors/tinycorelinux.org
    TCL_RELEASE="9.x"
    #TCL_ARCHS="armv6 armv7 mips x86 x86_64"
    TCL_ARCHS="armv7 x86 x86_64"
    TCL_EXCLUDES="--exclude=src --exclude=release_candidates --exclude=test_releases --exclude=tcz"
    for f in ${TCL_ARCHS}; do
        printf "\nTinyCoreLinux $f\n"
        if [ ! -d $TCL_MIRROR/tc/$TCL_RELEASE/$f/releases ]; then mkdir -p $TCL_MIRROR/tc/$TCL_RELEASE/$f/releases ;fi
        printf "rsync://$TCL_MIRROR_HOST/tc/$TCL_RELEASE/$f/releases/ $TCL_MIRROR/tc/$TCL_RELEASE/$f/releases/\n"
        $NICE /usr/bin/rsync $RSYNC_OPTS $TCL_EXCLUDES \
            rsync://$TCL_MIRROR_HOST/tc/$TCL_RELEASE/$f/releases/ $TCL_MIRROR/tc/$TCL_RELEASE/$f/releases/
    done
}

torproject () {
    TOR_MIRROR=$LOCAL_MIRROR_DIR/torproject/dist-mirror && if [ ! -d $TOR_MIRROR ]; then mkdir -p $TOR_MIRROR; fi
    TOR_MIRROR_SELECTION=rsync.torproject.org/dist-mirror
    TOR_RSYNC_OPTIONS=""
    printf "TOR\n"
    printf "rsync://$TOR_MIRROR_SELECTION/tor-packages $TOR_MIRROR\n"
    $NICE /usr/bin/rsync $RSYNC_OPTS $TOR_RSYNC_OPTIONS \
        --exclude=/torbrowser/* --include=torbrowser/*en-*.exe \
        rsync://$TOR_MIRROR_SELECTION/ $TOR_MIRROR/

    TAILS_MIRROR=$LOCAL_MIRROR_DIR/TAILS && if [ ! -d $TAILS_MIRROR ]; then mkdir -p $TAILS_MIRROR; fi
    TAILS_MIRROR_SELECTION=rsync.torproject.org/amnesia-archive/tails/stable
    TAILS_RSYNC_OPTIONS="--exclude=/iuk"
    printf "TAILS\n"
    printf "rsync://rsync.torproject.org/amnesia-archive/tails/stable $TAILS_MIRROR\n"
    $NICE /usr/bin/rsync $RSYNC_OPTS $TAILS_RSYNC_OPTIONS \
        rsync://$TAILS_MIRROR_SELECTION/ $TAILS_MIRROR/
}

ubcd () {
    UBCD_MIRROR=$LOCAL_MIRROR_DIR/ubcd && if [ ! -d "${UBCD_MIRROR}" ]; then mkdir -p "${UBCD_MIRROR}"; fi
    UBCD_MIRROR_HOST="${INTERNODE_RSYNC_MIRROR_HOST}"
#    UBCD_MIRROR_HOST=$AARNET_RSYNC_MIRROR_HOST
#    UBCD_MIRROR_HOST=$LUGMTUEDU_RSYNC_MIRROR_HOST
    LATEST_UBCD=$(curl -s ftp://mirror.internode.on.net/pub/ubcd/|grep -v live|tail -1|awk '{print $9}'|cut -d . -f 1)
    LATEST_UBCDLIVE=$(curl -s ftp://mirror.internode.on.net/pub/ubcd/|grep live|tail -1|awk '{print $9}'|cut -d . -f 1)

    printf "\nUltimate Boot CD %s %s\n" "$LATEST_UBCD" "$LATEST_UBCDLIVE"
    printf "rsync://${UBCD_MIRROR_HOST}/ubcd/ ${UBCD_MIRROR}/\n"
    $NICE /usr/bin/rsync ${RSYNC_OPTS} \
        rsync://${UBCD_MIRROR_HOST}/ubcd/${LATEST_UBCD}\*  "${UBCD_MIRROR}"/
    $NICE /usr/bin/rsync ${RSYNC_OPTS} \
        rsync://${UBCD_MIRROR_HOST}/ubcd/${LATEST_UBCDLIVE}\*  "${UBCD_MIRROR}"/
}

chdk () {
    printf "\nchdk\n"
    CHDK_MIRROR="${LOCAL_MIRROR_DIR}/chdk" && if [ ! -d "${CHDK_MIRROR}" ]; then mkdir -p "${CHDK_MIRROR}"; fi
    CHDK_MIRROR_HOST="http://subversion.assembla.com/svn/"
    CHDK_RELEASE="release-1_4"

    printf "\nchdk 1.4\n"
    if [ ! -d "${CHDK_MIRROR}/release-1_4/.svn" ]; then
        svn checkout $CHDK_MIRROR_HOST/chdk/branches/$CHDK_RELEASE/ "${CHDK_MIRROR}/${CHDK_RELEASE}"
    else
        svn update "${CHDK_MIRROR}/${CHDK_RELEASE}"
    fi
    printf "\nchdk trunk\n"
    if [ ! -d "${CHDK_MIRROR}/trunk/.svn" ]; then
        svn checkout $CHDK_MIRROR_HOST/chdk/trunk "${CHDK_MIRROR}/trunk"
    else
        svn update "${CHDK_MIRROR}/trunk"
    fi

    printf "\nchdkptp\n"
    if [ ! -d "${CHDK_MIRROR}/chdkptp/.svn" ]; then
        svn checkout $CHDK_MIRROR_HOST/chdkptp/trunk "${CHDK_MIRROR}"/chdkptp
    else
        svn update "${CHDK_MIRROR}"/chdkptp
    fi
}

ml () {
    ML_MIRROR=$LOCAL_MIRROR_DIR/magic-lantern && if [ ! -d "${ML_MIRROR}" ]; then mkdir -p "${ML_MIRROR}"; fi
    ML_MIRROR_HOST="https://bitbucket.org/hudson/magic-lantern/"
    # ML_RELEASE="release-1_4"

    printf "\nMagic Lantern\n"

    if [ ! -d "${ML_MIRROR}/src/.hg" ]; then
        hg clone -r unified "${ML_MIRROR_HOST}" "${ML_MIRROR}"/src/
    else
        hg pull --cwd "${ML_MIRROR}/src/"
    fi
}


###################################################
# case opstions
###################################################
case "$1" in

bb)
    init "$1" "$2" "$3"
    beaglebone
    cleanup
    ;;

cent)
    init "$1" "$2" "$3"
    centos
    epel
    elrepo
    cleanup
    ;;

deb)
    init "$1" "$2" "$3"
    debian_init
    debian_dvd
#	debian_dist
    debian_live
    cleanup
    ;;

dvcs)
    init "$1" "$2" "$3"
    big_dvcs
    cleanup
    ;;

fed)
    init "$1" "$2" "$3"
    fedora_init
    fedora_install
    fedora_update
    rpmfusion_init
    rpmfusion_install_fedora
    rpmfusion_update_fedora
    cleanup
    ;;

lca)
    init "$1" "$2" "$3"
    linuxconfau
    cleanup
    ;;

rest)
    init "$1" "$2" "$3"
#    openwrt
    libreoffice
    lulz
    tinycorelinux
    ubcd
    cleanup
    ;;

tor)
    init "$1" "$2" "$3"
    torproject
    cleanup
    ;;

cam)
    init "$1" "$2" "$3"
    chdk
    ml
    cleanup
    ;;

*)
    echo "Usage: $0 {bb|cent|deb|dvcs|fed|lca|rest|tor} {destination}"
    exit 1
    ;;

esac

exit 0
