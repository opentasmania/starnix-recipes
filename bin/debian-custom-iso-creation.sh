#!/bin/bash
#
# Create a custom debian install iso
# Copyright 2022 Peter Lawler

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# trap ctrl-c and call ctrl_c()
trap ctrl_c INT

# TODO: Does NOT handle patch version of installer releases without hardcode
# TODO: Choose between base iso of netinstall or DVD1
# TODO: On debian 12 and future, we want to add in b43-installer and the like (firmware that doesn't ship)

set -e
set -o pipefail

function usage() {
  echo "Usage: $0" 1>&2
  echo "[-b] BIOS mode" 1>&2
  echo "[-i identifier]" 1>&2
  echo "[-n] insert non-free firmware" 1>&2
  echo "[-p preseed-file]" 1>&2
  echo "[-u] UEFI mode" 1>&2
  echo "[-a] Automatic install" 1>&2
}

function init() {
  local OS
  if [ "${BASH_VERSINFO:-0}" -lt 5 ]; then
    echo "Need bash(1) version 5 minimum"
    exit 1
  fi
  if (command -v tput >/dev/null 2>&1); then
    TEXT_BOLD="$(tput bold)"
    TEXT_UL_ON="$(tput smul)"
    TEXT_UL_OFF="$(tput rmul)"
    TEXT_REVERSE="$(tput rev)"
    TEXT_NORMAL="$(tput sgr0)"
    BEEP="$(tput bel)"
  else
    TEXT_BOLD=""
    TEXT_UL_ON=""
    TEXT_UL_OFF=""
    TEXT_REVERSE=""
    TEXT_NORMAL=""
  fi
  if [ ! -f /etc/os-release ]; then
    echo "${TEXT_BOLD}Initial OS detection failure (no /etc/os-release)${TEXT_NORMAL}"
    $BEEP
    exit 1
  fi
  unset CPU_TYPE
  CPU_TYPE="$(dpkg --print-architecture)"
  if [ -z "${CPU_TYPE}" ]; then
    echo "$TEXT_BOLD CPU type not detected $TEXT_NORMAL"
    exit 1
  fi
  # shellcheck disable=SC1001
  OS="$(hostnamectl status | grep "Operating System" | cut -d \: -f 2 | sed s/^[[:space:]]//g)"
  if [ ! "${OS}" ]; then
    echo "${TEXT_BOLD}OS NOT DETECTED. EXITING${TEXT_NORMAL}"
    $BEEP
    exit 1
  fi
  echo "Detected OS: ${TEXT_UL_ON}${OS}${TEXT_UL_OFF}"
  APT=$(command -v apt 2>/dev/null)
  /bin/echo "apt(1) found at $APT"
  if [ -z "${APT}" ]; then
    echo "${TEXT_BOLD}apt(1) not found (System: ${OS})${TEXT_NORMAL}"
    $BEEP
    exit 1
  fi
  if [ ${UID} != "0" ]; then
    echo "Not root user"
    if ! hash sudo 2>/dev/null; then
      echo "sudo(1) not available, attempting to force install"
      echo "su --login --command \"apt update\""
      su --login --command "apt update" ||
        {
          echo >&2 "${TEXT_BOLD}Cannot issue 'apt update'. Aborting.${TEXT_NORMAL}"
          exit 1
        }
      echo "su --login --command \"apt --assume-yes install sudo\""
      su --login --command "apt --assume-yes install sudo" ||
        {
          echo >&2 "${TEXT_BOLD}Cannot issue 'apt --assume-yes install sudo'. Aborting.${TEXT_NORMAL}"
          exit 1
        }
    fi
    HAVE_SUDO=$(command -v sudo 2>/dev/null)
    echo "sudo(1) found at ${HAVE_SUDO}"
    # SUDO_USER="${USER}"
  else
    /bin/echo "Running as root, sudo(1) not needed."
    HAVE_SUDO=""
  fi
  apt_update

  # read dialog <<<"$(which whiptail dialog 2>/dev/null)"
  # [[ "$dialog" ]] || {
  #  echo 'neither whiptail nor dialog found' >&2
  #  echo "Installing dialog(1)"
  #  install_packages "dialog"
  # }
  # TODO: Used for dialog (or is this redundant in later releases?)
  # TMPFILE=$(mktemp --tmpdir="$(dirname "$0")")
}

function parseopts() {
  local o
  unset BIOS
  unset UEFI
  unset AUTOMATED
  NONFREEFIRMWARE="false"
  CUSTOM_ISO_OUTPUT_FILE_STRING="custom"
  while getopts "abi:np:u" o; do
  case $o in
  a)
    AUTOMATED="true"
    ;;
  b)
    BIOS="true"
    ;;
  i)
    CUSTOM_ISO_OUTPUT_FILE_STRING="${OPTARG}"
    echo "${CUSTOM_ISO_OUTPUT_FILE_STRING}"
    ;;
  n)
    NONFREEFIRMWARE="true"
    ;;
  p)
    if [ -z "${OPTARG}" ]; then
      usage
      exit 1
    fi
    local PRESEEDFILE
    PRESEEDFILE=${OPTARG}
    if [ ! -f "$PRESEEDFILE" ]; then
      echo "Preseedfile not found $PRESEEDFILE"
      usage
      exit 1
    else
      FULLPRESEEDFILE="$(pwd)/${PRESEEDFILE}"
    fi
    ;;
  u)
    UEFI="true"
    ;;
  ?)
    echo "Unknown option passed"
    usage
    exit 1
    ;;
  *)
    unset PRESEEDFILE
    ;;
  esac
done
  if [ -z "${BIOS}" ] && [ -z "${UEFI}" ]; then
    usage
    echo "Select BIOS [-b], UEFI [-u], or both [-bu] mode"
    exit
  fi
}

function finish() {
  echo "NOT Removing tempfile (because we haven't used it (yet?))"
  # rm --force --verbose "${TMPFILE}"
  echo "Removing redundant packages"
  remove_redundant_packages ""
}

function ctrl_c() {
  echo "** Trapped CTRL-C"
  # cleanup
  $BEEP
  exit 1
}

function apt_update() {
  ${HAVE_SUDO} "${APT}" update
}

function remove_redundant_packages() {
  apt_update
  local unwanted_pkgs=""
  local pkg_check=("$@")
  for pkg in "${pkg_check[@]}"; do
    LC_ALL=C /usr/bin/dpkg --list | awk '{print $2}' | grep "^${pkg}" >/dev/null || unwanted_pkgs="${unwanted_pkgs}${pkg} "
  done

  if [ "${unwanted_pkgs}" ]; then
    echo "Removing: ${unwanted_pkgs}"
    # shellcheck disable=SC2086
    ${HAVE_SUDO} ${APT} --assume-yes --purge remove ${unwanted_pkgs}
    cleanup_packages
    # shellcheck disable=SC2086
    ${HAVE_SUDO} "${APT}" clean
  fi
}

function install_packages() {
  # TODO: prereq dpkg and apt
  local needed_pkgs=""
  local pkg_check=("$@")
  for pkg in "${pkg_check[@]}"; do
    LC_ALL=C /usr/bin/dpkg --list | awk '{print $2}' | grep "^${pkg}" >/dev/null || needed_pkgs="${needed_pkgs}${pkg} "
  done

  if [ "${needed_pkgs}" ]; then
    echo "Installing: ${needed_pkgs}"
    # shellcheck disable=SC2086
    ${HAVE_SUDO} ${APT} --assume-yes install ${needed_pkgs}
    cleanup_packages
    # shellcheck disable=SC2086
    ${HAVE_SUDO} ${APT} clean
  fi
}

function cleanup_packages() {
  local PURGE_LIST
  local DEINSTALL_LIST
  echo "${TEXT_BOLD}autoremove${TEXT_NORMAL}"
  # shellcheck disable=SC2086
  ${HAVE_SUDO} ${APT} --assume-yes --purge autoremove
  PURGE_LIST="$(dpkg --list | grep '^rc' | awk '{ print $2 }')"
  if [[ ${PURGE_LIST} != "" ]]; then
    echo "${TEXT_BOLD}Additional purge${TEXT_NORMAL}"
    # shellcheck disable=SC2086
    ${HAVE_SUDO} ${APT} --assume-yes purge $PURGE_LIST
  fi
  DEINSTALL_LIST="$(dpkg --get-selections | grep deinstall | cut -f 1)"
  if [[ ${DEINSTALL_LIST} != "" ]]; then
    echo "${TEXT_BOLD}Deinstalling${TEXT_NORMAL}"
    # shellcheck disable=SC2086
    ${HAVE_SUDO} ${APT} --assume-yes purge "$DEINSTALL_LIST"
  fi
}

function prereqs() {
  install_packages 'coreutils' 'genisoimage' 'isolinux' 'wget' 'xorriso'
}

function download_debian_netinstall() {
  local DEBIAN_URL_PREFIX
  local DEBIAN_NONFREE_FIRMWARE_URL
  local DEBIAN_SHA512_URL
  DEBIAN_NONFREE_FIRMWARE_URL=https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/
  DEBIAN_URL_PREFIX="https://cdimage.debian.org/debian-cd/current/${CPU_TYPE}/iso-cd"
  DEBIAN_ISO_FILENAME=debian-"${DEBIAN_CURRENT_RELEASE}".0-${CPU_TYPE}-netinst.iso
  DEBIAN_SHA512_URL="$DEBIAN_URL_PREFIX/SHA512SUMS"
  if [ ! -f "debian-${DEBIAN_CURRENT_RELEASE}.0-iso.SHA512SUMS" ]; then
    echo "Retrieving debian iso sha512sums"
    wget --quiet --show-progress --directory-prefix="$(pwd)" --output-document="debian-${DEBIAN_CURRENT_RELEASE}.0-iso.SHA512SUMS" "${DEBIAN_SHA512_URL}"
  fi
  if [ ! -f "${DEBIAN_ISO_FILENAME}" ]; then
    echo "Retrieving debian iso"
    wget --quiet --show-progress --directory-prefix="$(pwd)" "${DEBIAN_URL_PREFIX}/${DEBIAN_ISO_FILENAME}"
  fi
  echo "Checking ${DEBIAN_ISO_FILENAME} sha512sum"
  printf %s "${TEXT_UL_ON}"
  sha512sum --ignore-missing --check "debian-${DEBIAN_CURRENT_RELEASE}.0-iso.SHA512SUMS"
  printf %s "${TEXT_UL_OFF}"
  if [ "$NONFREEFIRMWARE" = true ]; then
    if [ ! -f "debian-${DEBIAN_CURRENT_RELEASE}-nonfree-firmware.SHA512SUMS" ]; then
      echo "Retrieving debian ${TEXT_BOLD}non-free${TEXT_NORMAL} firmware sha512sums"
      wget --quiet --show-progress --directory-prefix="$(pwd)" --output-document="debian-${DEBIAN_CURRENT_RELEASE}-nonfree-firmware.SHA512SUMS" "${DEBIAN_NONFREE_FIRMWARE_URL}/${DEBIAN_CURRENT_RELEASE}.0/SHA512SUMS"
    fi
    # TODO: If there's a firmware file, grab the SHA512SUMS and test it, then download if it doesn't match
    if [ ! -f  "debian-${DEBIAN_CURRENT_RELEASE}-nonfree-firmware.tar.gz" ]; then
      echo "Retrieving debian ${TEXT_BOLD}non-free${TEXT_NORMAL} tarball"
      wget --quiet --show-progress --directory-prefix="$(pwd)" --output-document="firmware.tar.gz" "${DEBIAN_NONFREE_FIRMWARE_URL}/${DEBIAN_CURRENT_RELEASE}.0/firmware.tar.gz"
    fi
    echo "Checking $TEXT_BOLD debian-${DEBIAN_CURRENT_RELEASE}-nonfree-firmware.tar.gz $TEXT_NORMAL sha512sum"
    printf %s "${TEXT_UL_ON}"
    sha512sum --ignore-missing --check "debian-${DEBIAN_CURRENT_RELEASE}-nonfree-firmware.SHA512SUMS"
    printf %s "${TEXT_UL_OFF}"
    cp firmware.tar.gz "debian-${DEBIAN_CURRENT_RELEASE}-nonfree-firmware.tar.gz"
  fi
}

function create_custom_iso() {
  local loop_directory
  local cd_directory
  local initrd_modules_directory
  local output
  loop_directory=__loopdir__
  cd_directory=__cd__
  initrd_modules_directory=__irmod__
  if [ -d "${loop_directory}" ]; then
    ${HAVE_SUDO} rm --force --recursive "${loop_directory}"
  fi
  if [ -d "${cd_directory}" ]; then
    ${HAVE_SUDO} rm --force --recursive "${cd_directory}"
  fi
  if [ -d "${initrd_modules_directory}" ]; then
    ${HAVE_SUDO} rm --force --recursive "${initrd_modules_directory}"
  fi
  if [ ! -d "iso" ]; then
    mkdir "iso"
  fi
  mkdir "${cd_directory}"
  mkdir "${initrd_modules_directory}"
  mkdir "${loop_directory}"
  ${HAVE_SUDO} mount --options loop "${DEBIAN_ISO_FILENAME}" "${loop_directory}"
  rsync --archive --hard-links --exclude=TRANS.TBL "${loop_directory}"/ "${cd_directory}"
  ${HAVE_SUDO} umount "${loop_directory}"
  (
    cd "${initrd_modules_directory}" || exit
    gzip -d <../"${cd_directory}"/install.amd/initrd.gz |
      ${HAVE_SUDO} cpio --extract --make-directories --no-absolute-filenames
    if [ "${FULLPRESEEDFILE}" ]; then
      echo "Installing custom preseed file"
      # TODO: Better file handling
      cp --verbose "${FULLPRESEEDFILE}" $(pwd)/preseed.cfg
    fi
    echo "Creating new initrd, please wait"
    GZIP_LEVEL="-9"
    ${HAVE_SUDO} bash -c "find . | cpio -H newc --create | gzip ${GZIP_LEVEL} > ../${cd_directory}/install.amd/initrd.gz"
  )
  ${HAVE_SUDO} rm --force --recursive "${initrd_modules_directory}"
  if [ "${NONFREEFIRMWARE}" = "true" ]; then
    CUSTOM_ISO_OUTPUT_FILE_STRING="${CUSTOM_ISO_OUTPUT_FILE_STRING}-nonfree-firmware"
    echo "Extracting nonfree firmware"
    ${HAVE_SUDO} tar --extract --file "debian-${DEBIAN_CURRENT_RELEASE}-nonfree-firmware.tar.gz" --directory=${cd_directory}/firmware/.
  fi
  cd "${cd_directory}" || exit
  # shellcheck disable=SC2046
  # shellcheck disable=SC2086
  ${HAVE_SUDO} md5sum $(find . -follow -type f) | ${HAVE_SUDO} dd conv=notrunc of=md5sum.txt
  cd ..
  if [ -n "${AUTOMATED}" ]; then
    echo "Prep automated"
  fi
  # TODO: getopts iso creation
  echo "Creating iso image"
  # For legacy BIOS
  if [ -n "${BIOS}" ]; then
    echo "BIOS mode"
    output="iso/debian-${DEBIAN_CURRENT_RELEASE}.0-${CUSTOM_ISO_OUTPUT_FILE_STRING}-${CPU_TYPE}-BIOS-netinst.iso"
    echo "Creating ${output}"
    ${HAVE_SUDO} genisoimage -quiet \
     -o "${output}" \
      -r \
      -J \
      -no-emul-boot \
      -boot-load-size 4 \
      -boot-info-table \
      -b isolinux/isolinux.bin \
      -c isolinux/boot.cat \
      ./${cd_directory}
    sha512sum "${output}" > "${output}".SHA512SUM
  fi
  if [ -n "${UEFI}" ]; then
    # For UEFI
    # Notes:
    # https://lists.debian.org/debian-live/2014/06/msg00090.html
    # https://stackoverflow.com/questions/31831268/genisoimage-and-uefi
    # https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=857597
    # https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=879004
    # I HATE MY LIFE THIS IS WHY I'VE YET TO TEST THIS AND I DO NOT CARE
    # GET ME A FUCKING TOWEL
    # Untested
    # Need to put this into preseed as well

# Force UEFI booting ('BIOS compatibility' will be lost). Default: false.
#d-i partman-efi/non_efi_system boolean false

    echo "UEFI ${TEXT_BOLD}BROKEN${TEXT_NORMAL} mode"
    output="debian-${DEBIAN_CURRENT_RELEASE}.0-${CUSTOM_ISO_OUTPUT_FILE_STRING}-${CPU_TYPE}-UEFI-netinst.iso"
    echo "Creating ${output}"
    ${HAVE_SUDO} xorriso \
      -as mkisofs \
      -quiet \
      -o "${output}" \
      -r \
      -J \
      -no-emul-boot \
      -boot-load-size 4 \
      -boot-info-table \
      -b isolinux/isolinux.bin \
      -isohybrid-mbr /usr/lib/ISOLINUX/isohdpfx.bin \
      -eltorito-alt-boot \
      -isohybrid-gpt-basdat \
      -c isolinux/boot.cat \
      ./${cd_directory}
    sha512sum "${output}" > "${output}".SHA512SUM
  fi
  ${HAVE_SUDO} rm --force --recursive "${loop_directory}"
  ${HAVE_SUDO} rm --force --recursive "${cd_directory}"
  echo "${TEXT_REVERSE}COMPLETED${TEXT_NORMAL} at $(date) "
}

function custom_init() {
  # TODO: Handle PATCH version
  echo "Determining current debian release"
  DEBIAN_CURRENT_RELEASE="$(wget --show-progress --quiet --output-document - https://ftp.debian.org/debian/dists/stable/Release | grep "Version" | cut -f 2 -d \ )"
  export DEBIAN_CURRENT_RELEASE
  echo "Current debian release is ${TEXT_BOLD}${DEBIAN_CURRENT_RELEASE}${TEXT_NORMAL}"
}

function custom_cleanup() {
  unset DEBIAN_CURRENT_RELEASE
}

function main() {
  parseopts "$@"
  init
  prereqs
  custom_init
  download_debian_netinstall
  create_custom_iso
  custom_cleanup
  :
}

main "$@"
exit 0