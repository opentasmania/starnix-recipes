#!/bin/bash

if ! command -v rsync >/dev/null; then
  echo 'rsync is required but it is not installed.'
  exit 1
fi

ISCSI_PORTAL="iscsi_portal"
ISCSI_TARGET="iscsi_target"
ISCSI_ADDRESS="${ISCSI_PORTAL}:${ISCSI_TARGET}"

BACKUP_DIR="/mnt/isci/${ISCSI_ADDRESS}/Backups/Proxmox/"
BACKUP_FOLDER="proxmox-config-backup-$(date +%Y-%m-%d-%H-%M-%S)"

RSYNC_OPTIONS=" --archive --compress --verbose "

# check if ISCSI_PORTAL is not set or empty
if [ -z "${ISCSI_PORTAL}" ]; then
  echo 'ISCSI_PORTAL must be set'
  exit 1
fi

# check if ISCSI_TARGET is not set or empty
if [ -z "${ISCSI_TARGET}" ]; then
  echo 'ISCSI_TARGET must be set'
  exit 1
fi

# check if backup directory is accessible (and likely mounted)
if ! [ -e "${BACKUP_DIR}" ]; then
  echo "Backup directory ${BACKUP_DIR} does not exist or is not accessible"
  exit 1
fi
if ! [ -w "${BACKUP_DIR}" ]; then
  echo "Backup directory ${BACKUP_DIR} does not exist or is not writable"
  exit 1
else
  mkdir --parents "${BACKUP_DIR}/${BACKUP_FOLDER}" || {
    echo 'Failed to create backup directory'
    exit 1
  }
fi

rsync ${RSYNC_OPTIONS} /etc/pve "${BACKUP_DIR}/${BACKUP_FOLDER}" || {
  echo 'Failed to sync PVE configuration'
  exit 1
}

rsync ${RSYNC_OPTIONS} /etc/vzdump.conf "${BACKUP_DIR}/${BACKUP_FOLDER}" || {
  echo 'Failed to sync vzdump configuration'
  exit 1
}

echo "Proxmox server configuration files backed up to ${BACKUP_DIR}/${BACKUP_FOLDER}"
