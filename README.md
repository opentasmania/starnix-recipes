# starnix-recipes
A collection of scripts and bits-n-pieces to make my *nix life a little easier.

* At the moment, I only run GNU/Linux 4.4 kernels and above on Debian Testing OS
     (and as of the most recent update to this file, this means Debian Stretch).
* Nothing is guaranteed to work with lower kernel versions or other Linux variants.
* Using my scripts on other systems is liable to cause smoke to escape.
* Much of this code is based on things I wrote last century.
* A fair amount of this code is written under the influence of alcohol.
