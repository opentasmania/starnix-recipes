#!/usr/bin/env bash

# Proxmox iSCSI check script
# Copyright (C) 2025 Peter Lawler <relwalretep@gmail.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Place into a cronjob, for example
# */1 * * * * /etc/pve/hooks/iscsi_hook.sh  # Run every minute

ISCSI_TARGET="<your_iscsi_target_iqn>"      # Replace with your iSCSI target IQN
ISCSI_TARGET_IP="<your_iscsi_target_ip>"  # Replace with your iSCSI target IP
STORAGE_ID="<your_storage_id>"          # Replace with your storage ID
LOG_FILE="/var/log/iscsi_check.log"

# Check for required commands at the beginning
if ! command -v iscsiadm >/dev/null 2>&1; then
  log_message "Error: iscsiadm is not installed. Exiting."
  exit 1
fi

TIMEOUT_CMD=""  # Initialize as empty
if command -v timeout >/dev/null 2>&1; then
  TIMEOUT_CMD="timeout 5"  # Set to "timeout 5" if available
fi

# Automatically populate BACKUP_JOB_IDS
BACKUP_JOB_IDS=""
pvesr list --type=vzdump | while read line; do
  JOB_ID=$(echo "$line" | grep -oE '^+' | head -n 1)
  if [[ -n "$JOB_ID" ]]; then
    if vzdump --id "$JOB_ID" --status | grep -q "enabled: 1"; then
      if [[ -z "$BACKUP_JOB_IDS" ]]; then
        BACKUP_JOB_IDS="$JOB_ID"
      else
        BACKUP_JOB_IDS+=",${JOB_ID}"
      fi
    fi
  fi
done < <(pvesr list --type=vzdump)

# Function to log a message
log_message() {
  local message="$1"
  if command -v logger >/dev/null 2>&1; then
    logger -t iscsi_check "$message"
  elif [ -S /dev/log ]; then
    echo "iscsi_check: $message" | nc -w 1 /dev/log
  else
    echo "$(date): $message" >> "$LOG_FILE"
  fi
}

# Check iSCSI target and manage storage/backups
if iscsiadm -m session -o show | grep -q "Target: $ISCSI_TARGET"; then
  log_message "Target $ISCSI_TARGET is available"
  if ! pvesr set "$STORAGE_ID" --enable 1 2>/dev/null; then
    log_message "Error enabling storage $STORAGE_ID"
  else
    log_message "iSCSI storage $STORAGE_ID enabled"
  fi
  for JOB_ID in $(echo "$BACKUP_JOB_IDS" | tr ',' ' '); do
    if ! vzdump --id "$JOB_ID" --enable 1 2>/dev/null; then
      log_message "Error enabling backup job $JOB_ID"
    else
      log_message "Backup job $JOB_ID enabled"
    fi
  done
  exit 0  # Success

else  # Target NOT available
  log_message "Target $ISCSI_TARGET is NOT available (iscsiadm failed)"

  # Ping check
  if [[ -n "$TIMEOUT_CMD" ]]; then
    if $TIMEOUT_CMD ping -c 3 "$ISCSI_TARGET_IP" > /dev/null 2>&1; then
      log_message "Target $ISCSI_TARGET_IP is pingable, but iSCSI is down."
    else
      log_message "Target $ISCSI_TARGET_IP is NOT pingable (or ping timed out)."
    fi
  else
    log_message "timeout command is not available. Skipping ping timeout."
    if ping -c 3 "$ISCSI_TARGET_IP" > /dev/null 2>&1; then
      log_message "Target $ISCSI_TARGET_IP is pingable, but iSCSI is down."
    else
      log_message "Target $ISCSI_TARGET_IP is NOT pingable."
    fi
  fi

  if ! pvesr set "$STORAGE_ID" --enable 0 2>/dev/null; then
    log_message "Error disabling storage $STORAGE_ID"
  else
    log_message "iSCSI storage $STORAGE_ID disabled"
  fi
  for JOB_ID in $(echo "$BACKUP_JOB_IDS" | tr ',' ' '); do
    if ! vzdump --id "$JOB_ID" --enable 0 2>/dev/null; then
      log_message "Error disabling backup job $JOB_ID"
    else
      log_message "Backup job $JOB_ID disabled"
    fi
  done
  exit 1  # Failure
fi