hotplug.d - OpenWRT udev style scripts
======================================
Some scripts for [OpenWRT](http://www.openwrt.org)'s [hotplug2](https://wiki.openwrt.org/doc/techref/hotplug) system.
Note that hotplug2 has been replaced by [procd](https://wiki.openwrt.org/inbox/procd-init-scripts) for future released but scripts should remain backwards compatible for now.
