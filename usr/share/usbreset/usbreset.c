/*
usbreset -- send a USB port reset to a USB device
Copyright (C) Peter Lawler <relwalretep@gmail.com>
Copyright (C) 2008 Alan Stern linux-usb mail list
Copyright (C) Others
*/

/*
https://marc.info/?l=linux-usb&m=121459435621262&w=2
*/

/*
Compile with gcc
$ cc usbreset.c -o usbreset

Set result as executable:
$ chmod +x usbreset

Get Bus and Device ID of the USB device to reset:
$ lsusb
Bus 001 Device 002: ID baad:beef USBResetDevice

Execute the program with sudo privilege; make necessary substitution for (Bus) and (Device) IDs as found by running lsusb:
$ sudo ./usbreset /dev/bus/usb/001/002
*/

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

#include <sys/ioctl.h>
#include <linux/usbdevice_fs.h>


int main(int argc, char **argv)
{
    const char *filename;
    int fd;
    int rc;

    if (argc != 2) {
        perror("Usage: usbreset device-filename");
        return 1;
    }
    filename = argv[1];

    // Test for device existence
    if ( access( filename, F_OK ) != 0 ) {
        perror("Error: No such device-filename\n");
        return 1;
    } else {
        // Test for write enabled to device
        fd = open(filename, O_WRONLY);
        if (fd < 0) {
        perror("Error: opening device USB device");
        return 1;
        }
        // Attempt to reset the device
        printf("Resetting USB device %s reset\n", filename);
        rc = ioctl(fd, USBDEVFS_RESET, 0);
        // Error on failure
        if (rc < 0) {
            perror("Error: ioctl USBDEVFS_RESET failure");
            return 1;
        }
        printf("Completed USB device %s reset\n", filename);
        // Close file descriptor
        close(fd);
        printf("Closed fd\n");
        return 0;
        }
    perror("General failure");
}
