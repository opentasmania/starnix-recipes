#!/bin/bash

#.bash_aliases - a collection of bash aliases and alias handling routines
#Copyright (C) Peter Lawler
#Snail mail: PO Box 195
#             Lindisfarne, Tasmania
#            AUSTRALIA 7015
#email:      relwalretep@gmail.com
#
#This program is free software; you can redistribute it and/or
#modify it under the terms of the GNU General Public License
#as published by the Free Software Foundation; either version 2
#of the License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

alias bc='bc -l'
alias chown='chown --preserve-root'
alias chmod='chmod --preserve-root'
alias chgrp='chgrp --preserve-root'
alias chromiumext="chromium --enable-remote-extensions"
alias cp="cp --interactive --preserve=mode,ownership,timestamps"
alias cpr="rsync --archive --progress --verbose"
alias cpz="rsync --archive --progress --verbose --compress"
alias diff='diff --unified=3 --show-c-function --color=auto'
alias du='du -k --human-readable'
alias df='df -k --print-type --human-readable'
alias dd='dd status=progress'
alias h='history'
alias fastping='ping -c 5 -s.2'
alias j='jobs -l'
alias l='ls -C --classify'
alias la='ls --almost-all'
alias ll='ls -l'
alias ln='ln --interactive'
alias minicom='minicom --color=on prolificUSBUART'
alias mkdir='mkdir --parents'
alias mount='mount | column -t'
alias mv='mv --interactive'
alias now='date +"%T"'
alias nowdate='date +"%d-%m-%Y"'
alias nowtime=now
alias ping='ping -c 5'
alias pwgen='pwgen --no-capitalize --no-numerals --ambiguous'
alias ports='netstat --tcp -udp --listening --all --numeric --program'
alias rm='rm --interactive=once --preserve-root'
alias top='atop'
alias wget='wget --continue'
alias wl='wc --lines'

if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# functions

function dictword {
    local WORD_LIST
    local WORD_LIST_COUNT
    local NEW_WORD
    WORD_LIST=/usr/share/dict/words
    # echo $WORD_LIST
    WORD_LIST_COUNT=$(cat $WORD_LIST | wc -l)
    # echo $WORD_LIST_COUNT
    RANDOM_NUM=$(od -N3 -An -i /dev/urandom | awk -v f=0 -v r="$WORD_LIST_COUNT" '{printf "%i\n", f + r * $1 / 16777216}')
    NEW_WORD=$(sed  -e s/\'s//g -e `echo $RANDOM_NUM`"q;d" -e 's/\(.*\)/\L\1/' $WORD_LIST | tr '[:upper:]' '[:lower:]' )
    echo $NEW_WORD
    return
}

function dpkg_config_check() {
  dpkg-query -W -f='${Conffiles}\n' '*' |
  sed  '/^$/d' |
  awk '{print $2,$1}' |
  LANG=C md5sum -c 2>/dev/null |
  awk -F': ' '$2 !~ /OK$/{print $1}' |
  sort
}

function git_create_remote_repo {
    if [ ! "${1}" ] || [ ! "${2}" ] || [ ! "${3}" ]; then
        echo "createremotegitrepo \$REPO_SERVER \$REPO_PATH \$REPO_NAME"
        return
    fi
    local REPO_SERVER="${1}"
    local REPO_PATH="${2}"
    local REPO_NAME="${3}"
    local DESCRIPTION=${*:4}
    ssh "${REPO_SERVER}" "mkdir -v ${REPO_PATH}/${REPO_NAME}.git && git -C ${REPO_PATH}/${REPO_NAME}.git init --bare"
    GIT_INIT_RETURN=$?
    if [ $GIT_INIT_RETURN -eq 0 ] && [ "${DESCRIPTION}" ]; then
        echo "Setting repo description"
        ssh "${REPO_SERVER}" "echo ${DESCRIPTION} > ${REPO_PATH}/${REPO_NAME}.git/.git/description"
        if [ $? -ne 0 ]; then
            echo "Set description error"
            return
        fi
    fi
    if [ $GIT_INIT_RETURN -eq 0 ]; then
        echo "Cloning locally"
        git clone "${REPO_SERVER}:${REPO_PATH}/${REPO_NAME}.git"
    else
        echo "Remote git init error"
        return
    fi
    echo "Complete"
}

function letsencryptcertcheck {
    if [ ! $(which ssl-cert-check) ]; then
        echo "ssl-cert-check not installed"
        exit 1
    fi
    local paths
    local sites
    for paths in $(for sites in $(sudo ls /etc/letsencrypt/live); do echo $sites; done)
        do
            sudo ssl-cert-check -c /etc/letsencrypt/live/$paths/cert.pem
        done
}

function reboot_to_windows {
    local MENUENTRY_TITLE
    if [ -f /boot/grub/grub.cfg ]; then
        MENUENTRY_TITLE=$(grep --ignore-case "^menuentry 'Windows" \
            /boot/grub/grub.cfg | head --lines=1 | \
            cut --delimiter="'" --field=2)
        sudo grub-reboot "$MENUENTRY_TITLE"
        sudo reboot
    else
        echo "/boot/grub/grub.cfg not found"
    fi
}

function reboot_to_debian {
    local MENUENTRY_TITLE
    if [ -f /boot/grub/grub.cfg ]; then
        MENUENTRY_TITLE=$(grep --ignore-case "^menuentry 'Debian" \
            /boot/grub/grub.cfg | head --lines=1 | \
            cut --delimiter="'" --field=2)
        sudo grub-reboot "$MENUENTRY_TITLE"
        sudo reboot
    else
        echo "/boot/grub/grub.cfg not found"
    fi
}

function projectupdates {
    local projectdirs
    for projectdirs in ~/Projects/*
    do
        if [ -d "$projectdirs/.git" ]; then
            echo $projectdirs
            git -C $projectdirs pull
        fi
    done

}

# SSH agent with default key
function ssa() {
    if [ -f ~/.ssh/authorized_keys ]; then
        eval $(ssh-agent -s)
        ssh-add
    else
        echo "No authorized_keys file"
    fi
}

function sddevice_backup {
    local COMPRESS
    local COMPRESS_FLAG
    local DEVICE
    local TAGNAME
    local TARGETNAME
    COMPRESS="xz"
    COMPRESS_FLAG="-z"
    if [ ! $(which $COMPRESS) ]; then
        echo "Compressor $COMPRESS not found!"
        exit 1
    fi
    if [ "${1}" ]; then
        DEVICE="${1}"
        TAGNAME="backup-image-sd${DEVICE}"
        if [ "${2}" ]; then
            TAGNAME="${TAGNAME}-${2}"
        fi
        TARGETNAME="${TAGNAME}-$(TZ=UTC date +%Y:%m:%d:%T:%Z).img.gz"
        echo "Zeroing empty space"
        sudo bash -c "TEMPFIXDIR=$(mktemp -d) && mnt /dev/sd${DEVICE} ${TEMPFIXDIR} && dd status=progress if=/dev/zero of=${TEMPFIXDIR}/tmpzero.txt && rm -fr ${TEMPFIXDIR}/tmpzero.txt"
        echo "Backing up and compressing to ${TARGETNAME}"
        sudo bash -c "dd status=progress conv=sparse bs=4M if=/dev/sd${DEVICE} | ionice --class 3  ${COMPRESS} ${COMPRESS_FLAG} > ${TARGETNAME}"
    else
        echo "sdcard_backup /dev/sd\$DEVICE [\$NAME]"
        return
    fi
}

function wakeall {
    if [ -x /usr/bin/wakeonlan ]; then
        for host in $(/usr/sbin/arp -a|grep ^\?|awk '{print $4}')
        do
            wakeonlan "${host}"
        done
    else
        echo "/usr/bin/wakeonlan missing or not installed"
    fi
}

