".vimrc - a vim config
".Copyright (C) Peter Lawler
".Snail mail: PO Box 195
".            Lindisfarne, Tasmania
".            AUSTRALIA 7015
".email:      relwalretep@gmail.com
".
".This program is free software; you can redistribute it and/or
".modify it under the terms of the GNU General Public License
".as published by the Free Software Foundation; either version 2
".of the License, or (at your option) any later version.
".
".This program is distributed in the hope that it will be useful,
".but WITHOUT ANY WARRANTY; without even the implied warranty of
".MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
".GNU General Public License for more details.
".
".You should have received a copy of the GNU General Public License
".long with this program; if not, write to the Free Software
" Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

" Pathogen
execute pathogen#infect()
call pathogen#helptags() " generate helptags for everything in 'runtimepath'
syntax on

filetype plugin indent on

let g:Powerline_symbols = 'fancy'

set autoread
set autoindent
set background=dark
set backup
set backupdir=~/.vim/backup
set cmdheight=2
set directory=~/.vim/tmp
set encoding=utf8
set expandtab
set ffs=unix,dos,mac
set foldcolumn=1
set foldenable
set foldlevelstart=10
set foldmethod=indent
set foldnestmax=10
set hid
set history=500
set hlsearch
set ignorecase
set incsearch
set laststatus=2
set lazyredraw
set mat=2
set nocompatible
set noerrorbells
set number
set rtp+=/usr/lib/python3/dist-packages/powerline/bindings/vim/
set ruler
set shiftwidth=4
set showcmd
set showmatch
set smartcase
set smartindent
set softtabstop=4
set so=7
set tabstop=4
set textwidth=72
set wildmenu
set wrap

syntax enable 
if $COLORTERM == 'gnome-terminal'
	set t_Co=256
endif
try
	colorscheme desert
catch
endtry

if has("gui_running")
    set guioptions-=T
    set guioptions-=e
    set t_Co=256
    set guitablabel=%M\ %t
endif
